using System;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;
using ThrasherShop.Extensions;
using ThrasherShop.Models.RequestModels;
using ThrasherShop.Models.ResultModels;
using ThrasherShop.Models.ViewModels;
using ThrasherShop.Repositories;

namespace ThrasherShop.Controllers
{
    public class AddressController : BaseController
    {
        private readonly IAddressRepository _addresses;

        public AddressController(ILogger<AddressController> logger, IAddressRepository addresses) : base(logger)
        {
            _addresses = addresses;
        }

        public async Task<ResultModel<AddressViewModel>> Get(int id)
        {
            var resultModel = new ResultModel<AddressViewModel>();
            try
            {
                var address = await _addresses.Get(id);
                if (address == null)
                {
                    resultModel.Errors.Add($"Address with id {id} was not found.");
                    return resultModel;
                }
                resultModel.ResultSet = new AddressViewModel(address);
            }
            catch (Exception e)
            {
                resultModel.Errors.Add("Unexpected exception occured. Please review the log files for more details");
                LogError(nameof(Get), e);
            }
            return resultModel;
        }

        public async Task<ListResultModel<AddressViewModel>> Filter(AddressRequestModel requestModel)
        {
            var listResultModel = new ListResultModel<AddressViewModel>();
            try
            {
                requestModel.AddressLine1 = requestModel.AddressLine1?.Trim() ?? string.Empty;
                requestModel.AddressLine2 = requestModel.AddressLine2?.Trim() ?? string.Empty;
                requestModel.AddressLine3 = requestModel.AddressLine3?.Trim() ?? string.Empty;
                requestModel.AddressLine4 = requestModel.AddressLine4?.Trim() ?? string.Empty;
                requestModel.HouseNumber = requestModel.HouseNumber?.Trim() ?? string.Empty;
                requestModel.ZipCode = requestModel.ZipCode?.Trim() ?? string.Empty;
                requestModel.City = requestModel.City?.Trim() ?? string.Empty;
                requestModel.Country = requestModel.Country?.Trim() ?? string.Empty;

                var dbResultModel = await _addresses.Filter(requestModel);

                listResultModel.AmountOfPages = dbResultModel.AmountOfPages;
                listResultModel.ResultSet = dbResultModel.ResultSet.ConvertAll(e => new AddressViewModel(e));
            }
            catch (Exception e)
            {
                listResultModel.Errors.Add("Unexpected exception occured. Please review the log files for more details");
                LogError(nameof(Filter), e);
            }
            return listResultModel;
        }

        public ResultModel<AddressViewModel> Post(AddressViewModel addressViewModel)
        {
            var resultModel = new ResultModel<AddressViewModel>();
            try
            {
                var address = addressViewModel.ToEntity();
                _addresses.Save(address);
                resultModel.ResultSet = new AddressViewModel(address);
            }
            catch (Exception e)
            {
                resultModel.Errors.Add(LogError(nameof(Post), e));
            }
            return resultModel;
        }

        public async Task<ResultModel<AddressViewModel>> Delete(int id)
        {
            var resultModel = new ResultModel<AddressViewModel>();
            try
            {
                await _addresses.Remove(id);
            }
            catch (Exception e)
            {
                resultModel.Errors.Add(LogError(nameof(Delete), e));
            }
            return resultModel;
        }
    }
}