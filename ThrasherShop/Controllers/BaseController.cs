using System;
using System.Text;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using ThrasherShop.Extensions;

namespace ThrasherShop.Controllers
{
    [Route("api/[controller]/[action]")]
    [ApiController]
    public class BaseController : Controller
    {
        protected readonly ILogger Logger;

        public BaseController(ILogger logger)
        {
            Logger = logger;
        }

        /// <summary>
        /// Writes a message to a log file with log level "Information"
        /// </summary>
        /// <param name="methodName">The method from where the <see cref="message"/> is coming</param>
        /// <param name="message">The message itself</param>
        /// <returns>The formatted log message</returns>
        public string LogInfo(string methodName, string message)
        {
            var logMessage = $"Method: {methodName}. Message: {message}";
            Logger.LogInformation(logMessage);
            return logMessage;
        }

        /// <summary>
        /// Writes an Exception to a log file with log lever "Error" in the format: "[Method] [Message] [StackTrace] [InnerException]"
        /// </summary>
        /// <param name="methodName">The method from where the <see cref="ex"/> has occured</param>
        /// <param name="ex">The exception that was thrown</param>
        /// <returns>The message formatted as it will be written to the log file</returns>
        public string LogError(string methodName, Exception ex)
        {
            var builder = new StringBuilder();
            if (ex is AggregateException aggregate)
            {
                builder.AppendLine(LogAggregateException(methodName, aggregate));
            }
            builder.AppendLine($"Error in method: {methodName}. {ex.FormatExceptionMessage()}");
            var logMessage = builder.ToString();
            Logger.LogError(ex, logMessage);
            return logMessage;
        }

        /// <summary>
        /// Writes an Exception to a log file with log lever "Error" in the format: "[Method] [Message] [StackTrace] [InnerException]"
        /// </summary>
        /// <param name="methodName">The method from where the <see cref="aggregate"/> has occured</param>
        /// <param name="aggregate">The exception that was thrown</param>
        /// <returns>The message formatted as it will be written to the log file</returns>
        public string LogAggregateException(string methodName, AggregateException aggregate)
        {
            var builder = new StringBuilder();
            foreach (var exception in aggregate.InnerExceptions)
            {
                builder.AppendLine(LogError(methodName, exception));
            }
            return builder.ToString();
        }
    }
}