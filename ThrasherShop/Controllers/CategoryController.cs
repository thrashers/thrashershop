using System;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using ThrasherShop.Extensions;
using ThrasherShop.Models.DbModels;
using ThrasherShop.Models.RequestModels;
using ThrasherShop.Models.ResultModels;
using ThrasherShop.Models.ViewModels;
using ThrasherShop.Policies;
using ThrasherShop.Repositories;

namespace ThrasherShop.Controllers
{
    public class CategoryController : BaseController
    {
        private readonly ICategoryRepository _categories;

        public CategoryController(ILogger<CategoryController> logger, ICategoryRepository categories) : base(logger)
        {
            _categories = categories;
        }

        [HttpGet]
        public async Task<ResultModel<CategoriesViewModel>> GetAll()
        {
            var resultModel = new ResultModel<CategoriesViewModel>();
            try
            {
                var categories = await _categories.GetCategories();
                if (categories == null)
                {
                    resultModel.Errors.Add("An error occurred while loading the categories");
                    return resultModel;
                }
                resultModel.ResultSet = new CategoriesViewModel(categories);
            }
            catch (Exception e)
            {
                resultModel.Errors.Add("Unexpected exception occured. Please review the log files for more details");
                LogError(nameof(Get), e);
            }
            return resultModel;
        }

        [HttpGet]
        public async Task<ResultModel<CategoryViewModel>> Get(int id)
        {
            var resultModel = new ResultModel<CategoryViewModel>();
            try
            {
                var category = await _categories.Get(id);
                if (category == null)
                {
                    resultModel.Errors.Add($"Category with id {id} does not exists");
                    return resultModel;
                }
                resultModel.ResultSet = new CategoryViewModel(category);
            }
            catch (Exception e)
            {
                resultModel.Errors.Add("Unexpected exception occured. Please review the log files for more details");
                LogError(nameof(Get), e);
            }
            return resultModel;
        }

        [HttpPost]
        [Authorize(Policy = AdminRequirement.RoleName)]
        public async Task<ListResultModel<CategoryViewModel>> Filter(CategoryRequestModel requestModel)
        {
            var listResultModel = new ListResultModel<CategoryViewModel>();
            try
            {
                requestModel.Description = requestModel.Description?.Trim() ?? string.Empty;

                var dbResultModel = await _categories.Filter(requestModel);
                listResultModel.AmountOfPages = dbResultModel.AmountOfPages;
                listResultModel.ResultSet = dbResultModel.ResultSet.ConvertAll(e => new CategoryViewModel(e));
            }
            catch (Exception e)
            {
                listResultModel.Errors.Add("Unexpected exception occured. Please review the log files for more details");
                LogError(nameof(Filter), e);
            }
            return listResultModel;
        }

        [HttpPost]
        [Authorize(Policy = AdminRequirement.RoleName)]
        public async Task<ResultModel<CategoryViewModel>> Post(CategoryViewModel categoryViewModel)
        {
            var resultModel = new ResultModel<CategoryViewModel>();
            try
            {
                var category = categoryViewModel.ToEntity();

                var existingCategoriesWithDescription = await _categories.GetCategoriesByDescription(category.Description);

                if (!existingCategoriesWithDescription.Any(c => c.Id != category.Id))
                {
                    _categories.Save(category);
                    resultModel.ResultSet = new CategoryViewModel(category);
                }
                else
                {
                    resultModel.Errors.Add($"Cannot save category {categoryViewModel.Description}. A category with that name already exists");
                }
            }
            catch (Exception e)
            {
                resultModel.Errors.Add(LogError(nameof(Post), e));
            }
            return resultModel;
        }


        [Authorize(Policy = AdminRequirement.RoleName)]
        public async Task<ResultModel<Category>> Delete(int Id)
        {
            var resultModel = new ResultModel<Category>();
            try
            {
                var category = await _categories.Get(Id);

                if (!category.Products.Any())
                {
                    await _categories.Remove(Id);
                }
                else
                {
                    resultModel.Errors.Add("Category linked to products");
                }
            }
            catch (Exception e)
            {
                resultModel.Errors.Add(LogError(nameof(Delete), e));
            }
            return resultModel;
        }
    }
}