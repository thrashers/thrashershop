using System;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using ThrasherShop.Extensions;
using ThrasherShop.Models.RequestModels;
using ThrasherShop.Models.ResultModels;
using ThrasherShop.Models.ViewModels;
using ThrasherShop.Repositories;

namespace ThrasherShop.Controllers
{
    public class VatController : BaseController
    {
        private readonly IVatRepository _vats;

        public VatController(ILogger<VatController> logger, IVatRepository vats) : base(logger)
        {
            _vats = vats;
        }

        [HttpGet]
        public async Task<ListResultModel<VatViewModel>> GetAll()
        {
            var listResultModel = new ListResultModel<VatViewModel>();
            try
            {
                var dbResultModel = await _vats.GetAll();
                listResultModel.ResultSet = dbResultModel.ConvertAll(e => new VatViewModel(e));
            }
            catch (Exception e)
            {
                listResultModel.Errors.Add(LogError(nameof(GetAll), e));
            }
            return listResultModel;
        }

        [HttpGet]
        public async Task<ResultModel<VatViewModel>> Get(int id)
        {
            var resultModel = new ResultModel<VatViewModel>();
            try
            {
                {
                    var vat = await _vats.Get(id);
                    if (vat == null)
                    {
                        resultModel.Errors.Add($"Vat with id {id} was not found.");
                        return resultModel;
                    }
                    resultModel.ResultSet = new VatViewModel(vat);
                }
            }
            catch (Exception e)
            {
                resultModel.Errors.Add(LogError(nameof(Get), e));
            }

            return resultModel;
        }

        [HttpPost]
        public async Task<ListResultModel<VatViewModel>> Filter(VatRequestModel requestModel)
        {
            var listResultModel = new ListResultModel<VatViewModel>();
            try
            {
                requestModel.Description = requestModel.Description?.Trim() ?? string.Empty;

                var dbResultModel = await _vats.Filter(requestModel);

                listResultModel.AmountOfPages = dbResultModel.AmountOfPages;
                listResultModel.ResultSet = dbResultModel.ResultSet.ConvertAll(e => new VatViewModel(e));
            }
            catch (Exception e)
            {
                listResultModel.Errors.Add(LogError(nameof(Filter), e));
            }
            return listResultModel;
        }

        [HttpPost]
        public async Task<ResultModel<VatViewModel>> Post(VatViewModel viewModel)
        {
            var resultModel = new ResultModel<VatViewModel>();
            try
            {
                var vat = viewModel.ToEntity();

                var existingVatsWithDescription = await _vats.GetVatsByDescription(vat.Description);

                if (!existingVatsWithDescription.Any(v => v.Id != vat.Id))
                {
                    _vats.Save(vat);
                    resultModel.ResultSet = new VatViewModel(vat);
                }
                else
                {
                    resultModel.Errors.Add($"Cannot save vat record {vat.Description}. A vat record with that description already exists");
                }

            }
            catch (Exception e)
            {
                resultModel.Errors.Add(LogError(nameof(Post), e));
            }
            return resultModel;
        }

        public async Task<ResultModel<VatViewModel>> Delete(int id)
        {
            var resultModel = new ResultModel<VatViewModel>();
            try
            {
                await _vats.Remove(id);
            }
            catch (Exception e)
            {
                resultModel.Errors.Add(LogError(nameof(Delete), e));
            }
            return resultModel;
        }
    }
}