﻿using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.UI.Services;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Microsoft.IdentityModel.Tokens;
using ThrasherShop.Extensions;
using ThrasherShop.Models;
using ThrasherShop.Models.DbModels;
using ThrasherShop.Models.ResultModels;
using ThrasherShop.Models.ViewModels;
using ThrasherShop.Repositories;

namespace ThrasherShop.Controllers
{
    public class AccountController : BaseController
    {
        private readonly UserManager<User> _userManager;
        private readonly IPasswordHasher<User> _passwordHasher;
        private readonly IConfiguration _configuration;
        private readonly IEmailSender _emailSender;
        private readonly IUserTokenRepository _userTokens;

        public AccountController(ILogger<AccountController> logger, UserManager<User> userManager, IPasswordHasher<User> passwordHasher,
            IConfiguration configuration, IEmailSender emailSender, IUserTokenRepository userTokens)
            : base(logger)
        {
            _userManager = userManager;
            _passwordHasher = passwordHasher;
            _configuration = configuration;
            _emailSender = emailSender;
            _userTokens = userTokens;
        }

        [HttpPost]
        public async Task<ResultModel<bool>> Register(RegisterViewModel registerViewModel)
        {
            var resultModel = new ResultModel<bool>();
            try
            {
                var user = new User
                {
                    UserName = registerViewModel.UserName,
                    Email = registerViewModel.UserName,
                    FirstName = registerViewModel.FirstName,
                    LastName = registerViewModel.LastName,
                };
                var identityResult = await _userManager.CreateAsync(user, registerViewModel.Password);

                if (!identityResult.Succeeded)
                {
                    LogInfo(nameof(Register), $"Failed to register user {registerViewModel.UserName}");
                    resultModel.Errors.Add("Error while registering a new user.");
                    ExtractIdentityErrors(identityResult, resultModel.Errors);
                }
                else
                {
                    LogInfo(nameof(Register), $"User successfully registered {registerViewModel.UserName}");
                    var roleResult = await _userManager.AddToRoleAsync(user, "User");
                    if (!roleResult.Succeeded)
                    {
                        resultModel.Errors.Add("Error adding user to role User");
                        ExtractIdentityErrors(identityResult, resultModel.Errors);
                    }
                    else
                    {
                        var token = _userTokens.GenerateThrasherEmailConfirmationToken(user.Id);
                        await _emailSender.SendEmailConfirmationAsync(user.Email, $"{Request.Scheme}://{Request.Host}/account/emailconfirmation", token);
                    }
                }
            }
            catch (Exception e)
            {
                resultModel.Errors.Add("An unexpected exception occured. Please review the log files for more details.");
                LogError(nameof(Register), e);
            }
            resultModel.ResultSet = resultModel.IsSuccess;
            return resultModel;
        }

        public async Task<ResultModel<bool>> SendPasswordRecoveryLink(string email)
        {
            var resultModel = new ResultModel<bool>();
            try
            {
                if (!email.IsValidEmail())
                {
                    resultModel.Errors.Add("No or invalid e-mail address");
                    resultModel.ResultSet = false;
                    return resultModel;
                }

                var user = await _userManager.FindByEmailAsync(email);
                if (user == null)
                {
                    resultModel.Errors.Add($"No user found for e-mail address {email}");
                    resultModel.ResultSet = false;
                    return resultModel;
                }

                var token = await _userTokens.GenerateThrasherPasswordRestoreToken(user.Id);
                await _emailSender.SendPasswordRecoveryLinkAsync(email, $"{Request.Scheme}://{Request.Host}/account/resetpassword", token);
                resultModel.ResultSet = resultModel.IsSuccess;
            }
            catch (Exception e)
            {
                LogError(nameof(SendPasswordRecoveryLink), e);
                resultModel.Errors.Add("Something went terribly wrong while sending the password recovery link.");
            }
            return resultModel;
        }

        [HttpPost]
        public async Task<ResultModel<string>> Login(LoginViewModel viewModel)
        {
            var resultModel = new ResultModel<string>();
            try
            {
                var user = await _userManager.FindByNameAsync(viewModel.UserName);
                if (user != null)
                {
                    if (_passwordHasher.VerifyHashedPassword(user, user.PasswordHash, viewModel.Password) == PasswordVerificationResult.Success &&
                        user.EmailConfirmed)
                    {
                        LogInfo(nameof(Login), $"User {user.UserName} logged in successfully");
                        var roles = await _userManager.GetRolesAsync(user);
                        var encryptedToken = GenerateEncryptedToken(user, roles.First());
                        resultModel.ResultSet = encryptedToken;
                    }
                    else
                    {
                        resultModel.Errors.Add("Failed login attempt. Either password is incorrect or e-mail is not confirmed yet.");
                    }
                }
                else
                {
                    resultModel.Errors.Add($"Failed login attempt user with username {viewModel.UserName} not found");
                }
            }
            catch (Exception e)
            {
                resultModel.Errors.Add("An unexpected exception occured. Please review the log files for more details.");
                LogError(nameof(Login), e);
            }
            return resultModel;
        }

        public async Task<ResultModel<string>> ConfirmEmail(string token)
        {
            var resultModel = new ResultModel<string>();
            try
            {
                if (string.IsNullOrEmpty(token?.Trim()))
                {
                    resultModel.Errors.Add("Couldn't verify email. Token is null or empty.");
                    return resultModel;
                }

                var user = await _userTokens.ConfirmEmail(_userManager, token);
                var roles = await _userManager.GetRolesAsync(user);
                resultModel.ResultSet = GenerateEncryptedToken(user, roles.First());
            }
            catch (Exception e)
            {
                resultModel.Errors.Add(LogError(nameof(ConfirmEmail), e));
            }
            return resultModel;
        }

        [HttpPost]
        public async Task<ResultModel<string>> ResetPassword(ResetPasswordViewModel model)
        {
            var resultModel = new ResultModel<string>();
            try
            {
                if (!await _userTokens.ValidatePasswordRestoreToken(_userManager, Logger, model.Token))
                {
                    return resultModel;
                }

                LogInfo(nameof(ResetPassword), "Token validation successful");

                var userId = model.Token.GetUserIdFromToken();
                var user = await _userManager.FindByIdAsync(userId.ToString());

                LogInfo(nameof(ResetPassword), $"Resetting password for user {userId}");
                user.PasswordHash = _passwordHasher.HashPassword(user, model.Password);
                await _userManager.UpdateAsync(user);
            }
            catch (Exception e)
            {
                LogError(nameof(ResetPassword), e);
                resultModel.Errors.Add($"Something bad happend while resetting the password: {e.Message}");
            }
            return resultModel;
        }

        private string GenerateEncryptedToken(User user, string role)
        {
            var claims = new List<Claim>
            {
                new Claim(ClaimTypeConstants.Email, user.Email),
                new Claim(ClaimTypeConstants.Name, string.Concat(user.FirstName, " ", user.LastName)),
                new Claim(ClaimTypeConstants.Identifier, user.Id.ToString()),
                new Claim(ClaimTypeConstants.Role, role)
            };
            var audience = _configuration.GetValueFromJwtSection("Audience");
            var issuer = _configuration.GetValueFromJwtSection("Issuer");
            var key = _configuration.GetValueFromJwtSection("Key");

            var signingKey = new SymmetricSecurityKey(Encoding.ASCII.GetBytes(key));
            var now = DateTime.Now;
            var securityToken = new JwtSecurityToken(
                issuer: issuer,
                audience: audience,
                claims: claims,
                notBefore: now,
                expires: now.Add(TimeSpan.FromMinutes(3600)),
                signingCredentials: new SigningCredentials(signingKey, SecurityAlgorithms.HmacSha256));

            return new JwtSecurityTokenHandler().WriteToken(securityToken);
        }

        private void ExtractIdentityErrors(IdentityResult identityResult, ICollection<string> errorList)
        {
            foreach (var identityError in identityResult.Errors)
            {
                LogInfo(nameof(ExtractIdentityErrors), $"Identity error: code {identityError.Code} description {identityError.Description}");
                errorList.Add($"Identity error: code {identityError.Code} description {identityError.Description}");
            }
        }
    }
}