using System;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using ThrasherShop.Extensions;
using ThrasherShop.Models.DbModels;
using ThrasherShop.Models.RequestModels;
using ThrasherShop.Models.ResultModels;
using ThrasherShop.Models.ViewModels;
using ThrasherShop.Policies;
using ThrasherShop.Repositories;

namespace ThrasherShop.Controllers
{
    [Authorize(Policy = AdminRequirement.RoleName)]
    public class UserController : BaseController
    {
        private readonly IUserRepository _users;
        private readonly IOrderRepository _orders;

        public UserController(ILogger<UserController> logger, IUserRepository users, IOrderRepository orders) : base(logger)
        {
            _users = users;
            _orders = orders;
        }

        [HttpGet]
        public async Task<ResultModel<UserViewModel>> Get(int id)
        {
            var resultModel = new ResultModel<UserViewModel>();
            try
            {
                var user = await _users.Get(id);
                if (user == null)
                {
                    resultModel.Errors.Add($"User with id {id} was not found.");
                    return resultModel;
                }
                resultModel.ResultSet = new UserViewModel(user);
            }
            catch (Exception e)
            {
                resultModel.Errors.Add("Unexpected exception occured. Please review the log files for more details");
                LogError(nameof(Get), e);
            }
            return resultModel;
        }

        [HttpPost]
        public async Task<ListResultModel<UserViewModel>> Filter(UserRequestModel requestModel)
        {
            var listResultModel = new ListResultModel<UserViewModel>();
            try
            {
                requestModel.Email = requestModel.Email?.Trim() ?? string.Empty;
                requestModel.FirstName = requestModel.FirstName?.Trim() ?? string.Empty;
                requestModel.LastName = requestModel.LastName?.Trim() ?? string.Empty;

                var dbResultModel = await _users.Filter(requestModel);

                listResultModel.TotalAmount = dbResultModel.TotalAmount;
                listResultModel.AmountOfPages = dbResultModel.AmountOfPages;
                listResultModel.ResultSet = dbResultModel.ResultSet.ConvertAll(e => new UserViewModel(e));
            }
            catch (Exception e)
            {
                listResultModel.Errors.Add("Unexpected exception occured. Please review the log files for more details");
                LogError(nameof(Get), e);
            }
            return listResultModel;
        }

        [HttpPost]
        public ResultModel<UserViewModel> Post(UserViewModel userViewModel)
        {
            var resultModel = new ResultModel<UserViewModel>();
            try
            {
                var user = userViewModel.ToEntity();
                _users.Save(user);
                resultModel.ResultSet = new UserViewModel(user);
            }
            catch (Exception e)
            {
                resultModel.Errors.Add(LogError(nameof(Post), e));
            }
            return resultModel;
        }

        public async Task<ResultModel<User>> Delete(int id)
        {
            var resultModel = new ResultModel<User>();
            try
            {
                var orders = await _orders.GetByUser(1, 1, id);
                if (orders.ResultSet.Any())
                {
                    resultModel.Errors.Add($"Can't delete user {id}. It is linked to orders.");
                    return resultModel;
                }
                await _users.Remove(id);
            }
            catch (Exception e)
            {
                resultModel.Errors.Add(LogError(nameof(Delete), e));
            }
            return resultModel;
        }
    }
}