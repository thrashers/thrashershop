using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.Extensions.Logging;
using ThrasherShop.Extensions;
using ThrasherShop.Models.DbModels;
using ThrasherShop.Models.ResultModels;
using ThrasherShop.Models.ViewModels;
using ThrasherShop.Repositories;
using Microsoft.AspNetCore.Mvc;


namespace ThrasherShop.Controllers
{
    [Authorize]
    public class WishListController : BaseController
    {
        private readonly IWishListRepository _wishLists;

        public WishListController(ILogger<WishListController> logger, IWishListRepository wishLists) : base(logger)
        {
            _wishLists = wishLists;
        }

        [HttpGet]
        public async Task<ListResultModel<WishListViewModel>> GetByUser(int userId, int pageNumber, int pageSize)
        {
            var resultModel = new ListResultModel<WishListViewModel>();
            try
            {
                if (userId == 0)
                {
                    resultModel.Errors.Add("User id is invalid");
                    return resultModel;
                }
                var dbResult = await _wishLists.GetByUser(userId, pageNumber, pageSize);
                resultModel.ResultSet = dbResult.ResultSet.ConvertAll(e => new WishListViewModel(e));
                resultModel.AmountOfPages = dbResult.AmountOfPages;
            }
            catch (Exception e)
            {
                LogError(nameof(GetByUser), e);
                resultModel.Errors.Add(e.Message);
            }
            return resultModel;
        }

        [HttpPost]
        public ResultModel<WishListViewModel> Save(int userId, int productId)
        {
            var resultModel = new ResultModel<WishListViewModel>();
            try
            {
                if (userId <= 0 || productId <= 0)
                {
                    resultModel.Errors.Add("either user id or product id is invalid");
                    return resultModel;
                }
                var wishList = new WishList {ProductId = productId, UserId = userId};
                _wishLists.Save(wishList);
                resultModel.ResultSet = new WishListViewModel(wishList);
            }
            catch (Exception e)
            {
                LogError(nameof(Save), e);
                resultModel.Errors.Add(e.FormatExceptionMessage());
            }
            return resultModel;
        }

        [HttpDelete]
        public async Task<ListResultModel<WishListViewModel>> Delete(int userId, int productId)
        {
            var resultModel = new ListResultModel<WishListViewModel>();
            try
            {
                if (userId == 0 || productId == 0)
                {
                    resultModel.Errors.Add("Either user id or product id is invalid");
                    return resultModel;
                }
                _wishLists.Remove(userId, productId);
                var dbResult = await _wishLists.GetByUser(userId, pageNumber:1, pageSize:500);
                resultModel.ResultSet = dbResult.ResultSet.ConvertAll(e => new WishListViewModel(e));
            }
            catch (Exception e)
            {
                LogError(nameof(Delete), e);
                resultModel.Errors.Add(e.Message);
            }
            return resultModel;
        }
    }
}