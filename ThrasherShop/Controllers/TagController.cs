using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using ThrasherShop.Extensions;
using ThrasherShop.Models.DbModels;
using ThrasherShop.Models.RequestModels;
using ThrasherShop.Models.ResultModels;
using ThrasherShop.Models.ViewModels;
using ThrasherShop.Policies;
using ThrasherShop.Repositories;

namespace ThrasherShop.Controllers
{
    public class TagController : BaseController
    {
        private readonly ITagRepository _tags;

        public TagController(ILogger<TagController> logger, ITagRepository tags) : base(logger)
        {
            _tags = tags;
        }

        [HttpGet]
        public async Task<ResultModel<TagViewModel>> Get(int id)
        {
            var resultModel = new ResultModel<TagViewModel>();
            try
            {
                var tag = await _tags.Get(id);
                if (tag == null)
                {
                    resultModel.Errors.Add($"Tag with id {id} was not found.");
                    return resultModel;
                }
                resultModel.ResultSet = new TagViewModel(tag);
            }
            catch (Exception e)
            {
                resultModel.Errors.Add("Unexpected exception occured. Please review the log files for more details");
                LogError(nameof(Get), e);
            }
            return resultModel;
        }

        [HttpPost]
        [Authorize(Policy = AdminRequirement.RoleName)]
        public async Task<ListResultModel<TagViewModel>> Filter(TagRequestModel requestModel)
        {
            var listResultModel = new ListResultModel<TagViewModel>();
            try
            {
                requestModel.TagName = requestModel.TagName?.Trim() ?? string.Empty;

                var dbResultModel = await _tags.Filter(requestModel);

                listResultModel.AmountOfPages = dbResultModel.AmountOfPages;
                listResultModel.ResultSet = dbResultModel.ResultSet.ConvertAll(e => new TagViewModel(e));
            }
            catch (Exception e)
            {
                listResultModel.Errors.Add("Unexpected exception occured. Please review the log files for more details");
                LogError(nameof(Filter), e);
            }
            return listResultModel;
        }

        [HttpPost]
        [Authorize(Policy = AdminRequirement.RoleName)]
        public ResultModel<TagViewModel> Post(TagViewModel tagViewModel)
        {
            var resultModel = new ResultModel<TagViewModel>();
            try
            {
                var tag = tagViewModel.ToEntity();
                _tags.Save(tag);
                resultModel.ResultSet = new TagViewModel(tag);
            }
            catch (Exception e)
            {
                resultModel.Errors.Add(LogError(nameof(Post), e));
            }
            return resultModel;
        }

        [Authorize(Policy = AdminRequirement.RoleName)]
        public async Task<ResultModel<Tag>> Delete(int id)
        {
            var resultModel = new ResultModel<Tag>();
            try
            {
                await _tags.Remove(id);
            }
            catch (Exception e)
            {
                resultModel.Errors.Add(LogError(nameof(Delete), e));
            }
            return resultModel;
        }
    }
}