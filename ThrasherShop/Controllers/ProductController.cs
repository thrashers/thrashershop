﻿using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using ThrasherShop.Extensions;
using ThrasherShop.Models.RequestModels;
using ThrasherShop.Models.ResultModels;
using ThrasherShop.Models.ViewModels;
using ThrasherShop.Policies;
using ThrasherShop.Repositories;

namespace ThrasherShop.Controllers
{
    public class ProductController : BaseController
    {
        private readonly IProductRepository _products;

        public ProductController(ILogger<ProductController> logger, IProductRepository products) : base(logger)
        {
            _products = products;
        }

        [HttpGet]
        public async Task<ResultModel<ProductViewModel>> Get(int id)
        {
            var resultModel = new ResultModel<ProductViewModel>();
            try
            {
                var product = await _products.Get(id);
                if (product == null)
                {
                    resultModel.Errors.Add($"Product with id {id} was not found.");
                    return resultModel;
                }

                resultModel.ResultSet = new ProductViewModel(product);
            }
            catch (Exception e)
            {
                resultModel.Errors.Add("Unexpected exception occured. Please review the log files for more details");
                LogError(nameof(Get), e);
            }
            return resultModel;
        }

        [HttpGet]
        public async Task<ListResultModel<ProductViewModel>> GetByCategory(int pageNumber, int pageSize, int categoryId)
        {
            var listResultModel = new ListResultModel<ProductViewModel>();
            try
            {
                var dbResultModel = await _products.GetByCategory(pageNumber, pageSize, categoryId);
                listResultModel.ResultSet = dbResultModel.ResultSet.ConvertAll(e => new ProductViewModel(e));
                listResultModel.AmountOfPages = dbResultModel.AmountOfPages;
            }
            catch (Exception e)
            {
                listResultModel.Errors.Add(LogError(nameof(GetByCategory), e));
            }
            return listResultModel;
        }

        [HttpPost]
        public async Task<ListResultModel<ProductViewModel>> Filter(ProductRequestModel requestModel)
        {
            var listResultModel = new ListResultModel<ProductViewModel>();
            try
            {
                requestModel.Description = requestModel.Description?.Trim() ?? string.Empty;
                requestModel.Name = requestModel.Name?.Trim() ?? string.Empty;
                requestModel.ISBN = requestModel.ISBN?.Trim() ?? string.Empty;

                var dbResultModel = await _products.Filter(requestModel);

                listResultModel.AmountOfPages = dbResultModel.AmountOfPages;
                listResultModel.ResultSet = dbResultModel.ResultSet.ConvertAll(e => new ProductViewModel(e));
            }
            catch (Exception e)
            {
                listResultModel.Errors.Add("Unexpected exception occured. Please review the log files for more details");
                LogError(nameof(Filter), e);
            }
            return listResultModel;
        }

        [HttpGet]
        public async Task<ListResultModel<ProductViewModel>> GetBestSelling(int pageNumber, int pageSize)
        {
            var listResultModel = new ListResultModel<ProductViewModel>();
            try
            {
                var dbResultModel = await _products.GetBestSelling(pageNumber, pageSize);
                listResultModel.AmountOfPages = dbResultModel.AmountOfPages;
                listResultModel.ResultSet = dbResultModel.ResultSet.ConvertAll(e => new ProductViewModel(e));
            }
            catch (Exception e)
            {
                listResultModel.Errors.Add("Unexpected exception occured. Please review the log files for more details");
                LogError(nameof(GetBestSelling), e);
            }
            return listResultModel;
        }

        [HttpPost]
        [Authorize(Policy = AdminRequirement.RoleName)]
        public ResultModel<ProductViewModel> Post(ProductViewModel productViewModel)
        {
            var resultModel = new ResultModel<ProductViewModel>();
            try
            {
                var product = productViewModel.ToEntity();
                _products.Save(product);
                resultModel.ResultSet = new ProductViewModel(product);
            }
            catch (Exception e)
            {
                resultModel.Errors.Add(LogError(nameof(Post), e));
            }
            return resultModel;
        }

        [HttpDelete]
        [Authorize(Policy = AdminRequirement.RoleName)]
        public async Task<ResultModel<ProductViewModel>> Delete(int id)
        {
            var resultModel = new ResultModel<ProductViewModel>();
            try
            {
                await _products.Remove(id);
            }
            catch (Exception e)
            {
                resultModel.Errors.Add(LogError(nameof(Delete), e));
            }
            return resultModel;
        }
    }
}