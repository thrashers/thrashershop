using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using ThrasherShop.Extensions;
using ThrasherShop.Models.RequestModels;
using ThrasherShop.Models.ResultModels;
using ThrasherShop.Models.ViewModels;
using ThrasherShop.Repositories;

namespace ThrasherShop.Controllers
{
    public class OrderController : BaseController
    {
        private readonly IOrderRepository _orders;

        public OrderController(ILogger<OrderController> logger, IOrderRepository orders) : base(logger)
        {
            _orders = orders;
        }

        [HttpGet]
        public async Task<ResultModel<OrderViewModel>> Get(int id)
        {
            var resultModel = new ResultModel<OrderViewModel>();
            try
            {
                var order = await _orders.Get(id);
                if (order == null)
                {
                    resultModel.Errors.Add($"Order with id {id} does not exists");
                    return resultModel;
                }
                resultModel.ResultSet = new OrderViewModel(order);
            }
            catch (Exception e)
            {
                resultModel.Errors.Add("Unexpected exception occured. Please review the log files for more details");
                LogError(nameof(Get), e);
            }
            return resultModel;
        }

        [HttpPost]
        public async Task<ListResultModel<OrderViewModel>> Filter(OrderRequestModel requestModel)
        {
            var listResultModel = new ListResultModel<OrderViewModel>();
            try
            {
                requestModel.FirstName = requestModel.FirstName?.Trim() ?? string.Empty;
                requestModel.LastName = requestModel.LastName?.Trim() ?? string.Empty;
                requestModel.Email = requestModel.Email?.Trim() ?? string.Empty;
                requestModel.AddressLine1 = requestModel.AddressLine1?.Trim() ?? string.Empty;
                requestModel.HouseNumber = requestModel.HouseNumber?.Trim() ?? string.Empty;
                requestModel.ZipCode = requestModel.ZipCode?.Trim() ?? string.Empty;
                requestModel.City = requestModel.City?.Trim() ?? string.Empty;
                requestModel.Country = requestModel.Country?.Trim() ?? string.Empty;

                var dbResultModel = await _orders.Filter(requestModel);

                listResultModel.AmountOfPages = dbResultModel.AmountOfPages;
                listResultModel.ResultSet = dbResultModel.ResultSet.ConvertAll(e => new OrderViewModel(e));
            }
            catch (Exception e)
            {
                listResultModel.Errors.Add("Unexpected exception occured. Please review the log files for more details");
                LogError(nameof(Get), e);
            }
            return listResultModel;
        }

        [HttpGet]
        [Authorize]
        public async Task<ListResultModel<OrderViewModel>> GetByUser(int userId, int pageNumber, int pageSize)
        {
            var listResultModel = new ListResultModel<OrderViewModel>();
            try
            {
                var dbResultModel = await _orders.GetByUser(pageNumber, pageSize, userId);

                listResultModel.AmountOfPages = dbResultModel.AmountOfPages;
                listResultModel.ResultSet = dbResultModel.ResultSet.ConvertAll(e => new OrderViewModel(e));
            }
            catch (Exception e)
            {
                listResultModel.Errors.Add("Unexpected exception occured. Please review the log files for more details");
                LogError(nameof(GetByUser), e);
            }
            return listResultModel;
        }

        [HttpPost]
        public ResultModel<OrderViewModel> Post(OrderViewModel orderViewModel)
        {
            var resultModel = new ResultModel<OrderViewModel>();
            try
            {
                var order = orderViewModel.ToEntity();
                _orders.Save(order);
                resultModel.ResultSet = new OrderViewModel(order);
            }
            catch (Exception e)
            {
                resultModel.Errors.Add(LogError(nameof(Post), e));
            }
            return resultModel;
        }

        [HttpPost]
        public async Task<ResultModel<OrderViewModel>> Delete(int id)
        {
            var resultModel = new ResultModel<OrderViewModel>();
            try
            {
                await _orders.Remove(id);
            }
            catch (Exception e)
            {
                resultModel.Errors.Add(LogError(nameof(Delete), e));
            }
            return resultModel;
        }
    }
}