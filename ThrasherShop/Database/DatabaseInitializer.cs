﻿using System;
using System.Linq;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using ThrasherShop.Extensions;
using ThrasherShop.Models.DbModels;

namespace ThrasherShop.Database
{
    public class DatabaseInitializer
    {
        private static UserManager<User> _userManager;
        private static RoleManager<Role> _roleManager;
        private static ThrasherDbContext _dbContext;
        private static ILogger<DatabaseInitializer> _logger;

        private static bool _migrationsInitialized = false;

        public static void InitializeAndSeedDatabase(IServiceProvider services)
        {
            using (var scope = services.CreateScope())
            {
                _userManager = scope.ServiceProvider.GetRequiredService<UserManager<User>>();
                _roleManager = scope.ServiceProvider.GetRequiredService<RoleManager<Role>>();
                _dbContext = scope.ServiceProvider.GetRequiredService<ThrasherDbContext>();
                _logger = scope.ServiceProvider.GetRequiredService<ILogger<DatabaseInitializer>>();

                InitializeDatabaseMigrations();
                SeedDatabase();
            }
        }

        private static void InitializeDatabaseMigrations()
        {
            try
            {
                _dbContext.Database.Migrate();
                _migrationsInitialized = true;
            }
            catch (Exception e)
            {
                _logger.LogCritical(e, e.FormatExceptionMessage());
                throw;
            }
        }

        private static void SeedDatabase()
        {
            if (!_migrationsInitialized)
            {
                _logger.LogCritical("Cannot seed database without migrations first being initialized.");
                return;
            }

            if (!_dbContext.Users.Any())
            {
                SeedUsers(out var userMike, out var userNick, out var userTim, out var userAstrid, out var userMichael);

                SeedRolesAndSetUsersInRole(userMike, userNick, userTim, userAstrid, userMichael);
            }

            var users = _userManager.GetUsersInRoleAsync("User").Result;
            if (!users.Any())
            {
                GenerateCustomers();
            }

            if (!_dbContext.Vats.Any())
            {
                GenerateVats();
            }

            if (!_dbContext.Orders.Any() && _dbContext.Products.Any())
            {
                GenerateOrders();
            }

            if (_dbContext.Orders.Any() && _dbContext.Orders.All(e => !e.OrderedById.HasValue))
            {
                FixOrdersForeignKeyUser();
            }
        }

        private static void FixOrdersForeignKeyUser()
        {
            var orders = _dbContext.Orders.ToList();
            var users = _dbContext.Users.ToList();

            foreach (var order in orders)
            {
                var user = users.FirstOrDefault(e => e.FirstName == order.FirstName);
                if (user != null)
                {
                    order.OrderedById = user.Id;
                }
            }
            _dbContext.SaveChanges();
        }

        private static void GenerateCustomers()
        {
            for (int i = 0; i < 1000; i++)
            {
                var user = new User
                {
                    UserName = $"{i}-{i}@hr.nl",
                    Email = $"{i}-{i}@hr.nl",
                    EmailConfirmed = true,
                    FirstName = $"{i}-customer",
                    LastName = $"{i}-sir"
                };
                LogAccountIdentityResult(_userManager.CreateAsync(user, "Test1234@!").Result, "Some generated customer");
                LogAccountIdentityResult(_userManager.AddToRoleAsync(user, "User").Result, "Some generated customer role");
            }
        }

        private static void GenerateVats()
        {
            var vatHigh = new Vat
            {
                Description = "Hoog Tarief",
                Tax = 21
            };
            var vatMiddle = new Vat
            {
                Description = "Middel Tarief",
                Tax = 6
            };
            var vatLow = new Vat
            {
                Description = "Laag Tarief",
                Tax = 0
            };
            _dbContext.Vats.Add(vatHigh);
            _dbContext.Vats.Add(vatMiddle);
            _dbContext.Vats.Add(vatLow);
            _dbContext.SaveChanges();
        }

        private static void GenerateOrders()
        {
            var userRand = new Random();
            var orderRand = new Random();

            for (int i = 0; i < 1000; i++)
            {
                var userId = userRand.Next(7, 900);
                var user = _dbContext.Users.First(e => e.Id == userId);

                var productId = orderRand.Next(1, 1790);
                var product = _dbContext.Products.First(e => e.Id == productId);
                var vat = _dbContext.Vats.First(e => e.Id == product.VatId);

                var order = new Order
                {
                    OrderedOn = DateTime.Now,
                    Email = user.UserName,
                    FirstName = user.FirstName,
                    LastName = user.LastName,
                    Status = OrderStatus.Ordered
                };
                _dbContext.Orders.Add(order);
                _dbContext.SaveChanges();

                var orderLine = new OrderLine
                {
                    OrderId = order.Id,
                    ProductId = productId,
                    PriceWithoutVat = product.PriceWithoutVat,
                    Vat = vat.Tax
                };

                _dbContext.OrderLines.Add(orderLine);
                _dbContext.SaveChanges();
            }
        }

        private static void SeedRolesAndSetUsersInRole(User userMike, User userNick, User userTim, User userAstrid, User userMichael)
        {
            var adminRole = new Role
            {
                Name = "Admin",
                NormalizedName = "Admin".ToUpperInvariant()
            };
            var userRole = new Role
            {
                Name = "User",
                NormalizedName = "user".ToUpperInvariant()
            };
            LogRoleIdentityResult(_roleManager.CreateAsync(adminRole).Result, "Admin", "", true);
            LogRoleIdentityResult(_roleManager.CreateAsync(userRole).Result, "User", "", true);

            LogRoleIdentityResult(_userManager.AddToRoleAsync(userMichael, "Admin").Result, "Michael", "Admin", false);
            LogRoleIdentityResult(_userManager.AddToRoleAsync(userMike, "Admin").Result, "Mike", "Admin", false);
            LogRoleIdentityResult(_userManager.AddToRoleAsync(userNick, "Admin").Result, "Nick", "Admin", false);
            LogRoleIdentityResult(_userManager.AddToRoleAsync(userTim, "Admin").Result, "Tim", "Admin", false);
            LogRoleIdentityResult(_userManager.AddToRoleAsync(userAstrid, "Admin").Result, "Astrid", "Admin", false);
        }

        private static void SeedUsers(out User userMike, out User userNick, out User userTim, out User userAstrid, out User userMichael)
        {
            userMichael = new User
            {
                Email = "0947704@hr.nl",
                UserName = "0947704@hr.nl",
                FirstName = "Michael",
                LastName = "de Bruin",
                EmailConfirmed = true
            };
            userMike = new User
            {
                Email = "0152110@hr.nl",
                UserName = "0152110@hr.nl",
                FirstName = "Mike",
                LastName = "van Niel",
                EmailConfirmed = true
            };
            userNick = new User
            {
                Email = "0952636@hr.nl",
                UserName = "0952636@hr.nl",
                FirstName = "Nick",
                LastName = "Gruijters",
                EmailConfirmed = true
            };
            userTim = new User
            {
                Email = "0942185@hr.nl",
                UserName = "0942185@hr.nl",
                FirstName = "Tim",
                LastName = "Damen",
                EmailConfirmed = true
            };
            userAstrid = new User
            {
                Email = "0949082@hr.nl",
                NormalizedEmail = "0949082@hr.nl".ToUpperInvariant(),
                UserName = "0949082@hr.nl",
                FirstName = "Astrid",
                LastName = "Jacobs",
                EmailConfirmed = true
            };
            LogAccountIdentityResult(_userManager.CreateAsync(userMichael, "Thrash3r@!").Result, "Michael");
            LogAccountIdentityResult(_userManager.CreateAsync(userMike, "Thrash3r@!").Result, "Mike");
            LogAccountIdentityResult(_userManager.CreateAsync(userNick, "Thrash3r@!").Result, "Nick");
            LogAccountIdentityResult(_userManager.CreateAsync(userTim, "Thrash3r@!").Result, "Tim");
            LogAccountIdentityResult(_userManager.CreateAsync(userAstrid, "Thrash3r@!").Result, "Astrid");
        }

        private static void LogAccountIdentityResult(IdentityResult identityResult, string user)
        {
            if (identityResult.Succeeded)
            {
                _logger.LogInformation($"User account created for {user}");
                return;
            }

            _logger.LogError($"Failed to create user account for {user}");
            InnerLogIdentityResult(identityResult);
        }

        private static void LogRoleIdentityResult(IdentityResult identityResult, string user, string role, bool creation)
        {
            if (identityResult.Succeeded)
            {
                _logger.LogInformation(creation ? $"Successfully created role {role}" : $"Successfully added user {user} to role {role}");
                return;
            }

            _logger.LogError(creation ? $"Error creating role {role}" : $"Failed to add user {user} to role {role}");
            InnerLogIdentityResult(identityResult);
        }

        private static void InnerLogIdentityResult(IdentityResult identityResult)
        {
            foreach (var identityError in identityResult.Errors)
            {
                _logger.LogError($"Identity details: code {identityError.Code}. Description {identityError.Description}");
            }
        }
    }
}