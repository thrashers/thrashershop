﻿using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using ThrasherShop.Models.DbModels;

namespace ThrasherShop.Database
{
    public class ThrasherDbContext : IdentityDbContext<User, Role, int>
    {
        public ThrasherDbContext(DbContextOptions<ThrasherDbContext> options) : base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);

            builder.Entity<ProductTag>()
                .HasKey(e => new { e.ProductId, e.TagId });

            builder.Entity<WishList>()
                .HasKey(e => new { e.UserId, e.ProductId });

            builder.Entity<ProductAuthor>()
                .HasKey(e => new { e.AuthorId, e.ProductId });

            builder.Entity<UserRating>()
                .HasKey(e => new { e.ProductId, e.UserId });
        }

        public DbSet<Address> Addresses { get; set; }
        public DbSet<Author> Authors { get; set; }
        public DbSet<Category> Categories { get; set; }
        public DbSet<Order> Orders { get; set; }
        public DbSet<OrderStatusLog> OrderStatusLogs { get; set; }
        public DbSet<OrderLine> OrderLines { get; set; }
        public DbSet<Product> Products { get; set; }
        public DbSet<ProductAuthor> ProductAuthors { get; set; }
        public DbSet<ProductTag> ProductTags { get; set; }
        public DbSet<UserRating> UserRatings { get; set; }
        public DbSet<Tag> Tags { get; set; }
        public DbSet<UserToken> Tokens { get; set; }
        public DbSet<Vat> Vats { get; set; }
        public DbSet<WishList> Wishlists { get; set; }
    }
}