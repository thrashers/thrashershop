import { ICollection, IBaseApiClient, IResource } from '../common/interfaces';
import { IProduct } from './../product/interfaces';
import { IUser } from './../user/interfaces';


export interface IWishlist extends IResource {
    id: number;
    concurrencyStamp: string;
    userId: number;
    user: IUser;
    productId: number;
    product: IProduct;
}

export interface IWishlistCollection {
    resultSet: IWishlist[];
}

export interface IWishlistEntity {
    resultSet: IWishlist;
}

export interface IWishlistMapped {
    id: number;
    concurrencyStamp: string;
    userId: number;
    user: IUser;
    productId: number;
    product: IProduct;
}

export interface IWishlistClient extends IBaseApiClient<IWishlist> {
}
