import { HttpHeaders, HttpClient } from '@angular/common/http';
import { map, catchError } from 'rxjs/operators';
import { ICollectionEntity, ICollection } from './../common/interfaces';
import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { Response, RequestOptions, ResponseContentType } from '@angular/http';
import { BaseApiClient } from '../common/base-api-client';
import { IWishlist, IWishlistClient, IWishlistCollection, IWishlistMapped } from './interfaces';
import { ISchema } from '../common/interfaces';

const headers = new Headers();
headers.append('Content-Type', 'application/json');

@Injectable()
export class WishlistClient extends BaseApiClient<IWishlist> implements IWishlistClient {

    protected static classConfig: ISchema = {
        apiResourceName: 'wishlist'
    };

    constructor(protected http: HttpClient) {
        super(http);
    }

    protected apiBase(): string {
        return `/api/${ this.classConfig().apiResourceName }`;
    }

    getByUserId(userId: number, pageNumber: number, pageSize: number): Observable<IWishlistCollection> {
        const url = `${ this.apiBase() }/getByUser?userId=${userId}&pageNumber=${pageNumber}&pageSize=${pageSize}`;
        return this.http.get<IWishlistCollection>(url);
    }

    mapWishlistCollection(wishlists: IWishlist[]): IWishlistMapped[] {
        return wishlists.map((entity: IWishlist) => {
            return {
                id: entity.id,
                concurrencyStamp: entity.concurrencyStamp,
                userId: entity.userId,
                user: entity.user,
                productId: entity.productId,
                product: entity.product
            };
        });
    }
   
    addProduct(userId: number, productId: number): Observable<IWishlistCollection> {
        const url = `${ this.apiBase() }/Save?userId=${userId}&productId=${productId}`;
        var data = [];
        return this.http.post<IWishlistCollection>(url, data);
    }

    deleteProduct(userId: number, productId: number): Observable<IWishlistCollection> {
        const url = `${ this.apiBase() }/delete?userId=${userId}&productId=${productId}`;
        return this.http.delete<IWishlistCollection>(url);
    }
}
