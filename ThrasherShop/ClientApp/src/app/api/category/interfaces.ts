import { ICollection, IBaseApiClient, IResource } from '../common/interfaces';

export interface ICategory extends IResource {
    concurrencyStamp: string;
    description: string;
    id: number;
    parent?: ICategory;
    parentId?: number;
}


export interface ICategoryResult {
  resultSet: ICategoriesViewModel;
}

export interface ICategoriesViewModel {
  Categories: ICategory[];
}

export interface ICategoryEntity {
  resultSet: ICategory;
}

export interface ICategoryMapped {
  concurrencyStamp: string;
  description: string;
  id: number;
  parent?: ICategory;
  parentId?: number;
}

export interface ICategoryCollection {
    categories: ICategory[];
    errors: any[];
}

export interface ICategoryClient extends IBaseApiClient<ICategory> {
}
