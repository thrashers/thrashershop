
import { HttpHeaders, HttpClient } from '@angular/common/http';
import { map, catchError } from 'rxjs/operators';
import { ICollectionEntity, ICollection } from './../common/interfaces';
import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { Response, RequestOptions, ResponseContentType } from '@angular/http';
import { BaseApiClient } from '../common/base-api-client';
import { ICategory, ICategoryCollection, ICategoryClient, ICategoryMapped, ICategoryResult, ICategoriesViewModel } from './interfaces';
import { ISchema } from '../common/interfaces';
import { IResultModel } from '../../models/result.model';

const headers = new Headers();
headers.append('Content-Type', 'application/json');

@Injectable()
export class CategoryClient
  extends BaseApiClient<ICategory> implements ICategoryClient {

    protected static classConfig: ISchema = {
      apiResourceName: 'category'
    };

    constructor(protected http: HttpClient) {
      super(http);
    }

    protected apiBase(): string {
        return `/api/${ this.classConfig().apiResourceName }`;
  }

    getAllRecords(): Observable<ICategory> {
       const url = `${ this.apiBase() }/getAll`;
       return this.http.get<ICategory>(url);
}

    postCategory(category: ICategory): Observable<IResultModel<ICategory>> {
        const url = `${this.apiBase() }/post`;
        return this.http.post<IResultModel<ICategory>>(url, category);
    }

    deleteCategory(id: number): Observable<ICategory> {
        const url = `${this.apiBase()}/delete?id=${id}`;
        return this.http.delete<ICategory>(url);
    }
}
