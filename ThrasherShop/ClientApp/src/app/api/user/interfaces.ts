import { ICategory } from './../category/interfaces';
import { ICollection, IBaseApiClient, IResource } from '../common/interfaces';

export interface IUser {
    author: string;
    category: ICategory;
    categoryId: number;
    concurrencyStamp: string;
    description: string;
    id: number;
    imageUrl?: string;
    inStock: number;
    isbn: string;
    name: string;
    priceWithoutVat: number;
    rating: number;
}

export interface IUserClient extends IBaseApiClient<IUser> {
}
