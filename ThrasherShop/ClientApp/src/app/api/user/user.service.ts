import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map, catchError } from 'rxjs/operators';
import { BaseApiClient } from '../common/base-api-client';
import { IBaseApiClient, ISchema } from '../common/interfaces';
import { UserViewModel } from '../../models/userviewmodel';
import { IResultModel } from '../../models/result.model';
import { ICollectionEntity, ICollection } from './../common/interfaces';

@Injectable()
export class UserClient {
  public users: UserViewModel[];
  public totalNumber: number;

  constructor(public client: HttpClient) {
  }

  protected apiBase(): string {
    return `${location.origin}/api/user`;
  }

  public getTotalAmount(): number {
    return this.totalNumber;
  }

  private editUser: UserViewModel;

  setEditUser(user: UserViewModel) {
    this.editUser = user;
  }

  getEditUser() {
    return this.editUser;
  }

  get(id: number): Observable<IResultModel<UserViewModel>> {

    const url = `${this.apiBase()}?id=${id}`;

    return this.client.get<IResultModel<UserViewModel>>(url);
  }

  filter(pageNumber: number, pageSize: number, firstName: string, lastName: string, email: string) {
    const url = `${this.apiBase()}/filter`;

    this.client.post<IResultModel<UserViewModel[]>>(url, { pageNumber: pageNumber, pageSize: pageSize, firstName: firstName, lastName: lastName, email: email })
      .subscribe(response => {
        this.users = response.resultSet;
        this.totalNumber = response.amountOfPages;
      });
  }

  save(user: UserViewModel): Observable<IResultModel<UserViewModel>> {
    let url = `${this.apiBase()}/Post`;

    return this.client.post<IResultModel<UserViewModel>>(url, user);
  }

  delete(id: number): Observable<IResultModel<UserViewModel>> {
    const url = `${this.apiBase()}/delete?id=${id}`;

    return this.client.get<IResultModel<UserViewModel>>(url);
  }
}
