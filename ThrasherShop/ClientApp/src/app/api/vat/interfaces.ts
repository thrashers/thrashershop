
import { ICollection, IBaseApiClient, IResource } from '../common/interfaces';

export interface IVat extends IResource {
    concurrencyStamp: string;
    description: string;
    id: number;
    tax: number;
}

export interface IVatCollection {
    resultSet: IVat[];
}

export interface IVatClient extends IBaseApiClient<IVat> {
}
