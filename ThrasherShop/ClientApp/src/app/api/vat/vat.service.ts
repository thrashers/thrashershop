import { HttpHeaders, HttpClient } from '@angular/common/http';
import { map, catchError } from 'rxjs/operators';
import { ICollectionEntity, ICollection } from './../common/interfaces';
import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { Response, RequestOptions, ResponseContentType } from '@angular/http';
import { BaseApiClient } from '../common/base-api-client';
import { IVat, IVatClient, IVatCollection } from './interfaces';
import { ISchema } from '../common/interfaces';
import { IResultModel } from '../../models/result.model';

const headers = new Headers();
headers.append('Content-Type', 'application/json');

@Injectable()
export class VatClient extends BaseApiClient<IVat> implements IVatClient {

    protected static classConfig: ISchema = {
        apiResourceName: 'vat'
    };

    constructor(protected http: HttpClient) {
        super(http);
    }

    protected apiBase(): string {
        return `/api/${ this.classConfig().apiResourceName }`;
    }

    getAllRecords(): Observable<IVatCollection> {
       const url = `${ this.apiBase() }/getAll`;
       return this.http.get<IVatCollection>(url);
    }

    postVAT(vat: any): Observable<IResultModel<IVat>> {
        const url = `${this.apiBase() }/post`;
        return this.http.post<IResultModel<IVat>>(url, vat);
    }

    deleteVAT(id: number): Observable<IVat> {
        const url = `${this.apiBase()}/delete?id=${id}`;
        return this.http.delete<IVat>(url);
    }

}
