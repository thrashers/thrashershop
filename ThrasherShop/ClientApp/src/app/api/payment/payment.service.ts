
import { HttpHeaders, HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

const headers = new Headers();
headers.append('Content-Type', 'application/json');
headers.append('Authorization', 'Bearer ' + 'sk_test_9mCVOcsfkoAtDNtFjhU6bvVb');

@Injectable()
export class PaymentClient {

    constructor(protected http: HttpClient) {}

    makePayment(token: string, amount: number) {
        const url = 'https://wt-90681a3e24cd079c19bc6e4d867317d9-0.sandbox.auth0-extend.com/stripe-payment';

        return this.http.post(url, { amount: amount * 100, stripeToken: token });
        // .subscribe(
        //   (result) => {
        //       console.log(result);
        //   },
        //   errore => {}
        // );
    }
}
