import { IResultModel } from './../../models/result.model';
import { HttpClient } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { map } from 'rxjs/operators';
import { Http, Response } from '@angular/http';
import { IBaseApiClient, ICollection, ICollectionEntity, IResource, ISchema } from './interfaces';

export abstract class BaseApiClient<T extends IResource> implements IBaseApiClient<T> {

    protected static handleRequestError(error: any): Observable<any> {
        return throwError(error._body);
    }

    constructor(protected http: HttpClient) {
    }

    protected apiBase(): string {
        return `/api/${ this.classConfig().apiResourceName }`;
    }

    get(id: string): Observable<IResultModel<T>> {
        const url = `${ this.apiBase() }/get?id=${id}`;

        return this.http
            .get<IResultModel<T>>(url);
    }

    filter(requestModel): Observable<IResultModel<T[]>> {
        const url = `${ this.apiBase() }/filter`;

        return this.http
            .post<IResultModel<T[]>>(url, requestModel);
    }

    getAll(): Observable<IResultModel<T>> {

        const url = `${ this.apiBase() }/getAll`;

        return this.http
            .get<IResultModel<T>>(url);
    }

    getByUser(userId: number, pageNumber: number, pageSize: number): Observable<IResultModel<T>> {
        const url = `${ this.apiBase() }/getByUser?userId=${userId}&pageNumber=${pageNumber}&pageSize=${pageSize}`;

        return this.http
            .get<IResultModel<T>>(url);
    }

    delete(id: number): Observable<IResultModel<T>> {
        const url = `${ this.apiBase() }`;

        return this.http
            .post<IResultModel<T>>(url, id);
    }

    // save(entity: T): Observable<T> {
    //     if (entity.id) {
    //         return this.http.patch(`${ this.apiBase() }/${ entity.id }`, entity)
    //             .pipe(map((response: Response) => {
    //                 return response.json().data;
    //             }))
    //             .pipe(catchError(BaseApiClient.handleRequestError));
    //     } else {
    //         return this.http.post(`${ this.apiBase() }`, entity)
    //             .pipe(map((response: Response) => {
    //                 return response.json().data;
    //             }))
    //             .pipe(catchError(BaseApiClient.handleRequestError));
    //     }
    // }

    // remove(id: string): Observable<void> {
    //     return this.http.delete(`${ this.apiBase() }/${ id }`)
    //         .pipe(map((response: Response) => {
    //             return response.json().data;
    //         }))
    //         .pipe(catchError(BaseApiClient.handleRequestError));
    // }

    protected classConfig(): ISchema {
        return (this.constructor as any).classConfig as ISchema;
    }

}
