import { IResultModel } from './../../models/result.model';
import { Observable } from 'rxjs';

export interface ICollectionEntity<T> {
    data: T;
    include?: any;
}

export interface ICollection<T extends IResource> {
    resultSet: ICollectionEntity<T>[];
    count?: number;
    success?: boolean;
}

export interface IResource {
    id?: number;
}

export interface IBaseApiClient<T> {
    get(id: string): Observable<IResultModel<T>>;
    getAll(): Observable<IResultModel<T>>;
}

export interface IPaginationParams {
    offset: number;
    limit: number;
    count: number;
}

export interface ISchema {
    apiResourceName: string;
}

export interface IErrorResponse {
    _body: string;
    status: number;
    statusText: string;
}
