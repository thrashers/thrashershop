import { ICategory } from './../category/interfaces';
import { IVat } from './../vat/interfaces';
import { ICollection, IBaseApiClient, IResource } from '../common/interfaces';

export interface IProduct extends IResource {
    id: number;
    concurrencyStamp: string;
    name: string;
    description: string;
    author: string;
    isbn: string;
    priceWithoutVat: number;
    priceWithVat: number;
    inStock: number;
    imageUrl: string;
    rating: number;
    categoryId: number;
    category: ICategory;
    vatId: number;
    vat: IVat;
}

export interface IProductCollection {
    resultSet: IProduct[];
    errors: any[];
    isSuccess: boolean;
    amountOfPages: number;
}

export interface IProductEntity {
    resultSet: IProduct;
}

export interface IProductMapped {
    id: number;
    concurrencyStamp: string;
    name: string;
    description: string;
    author: string;
    isbn: string;
    priceWithoutVat: number;
    priceWithVat: number;
    inStock: number;
    imageUrl: string;
    rating: number;
    categoryId: number;
    category: ICategory;
    vatId: number;
    vat: IVat;
}

export interface IProductClient extends IBaseApiClient<IProduct> {
}
