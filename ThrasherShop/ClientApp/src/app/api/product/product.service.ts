import { HttpHeaders, HttpClient } from '@angular/common/http';
import { map, catchError } from 'rxjs/operators';
import { ICollectionEntity, ICollection } from './../common/interfaces';
import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { Response, RequestOptions, ResponseContentType } from '@angular/http';
import { BaseApiClient } from '../common/base-api-client';
import { IProduct, IProductClient, IProductCollection, IProductMapped } from './interfaces';
import { ISchema } from '../common/interfaces';
import { IResultModel } from './../../models/result.model';

const headers = new Headers();
headers.append('Content-Type', 'application/json');

@Injectable()
export class ProductClient extends BaseApiClient<IProduct> implements IProductClient {

    protected static classConfig: ISchema = {
        apiResourceName: 'product'
    };

    constructor(protected http: HttpClient) {
        super(http);
    }

    protected apiBase(): string {
        return `/api/${ this.classConfig().apiResourceName }`;
    }

    getBestSelling(pageNumber: number, pageSize: number): Observable<IProductCollection> {
        const url = `${ this.apiBase() }/getBestSelling?pageNumber=${ pageNumber }&pageSize=${ pageSize }`;
        return this.http.get<IProductCollection>(url);
    }

    getCategoryProducts(pageNumber: number, pageSize: number, categoryId: number): Observable<IProductCollection> {
        const url = `${ this.apiBase() }/getByCategory?pageNumber=${ pageNumber }&pageSize=${ pageSize }&categoryId=${ categoryId }`;
        return this.http.get<IProductCollection>(url);
    }

    getByCategory(pageNumber: number, pageSize: number, categoryId: number): Observable<IProductCollection> {
        const url = `${this.apiBase()}/GetByCategory?pageNumber=${pageNumber}&pageSize=${pageSize}&categoryId=${categoryId}`;
        return this.http.get<IProductCollection>(url);
    }

    updateProduct(product: IProduct): Observable<IResultModel<IProduct>> {
        const url = `${this.apiBase()}/post`;
        return this.http.post<IResultModel<IProduct>>(url, product);
    }

    deleteProduct(productId: number): Observable<IProduct> {
        const url = `${this.apiBase()}/delete?id=${productId}`;
        return this.http.delete<IProduct>(url);
    }

    mapProdoctCollection(products: IProduct[]): IProductMapped[] {
        return products.map((entity: IProduct) => {
            return {
              author: entity.author,
              category: entity.category,
              categoryId: entity.categoryId,
              concurrencyStamp: entity.concurrencyStamp,
              description: entity.description,
              id: entity.id,
              imageUrl: entity.imageUrl,
              inStock: entity.inStock,
              isbn: entity.isbn,
              name: entity.name,
              priceWithoutVat: entity.priceWithoutVat,
              priceWithVat: entity.priceWithVat,
              rating: entity.rating,
              vatId: entity.vatId,
              vat: entity.vat
            };
        });
    }

    mapProduct(product: IProduct): IProductMapped {

          return {
            author: product.author,
            category: product.category,
            categoryId: product.categoryId,
            concurrencyStamp: product.concurrencyStamp,
            description: product.description,
            id: product.id,
            imageUrl: product.imageUrl,
            inStock: product.inStock,
            isbn: product.isbn,
            name: product.name,
            priceWithoutVat: product.priceWithoutVat,
            priceWithVat: product.priceWithVat,
            rating: product.rating,
            vatId: product.vatId,
            vat: product.vat
          };
        
    }
}
