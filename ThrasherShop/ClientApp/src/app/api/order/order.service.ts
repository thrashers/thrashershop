import { Observable } from 'rxjs';
import { IResultModel } from './../../models/result.model';
import { HttpHeaders, HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BaseApiClient } from '../common/base-api-client';
import { IOrder, IOrderClient } from './interfaces';
import { ISchema } from '../common/interfaces';

const headers = new Headers();
headers.append('Content-Type', 'application/json');

@Injectable()
export class OrderClient extends BaseApiClient<IOrder> implements IOrderClient {

    protected static classConfig: ISchema = {
        apiResourceName: 'order'
    };

    constructor(protected http: HttpClient) {
        super(http);
    }

    protected apiBase(): string {
        return `/api/${ this.classConfig().apiResourceName }`;
    }

    post(formData, userId, products) {
        const url = `${this.apiBase()}/post`;
        const order = {
            FirstName: formData.firstName,
            LastName: formData.lastName,
            Email: formData.email,
            Street: formData.street,
            ZipCode: formData.zipCode,
            AddressLine1: formData.street + ' ' + formData.houseNumber,
            HouseNumber: formData.houseNumber,
            City: formData.city,
            Country: 'Nederland',
            concurrencyStamp: 'EUR',
            OrderedById: userId ? userId : null,
            OrderedOn: new Date(),
            Status: 'paid',
            Products: products ? products : [],
        };
        return this.http.post(url, order);
    }

    mapOrdersCollection(orders) {
        return orders.map((entity) => {
            return {
              id: entity.id,
              addressLine1: entity.addressLine1,
              addressLine2: entity.addressLine2,
              addressLine3: entity.addressLine3,
              addressLine4: entity.addressLine4,
              city: entity.city,
              concurrencyStamp: entity.concurrencyStamp,
              country: entity.country,
              email: entity.email,
              firstName: entity.firstName,
              fullName: entity.fullName,
              lastName: entity.lastName,
              orderedBy: entity.orderedBy,
              orderedById: entity.orderedById,
              orderedOn: entity.orderedOn,
              products: entity.products,
              status: entity.status,
              zipCode: entity.zipCode,
            };
        });
    }
}
