import { UserViewModel } from './../../models/userviewmodel';
import { IBaseApiClient, IResource } from '../common/interfaces';

export interface IOrder extends IResource {
    id: number;
    concurrencyStamp: string;
    OrderedOn: Date;
    email: string;
    firstName: string;
    lastName: string;
    fullName: string;
    addressLine1: string;
    addressLine2: string;
    addressLine3: string;
    addressLine4: string;
    houseNumber: number;
    zipCode: string;
    city: string;
    Country: string;
    status: string;
    orderedById: number;
    OrderedBy: UserViewModel;
}

export interface IOrderClient extends IBaseApiClient<IOrder> {
}
