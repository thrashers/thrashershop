export interface ICartItem {
    id: number;
    isbn: string;
    name: string;
    priceWithoutVat: number;
    priceWithVat: number;
    inStock: number;
    quantity: number;
}
