import { IProduct } from './../../api/product/interfaces';
import { SessionStorageService } from 'ngx-webstorage';
import { ICartItem } from './interfaces';
import { Injectable } from '@angular/core';

@Injectable()
export class ShoppingCartService {
    cartItem: ICartItem;
    cart: ICartItem[];

    constructor(
        private sessionStorageService: SessionStorageService
    ) {

    }

    productToCartItem(product: IProduct, quantity: number): ICartItem {

        const item = {
          id: product.id,
          isbn: product.isbn,
          name: product.name,
          priceWithoutVat: product.priceWithoutVat,
          priceWithVat: product.priceWithVat,
          inStock: product.inStock,
          quantity: quantity,
        };

        return item;
    }

    addProductToCart(product: any, quantity: number): void {
        const newItem = this.productToCartItem(product, quantity);
        let cart: ICartItem[];
        this.sessionStorageService.retrieve('cart') === null ? cart = [] : cart = this.sessionStorageService.retrieve('cart') ;

        if (!cart.some((item) => item.id === newItem.id)) {
            if (this.checkProductsInStock(quantity, newItem)) {
                cart.push(newItem);
            } else {
                alert(`There are only ${newItem.inStock} left of ${newItem.name}`);
            }
        } else {
            const itemToUpdate: ICartItem = cart.find(item => item.id === newItem.id);

            if (this.checkProductsInStock(quantity, newItem)) {
                this.updateQuantityItem(newItem, quantity, cart);
            } else {
                alert(`There are only ${newItem.inStock} left of ${newItem.name}`);
            }
        }

        this.sessionStorageService.store('cart', cart);
    }

    removeItemFromCart(item: ICartItem): void {
        const cart: ICartItem[] = this.sessionStorageService.retrieve('cart');
        const newCart = cart.filter(function(value, index, arr) {
            return value.id !== item.id;
        });

        this.sessionStorageService.store('cart', newCart);
    }

    getCartTotalPrice(): number {
        const cart: ICartItem[] = this.sessionStorageService.retrieve('cart');

        if (!cart) {
            return 0;
        }
        if (cart.length === 0) {
            return 0;
        } else {
            let total: number = null;
            cart.forEach(function(value, index, arry) {
              total += value.priceWithoutVat / 100 * 106 * value.quantity;
            });
            return total;
        }
    }

    getAllcartItems(): ICartItem[] {
        let cart: ICartItem[];
        this.sessionStorageService.retrieve('cart') === null ? cart = [] : cart = this.sessionStorageService.retrieve('cart') ;
        return cart;
    }

    calculateTotalPrice(orderedProducts: ICartItem[]): number {
        let total: number = 0;
        let subtotal: number = 0;
        let postage: number = 0;
        let discount: number = 0;
        let quantity: number = 0;
        
        subtotal = this.calculateSubtotal(orderedProducts);
        quantity = this.calculateTotalQuantity(orderedProducts);
        discount = this.calculateDiscount(subtotal, quantity);
        subtotal = subtotal - discount;
        postage = this.calculatePostage(subtotal);
        total = subtotal + postage;

        return total;
    }

    calculateSubtotal(orderedProducts: ICartItem[]): number {
        let subtotal: number = 0;

        orderedProducts.forEach(
            function (product, index, products) {
                subtotal += product.priceWithVat * product.quantity;
            }
        );

        return subtotal;
    }

    calculateTotalQuantity(orderedProducts: ICartItem[]): number {
        let count: number = 0;

        orderedProducts.forEach(
            function (product, index, products) {
                count += product.quantity;
            }
        );

        return count;
    }

    calculateDiscount(subtotal: number, quantity: number): number {
        let discountPercentage: number = this.getDiscountPercentage(quantity);
        return subtotal * discountPercentage;
    }

    getDiscountPercentage(quantity: number): number {
        if (quantity >= 10)
            return 0.05;
        else
            return 0.0;
    }

    calculatePostage(subtotal: number): number {
        const minSubtotalForFreePostage: number = 50.0;
        const postageFee: number = 6.99;

        if (subtotal >= minSubtotalForFreePostage)
            return 0.0;
        else
            return postageFee;
    }



    getCartFromSession(): ICartItem[] {
        return this.sessionStorageService.retrieve('cart');
    }

    private updateQuantityItem(newItem: ICartItem, quantity: number, cart: ICartItem[]): ICartItem[] {
        const itemIndex = cart.findIndex((item => item.id === newItem.id));

        cart[itemIndex].quantity = quantity;
        return cart;
    }

    private checkProductsInStock (quantity: number, newItem: ICartItem): boolean {
        return quantity <= newItem.inStock;
    }
}
