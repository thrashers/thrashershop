import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { User } from '../../models/user';
import { IResultModel } from '../../models/result.model';
import * as jwt_decode from 'jwt-decode';

@Injectable()
export class AuthenticationService {
  private user: User = null;
  private cookieName: string = 'ThrasherCookie';

  private apiBase(): string {
    return `${location.origin}/api/`;
  }

  constructor(private http: HttpClient) {
    this.setUser();
  }

  register(firstname: string, lastname: string, username: string, password: string): Observable<IResultModel<boolean>> {
    return this.http.post<IResultModel<boolean>>(`${this.apiBase()}account/register`,
      { FirstName: firstname, LastName: lastname, UserName: username, Password: password });
  }

  login(username: string, password: string): Observable<IResultModel<string>> {
    return this.http.post<IResultModel<string>>(`${this.apiBase()}account/login`, { username: username, password: password });
  }

  confirmEmail(token: string): Observable<IResultModel<string>> {
    return this.http.get<IResultModel<string>>(`${this.apiBase()}account/ConfirmEmail?token=${token}`);
  }

  sendPasswordLink(email: string): Observable<IResultModel<boolean>> {
    return this.http.get<IResultModel<boolean>>(`${this.apiBase()}account/SendPasswordRecoveryLink?email=` + email);
  }

  resetPassword(token: string, password: string): Observable<IResultModel<string>> {
    return this.http.post<IResultModel<string>>(`${this.apiBase()}account/ResetPassword`, { token: token, password: password });
  }

  setUser() {
    let cookie = localStorage.getItem(this.cookieName);
    if (cookie !== 'null' && cookie !== null) {
      this.user = new User();
      var decodedUser = jwt_decode(cookie);
      this.user.id = decodedUser.id;
      this.user.name = decodedUser.name;
      this.user.email = decodedUser.email;
      this.user.role = decodedUser.role;
    }
  }

  setToken(token: string) {
    localStorage.setItem(this.cookieName, token);
    this.setUser();
  }

  getToken() {
    return localStorage.getItem(this.cookieName);
  }

  isAdmin() {
    return this.isAuthenticated() && this.user.role === 'Admin';
  }

  isAuthenticated(): boolean {
    return this.user != null && this.user.id !== 1;
  }

  getUser(): User {
    return this.user;
  }

  getUserId(): number {
    return this.user ? this.user.id : null;
  }

  logout() {
    localStorage.removeItem(this.cookieName);
    this.user = null;
  }
}
