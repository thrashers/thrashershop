import { OrdersComponent } from './components/orders/orders.component';
import { ShoppingCartComponent } from './components/cart/shopping-cart.component';
import { ShopComponent } from './components/shop/shop.component';
import { HomeComponent } from './components/home/home.component';
import { RouterModule, Routes } from '@angular/router';
import { NgModule } from '@angular/core';
import { ProductComponent } from './components/product/product.component';
import { LoginComponent } from './components/login/login.component';
import { RegisterComponent } from './components/register/register.component';
import { WishlistComponent } from './components/wishlist/wishlist.component';
import { LogoutComponent } from './components/logout/logout.component';
import { NotfoundComponent } from './components/notfound/notfound.component';
import { EmailConfirmationComponent } from './components/account/email-confirmation/email-confirmation.component';
import { ConfirmEmailComponent } from './components/account/confirm-email/confirm-email.component';
import { ProfileComponent } from './components/account/profile/profile.component';
import { CheckoutComponent } from './components/checkout/checkout.component';
import { ManageUsersListComponent } from './components/admin/manage-users-list/manage-users-list.component';
import { EditUserComponent } from './components/admin/manage-users-list/edit-user/edit-user.component';
import { ForgotPasswordComponent } from './components/account/forgot-password/forgot-password.component';
import { ResetPasswordComponent } from './components/account/reset-password/reset-password.component';

import { AdminGuard } from './guards/admin.guard';
import { UserGuard } from './guards/user.guard';
import { ShopinfoComponent } from './components/shopinfo/shopinfo.component';
import { ManageProductsComponent } from './components/admin/manage-products/manage-products.component';
import { ManageVatComponent } from './components/admin/manage-vat/manage-vat.component';
import { ManageCategoriesComponent } from './components/admin/manage-categories/manage-categories.component';
import { DashboardComponent } from './components/admin/dashboard-statistics/dashboard.component';

const routes: Routes = [
  { path: '', component: HomeComponent },
  { path: 'product', component: ProductComponent },
  { path: 'product/:productId', component: ProductComponent },
  { path: 'register', component: RegisterComponent },
  { path: 'shop', component: ShopComponent },
  { path: 'shop/:categoryId', component: ShopComponent },
  { path: 'login', component: LoginComponent },
  { path: 'wishlist', component: WishlistComponent },
  { path: 'shopping-cart', component: ShoppingCartComponent },
  { path: 'logout', component: LogoutComponent },
  { path: 'wishlist', component: WishlistComponent },
  { path: 'checkout', component: CheckoutComponent },
  { path: 'shopinfo', component: ShopinfoComponent },
  { path: 'account', children: [
      { path: 'emailconfirmation/:token', component: EmailConfirmationComponent },
      { path: 'resetpassword/:token', component: ResetPasswordComponent },
      { path: 'confirmemail', component: ConfirmEmailComponent },
      { path: 'profile', component: ProfileComponent, canActivate: [UserGuard] }
    ]},
  {
    path: 'admin', canActivate: [AdminGuard], children: [
      { path: 'dashboard', component: DashboardComponent },
      { path: 'users', component: ManageUsersListComponent },
      { path: 'user-edit', component: EditUserComponent },
      { path: 'products', component: ManageProductsComponent },
      { path: 'vat', component: ManageVatComponent },
      { path: 'orders', component: OrdersComponent },
      { path: 'categories', component: ManageCategoriesComponent }]
  },
  { path: 'forgotpassword', component: ForgotPasswordComponent },
  { path: 'notfound', component: NotfoundComponent },
  { path: '**', redirectTo: 'notfound' } // leave this as the last routing. If it falls through till this one it means something went wrong
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes)
  ],
  exports: [RouterModule],
})
export class AppRoutingModule { }
