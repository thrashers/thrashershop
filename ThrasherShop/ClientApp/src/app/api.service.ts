import { Injectable } from '@angular/core'
import { HttpClient, HttpHeaders } from '@angular/common/http'
import { Subject} from 'rxjs'

@Injectable()
export class ApiService {

    private baseUrl = '/api';


    private selectedProduct = new Subject<any>();
    productSelected = this.selectedProduct.asObservable();

    private selectedCategory = new Subject<any>();
    categorySelected = this.selectedCategory.asObservable();

   
    constructor(private http: HttpClient) {}


    getProduct(productId) {
        return this.http.get(this.baseUrl + `/product/Get?id=${productId}`);
    }

    getCategories() {
        return this.http.get(this.baseUrl + '/category/GetAll');
    }

    getCategory(categoryId) {
        return this.http.get(this.baseUrl + `/category/Get?id=${categoryId}`);
    }

    saveCategory(category) {
        return this.http.post(this.baseUrl + '/category/edit', category);
    }

    deleteCategory(categoryId) {
        return this.http.delete<any>(this.baseUrl + '/category/' + categoryId);
    }

}
