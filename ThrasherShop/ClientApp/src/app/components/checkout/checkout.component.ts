import { IResultModel } from './../../models/result.model';
import { ToastrService } from 'ngx-toastr';
import { AuthenticationService } from './../../services/authentication/authentication.service';
import { IOrder } from './../../api/order/interfaces';
import { OrderClient } from './../../api/order/order.service';
import { PaymentClient } from './../../api/payment/payment.service';
import { HttpClient } from '@angular/common/http';
import { ShoppingCartService } from './../../services/shopping-cart/shopping-cart.service';
import { ICartItem } from './../../services/shopping-cart/interfaces';
import { Subscription } from 'rxjs';
import { Component,
  OnInit,
  ElementRef,
  ViewChild,
  ChangeDetectorRef,
  OnDestroy,
  AfterViewInit } from '@angular/core';
import { NgForm } from '@angular/forms';


@Component({
  selector: 'app-checkout',
  templateUrl: './checkout.component.html',
  styleUrls: ['./checkout.component.css'],
  providers: [
    PaymentClient,
    OrderClient,
  ]
})
export class CheckoutComponent implements OnInit, AfterViewInit, OnDestroy {
  @ViewChild('cardInfo') cardInfo: ElementRef;

  productsObservable: Subscription;
  totalPrice = 0;
  shippingCost = 3.95;
  cardHandler = this.onChange.bind(this);
  card: any;
  error: string;
  cartItems: ICartItem[] = [];
  creditcard: any;
  paypal:any;

  constructor(
    private shoppingCartService: ShoppingCartService,
    private cd: ChangeDetectorRef,
    private paymentClient: PaymentClient,
    private client: HttpClient,
    private orderClient: OrderClient,
    private authService: AuthenticationService,
    private toastService: ToastrService
  ) { }

  ngAfterViewInit() {
    this.card = elements.create('card');
    this.card.mount(this.cardInfo.nativeElement);
    this.card.addEventListener('change', this.cardHandler);
  }

  ngOnDestroy() {
    this.card.removeEventListener('change', this.cardHandler);
    this.card.destroy();
  }

  onChange({ error }) {
    if (error) {
      this.error = error.message;
    } else {
      this.error = null;
    }
    this.cd.detectChanges();
  }

    ngOnInit(): void {
        this.totalPrice = this.shoppingCartService.getCartTotalPrice();
    }

    async onCheckout(form: NgForm) {
      const formData = form.value;
      this.orderClient.post(formData, this.authService.getUserId(), []).subscribe(response => {
        if (response) {
          this.toastService.success('Successfully ordered your products', 'Success');
          form.reset();
          this.cartItems = this.shoppingCartService.getAllcartItems();
        }
      });

      if (formData.credticard) {
        const { token, error } = await stripe.createToken(this.card);
        if (error) {
          console.log('Something is wrong:', error);
        } else {
          this.paymentClient.makePayment(token.id, 200)
          .subscribe(
            (result) => {
                console.log(result);
            },
            errors => {}
          );
        }
      }
    }
}
