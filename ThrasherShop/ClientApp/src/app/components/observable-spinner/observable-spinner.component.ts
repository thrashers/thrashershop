import { Component, SimpleChanges, Input } from '@angular/core';
import { Subscription } from 'rxjs';

@Component({
    selector: 'app-observable-spinner-component',
    templateUrl: './observable-spinner.template.html',
    styleUrls: [ './observable-spinner.style.css' ]
})
export class ObservableSpinnerComponent {

    @Input() subscription: Subscription;
    @Input() backgroundColor: string = '#ffffff';
    @Input() marginTop: string;

    isSubscriptionClosed: boolean = true;

    ngOnChanges(change: SimpleChanges): void {
        if (this.isSubscriptionClosed === undefined && change.subscription.currentValue !== null) {
            this.isSubscriptionClosed = change.subscription.currentValue.closed;
        }
    }

    ngDoCheck(): void {
        if (this.subscription !== null && this.isSubscriptionClosed !== this.subscription.closed) {
            this.isSubscriptionClosed = this.subscription.closed;
        }
    }
}
