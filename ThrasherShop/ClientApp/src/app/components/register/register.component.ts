import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Router } from '@angular/router';
import { AuthenticationService } from "../../services/authentication/authentication.service";

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {
  public errors = [];

  constructor(private authService: AuthenticationService, private route: Router) {
    if (this.authService.isAuthenticated()) {
      this.route.navigate(['/']);
    }
  }

  ngOnInit() {
  }

  onSubmit(form: NgForm) {
    let formData = form.value;
    this.authService.register(formData.firstName, formData.lastName, formData.username, formData.password).subscribe(response => {
      if (response.isSuccess) {
        this.route.navigate(['/account/confirmemail']);
      } else {
        this.errors = response.errors;
      }
    });;
  }
}
