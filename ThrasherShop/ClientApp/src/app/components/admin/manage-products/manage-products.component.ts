import { Component, OnInit } from '@angular/core';
import { ApiService } from '../../../api.service';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs';
import { CategoryClient } from '../../../api/category/category.service';
import { ICollectionEntity, ICollection } from '../../../api/common/interfaces';
import { IProduct, IProductCollection, IProductMapped } from '../../../api/product/interfaces';
import { forEach } from '../../../../../node_modules/@angular/router/src/utils/collection';
import { ICategory, ICategoryCollection, ICategoryClient, ICategoryMapped } from '../../../api/category/interfaces';
import { ProductClient } from '../../../api/product/product.service';
import { IResultModel } from '../../../models/result.model';
import { NgForm } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
import { IVat } from '../../../api/vat/interfaces';

@Component({
  selector: 'app-manage-products',
  templateUrl: './manage-products.component.html',
  styleUrls: ['./manage-products.component.css'],
  providers: [
    CategoryClient,
    ProductClient
  ]
})
export class ManageProductsComponent implements OnInit {

    categoryObservable: Subscription;
    productsObservable: Subscription;
    productObservable: Subscription;
    categoryList: ICategory[];
    productList: IProduct[];
    product: IProduct;
    categoryId: number;
    addMode: boolean;
    pageNumber: number;
    pageSize: number;
    pageTotal: number;


    constructor(
        private api: ApiService,
        private categoryClient: CategoryClient,
        private productClient: ProductClient,
        private route: ActivatedRoute,
        private toastService: ToastrService
    ) { }
        
    ngOnInit() {
        this.addMode = false;
        this.pageSize = 15;
        this.loadCategories();
        this.changeCategory(1);
        this.gotoFirstPage();
    }


    loadCategories = function () {
        this.api.getCategories().subscribe(response => {this.categoryList = response.resultSet.categories});
    }

    changeCategory = function (id) {
        this.categoryId = Number(id);
        this.product = null;
        this.loadProducts();
    }

    gotoFirstPage() {
        this.pageNumber = 1;
        this.loadProducts();
    }

    gotoPrevPage() {
        if (this.pageNumber > 1) {
          this.pageNumber = this.pageNumber - 1;
          this.loadProducts();
        }
    }

    gotoNextPage() {
        if (this.pageNumber < this.pageTotal) {
          this.pageNumber = this.pageNumber + 1;
          this.loadProducts();
        }
    }

    gotoLastPage() {
        this.pageNumber = this.pageTotal;
        this.loadProducts();
    }

    loadProducts = function () {
        this.productsObservable = this.productClient.getCategoryProducts(this.pageNumber, this.pageSize, this.categoryId).subscribe(
            (result: IResultModel<IProductCollection>) => { 
                if (result.isSuccess) {
                    this.productList = result.resultSet;
                    this.pageTotal = result.amountOfPages;
                } else {
                    this.toastService.error(result.errors[0], 'Cannot load product records');
                }
            }
        );
    }

    setAddMode = function () {
        this.addMode = true;
        this.product = {
            id: 0, 
            concurrencyStamp: '1', 
            name: '', 
            description: '',
            author: '', 
            isbn: '', 
            priceWithoutVat: 0,
            priceWithVat: 0, 
            inStock: 0,
            imageUrl: '',
            rating: 0,
            categoryId: this.categoryId,
            category: null, 
            vatId: 1,
            vat: null};
    }

    cancelAddMode = function () {
        this.deselectProducts();
        this.addMode = false;
        this.product = null;
    }

    selectProduct = function (product) {
        this.deselectProducts();
        product.active = !product.active;
        this.product = product;
    }

    deselectProducts = function () {
        this.productList.forEach(e => e.active = false);
    }

    deleteProduct = function () {
        this.productObservable = this.productClient.deleteProduct(this.product.id).subscribe(
            (result: IResultModel<IProduct>) => { 
                if (result.isSuccess) {
                    this.toastService.success('Successfully deleted the product', "Success");
                    this.product = null;
                    this.changeCategory(this.categoryId);
                } else {
                    this.toastService.error(result.errors[0], 'Cannot delete product');
                }
            });
    }

    onSubmit(form: NgForm) {
        this.productObservable = this.productClient.updateProduct(this.product).subscribe(
            (result: IResultModel<IProduct>) => { 
                if (result.isSuccess) {
                    this.toastService.success('Successfully created or updated the product record', 'Post result');
                    this.product = null;
                    this.loadProducts();
                    this.addMode = false;
                } else {
                    this.toastService.error(result.errors[0], 'Cannot create or update product record');
                }
            });
    }
    
}
