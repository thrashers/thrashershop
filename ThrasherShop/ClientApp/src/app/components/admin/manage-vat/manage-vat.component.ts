import { Component, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { ICollectionEntity, ICollection } from '../../../api/common/interfaces';
import { forEach } from '../../../../../node_modules/@angular/router/src/utils/collection';
import { VatClient } from '../../../api/vat/vat.service';
import { IVat, IVatCollection } from '../../../api/vat/interfaces';
import { NgForm } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
import { IResultModel } from '../../../models/result.model';

@Component({
  selector: 'app-manage-vat',
  templateUrl: './manage-vat.component.html',
  styleUrls: ['./manage-vat.component.css'],
  providers: [
    VatClient
  ]
})
export class ManageVatComponent implements OnInit {

    vatObservable: Subscription;
    vatList: IVat[];
    vatId: number;
    vat: IVat;
    addMode: boolean;

    constructor(
        private vatClient: VatClient,
        private toastService: ToastrService
    ) { }
        
    ngOnInit() {
        this.loadVatList();
        this.addMode = false;
    }


    loadVatList = function () {
        this.vatObservable = this.vatClient.getAllRecords().subscribe(
            (result: IResultModel<IVat>) => { 
                if (result.isSuccess) {
                    this.vatList = result.resultSet;
                } else {
                    this.toastService.error(result.errors[0], 'Cannot load vat records');
                }
            }
        );
    }


    setAddMode = function () {
        this.deselectList();
        this.addMode = true;
        this.vat = {id: 0, concurrencyStamp: '', description: '', tax: 0};
    }

    cancelAddVat = function () {
        this.addMode = false;
        this.vat = null;
    }

    selectVat = function (vat) {
        this.deselectList();
        vat.active = !vat.active;
        this.vat = vat;
    }

    deselectList = function () {
        this.vatList.forEach(e => e.active = false);
    }

    deleteVat = function () {
        this.vatObservable = this.vatClient.deleteVAT(this.vat.id).subscribe(
            (result: IResultModel<IVat>) => { 
                if (result.isSuccess) {
                    this.toastService.success('Successfully deleted the VAT record', 'Update result');
                    this.vat = null;
                    this.loadVatList();
                } else {
                    this.toastService.error(result.errors[0], 'Cannot load vat records');
                }
            });
    }

    onSubmit = function (form: NgForm) {
        this.vatObservable = this.vatClient.postVAT(this.vat).subscribe(
            (result: IResultModel<IVat>) => { 
                if (result.isSuccess) {
                    this.toastService.success('Successfully created or updated VAT record', 'Post result');
                    this.vat = null;
                    this.loadVatList();
                    this.addMode = false;
                } else {
                    this.toastService.error(result.errors[0], 'Cannot create or update vat record');
                }
            });
    }

}
