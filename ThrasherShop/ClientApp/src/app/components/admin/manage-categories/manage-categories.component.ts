import { Component, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { ICollectionEntity, ICollection } from '../../../api/common/interfaces';
import { CategoryClient } from '../../../api/category/category.service';
import { ApiService } from '../../../api.service';
import { forEach } from '../../../../../node_modules/@angular/router/src/utils/collection';
import { ICategory, ICategoryCollection } from '../../../api/category/interfaces';
import { NgForm } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
import { IResultModel } from '../../../models/result.model';


@Component({
  selector: 'app-manage-categories',
  templateUrl: './manage-categories.component.html',
  styleUrls: ['./manage-categories.component.css'],
  providers: [
      ApiService,
      CategoryClient
  ]
})
export class ManageCategoriesComponent implements OnInit {

    catObservable: Subscription;
    catList: ICategory[];
    catId: number;
    cat: ICategory;
    addMode: boolean;

    constructor(
        private api: ApiService,
        private categoryClient: CategoryClient,
        private toastService: ToastrService
    ) { }
    

    ngOnInit() { 
        this.loadCatList();
        this.addMode = false;
    }

    loadCatList = function () {
        this.catObservable = this.categoryClient.getAllRecords().subscribe(
            (result: IResultModel<ICategoryCollection>) => { 
                if (result.isSuccess) {
                    this.catList = result.resultSet.categories;
                } else {
                    this.toastService.error(result.errors[0], 'Cannot load categories');
                }
            }
        );
    }

    setAddMode = function () {
        this.deselectList();
        this.addMode = true;
        this.cat = {id: 0, concurrencyStamp: '', description: ''};
    }

    cancelAddCategory = function () {
        this.addMode = false;
        this.cat = null;
    }

    selectCategory = function (category) {
        this.deselectList();
        category.active = !category.active;
        this.categoryId = category.id;
        this.cat = category;
    }

    deselectList = function () {
        this.catList.forEach(e => e.active = false);
    }

    deleteCategory = function () {
        this.catObservable = this.categoryClient.deleteCategory(this.cat.id).subscribe(
            (result: IResultModel<ICategory>) => { 
                if (result.isSuccess) {
                    this.toastService.success('Successfully deleted the category', 'Delete category result');
                    this.cat = null;
                    this.loadCatList();
                } else {
                    this.toastService.error(result.errors[0], 'Cannot delete category');
                }
            });
    }

    onSubmit = function (form: NgForm) {
        this.catObservable = this.categoryClient.postCategory(this.cat).subscribe(
            (result: IResultModel<ICategory>) => { 
                if (result.isSuccess) {
                    this.toastService.success('Successfully created or updated category record', 'Save changes result');
                    this.cat = null;
                    this.loadCatList();
                    this.addMode = false;
                } else {
                    this.toastService.error(result.errors[0], 'Cannot create or update category record');
                }
            });
    }

}
