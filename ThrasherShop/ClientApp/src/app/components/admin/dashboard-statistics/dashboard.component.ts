import { Component, OnInit, Pipe, PipeTransform } from '@angular/core';
import { ChartsModule } from 'ng2-charts';
import { ICategory, ICategoryResult, ICategoriesViewModel, ICategoryCollection } from '../../../api/category/interfaces';
import { ApiService } from '../../../api.service';
import { Subscription } from 'rxjs';
import { IProduct, IProductCollection } from '../../../api/product/interfaces';
//import { ProductClient } from '../../../api/product/product.service';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css'],
})
export class DashboardComponent implements OnInit {
  categories: ICategory[];
  productsObservable: Subscription;
  bestSellingProducts: any[];
  selectedCategoryId: number;
  selectedChartType;

  constructor(
    //private productClient: ProductClient,
    private api: ApiService
  ) { }

  public barChartOptions: any = {
    scaleShowVerticalLines: false,
    responsive: true,

    scales: {
      xAxes: [{
        ticks: {
          /*fontColor: "rgba(251, 183, 16, 0.8)",*/
          fontSize: 14,
          /*stepSize: 1,*/
          beginAtZero: true
        }
      }]
    }
  };

  public barChartLabels: string[] = ['Januari', 'Februari', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];
  public barChartType: string = 'bar';
  public barChartLegend: boolean = true;

  public barChartData: any[] = this.getBarChartsData(-1);

  public lineChartColors: Array<any> = [
    { // dark grey
      backgroundColor: 'rgba(51,51,51,0.6)',
      borderColor: 'rgba(51,51,51,1)',
      pointBackgroundColor: 'rgba(51,51,51,1)',
      pointBorderColor: '#fff',
      pointHoverBackgroundColor: '#fff',
      pointHoverBorderColor: 'rgba(51,51,51,1)'
    },
    { // yellow/orange
      backgroundColor: 'rgba(251, 183, 16, 0.8)',
      borderColor: 'rgba(251, 183, 16, 1)',
      pointBackgroundColor: 'rgba(251, 183, 16, 1)',
      pointBorderColor: '#fff',
      pointHoverBackgroundColor: '#fff',
      pointHoverBorderColor: 'rgba(251, 183, 16, 1)'
    },
  ];

  // events
  public chartClicked(e: any): void {
    console.log(e);
  }

  public chartHovered(e: any): void {
    console.log(e);
  }

  public onChartTypeChange(chartType: string): void {

    this.selectedChartType = chartType;
    if (chartType == 'mixed') {
      this.barChartType = 'bar';
      this.barChartData[1].type = 'line';
      this.barChartData[1].fill = false;
    }
    else {
      this.barChartType = chartType;
      this.barChartData[1].type = null;
      this.barChartData[1].fill = 'origin';
    }
    //if (chartType == 'mixed') {
    //  this.barChartData = this.getBarChartsData(this.selectedCategoryId)
    //}
  }

  public onChartDataCategoryChange(categoryId: number): void {
    this.barChartData = this.getBarChartsData(categoryId);
    this.selectedCategoryId = categoryId;
  }

  public getBarChartsData(categoryId: number): any[] {
    let currentYear: number = new Date().getFullYear();
    let currentYearSalesArray: Array<number> = new Array<number>();
    let lastYearSalesArray: Array<number> = new Array<number>();;
    let arrayOfSalesArrays: Array<any> = new Array<any>();

    for (let i = 1; i <= 12; i++) {
      currentYearSalesArray.push(this.booksSold(i, currentYear, categoryId))
    }

    arrayOfSalesArrays.push({ data: currentYearSalesArray, label: currentYear });
    for (let i = 1; i <= 12; i++) {
      lastYearSalesArray.push(this.booksSold(i, currentYear - 1, categoryId))
    }
    arrayOfSalesArrays.push({ data: lastYearSalesArray, label: currentYear - 1, type: this.selectedChartType == 'mixed' ? 'line' : null, fill: this.selectedChartType == 'mixed' ? false : true });
    return arrayOfSalesArrays;

  }

  public booksSold(month: number, year: number, categoryId: number): number {
    if (categoryId == -1) //all books
    {
      let number = 0;
      for (let i = 0; i < 17; i++) {
        number += Math.floor((Math.random() * 100) + 1)
      }
      return number;
    }
    else
    {
      return Math.floor((Math.random() * 100) + 1);
    }
  }

  /* method used for loading the categories */
  loadCategories = function () {
    this.api.getCategories().subscribe(response => {
      this.categories = response.resultSet.categories;
    });
  }

  ngOnInit() {
    this.loadCategories();

    //this.productsObservable = this.productClient.getBestSelling(1, 10).subscribe(
    //  (products: IProductCollection) => { this.bestSellingProducts = this.productClient.mapProdoctCollection(products.resultSet); },
    //  error => { }
    //);
    let bestSellingHardcoded: { name: string, category: any, priceWithoutVat: number }[] = [
      { "name": 'Projectmanagement incl. toegigang tot Prepzone', "category": { "description": 'Reizen & vakantie' }, "priceWithoutVat": 26.95 },
      { "name": 'Thirteen Reasons Why', "category": { "description": 'Literatuur & Romans' }, "priceWithoutVat": 6.49 },
      { "name": 'Visual thinking', "category": { "description": 'Studieboeken' }, "priceWithoutVat": 16.95 },
      { "name": 'En plotseling ben je van hem', "category": { "description": 'Managementboeken' }, "priceWithoutVat": 5.99 },
      { "name": 'De Gruffalo', "category": { "description": 'Religie & Spiritualiteit' }, "priceWithoutVat": 8.95 },
      { "name": 'Slaapklets! voor kleuters', "category": { "description": 'Religie en Spiritualiteit' }, "priceWithoutVat": 14.95 },
      { "name": 'Bobbi 30 - Bobbi viert Sint-Maarten', "category": { "description": 'Religie en Spiritualiteit' }, "priceWithoutVat": 7.99 },
      { "name": 'Harry Potter and the Cursed Child', "category": { "description": 'Literatuur & Romans' }, "priceWithoutVat": 8.99 },
      { "name": 'The Hate U Give', "category": { "description": 'Literatuur & Romans' }, "priceWithoutVat": 8.22 },
      { "name": 'Marathon Training', "category": { "description": 'Kookboeken' }, "priceWithoutVat": 8.09 }
    ];
    this.bestSellingProducts = bestSellingHardcoded;
  }

}
