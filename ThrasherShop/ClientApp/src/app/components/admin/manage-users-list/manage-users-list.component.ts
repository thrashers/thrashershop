import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { UserClient } from '../../../api/user/user.service';
import { UserViewModel } from "../../../models/userviewmodel";
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-manage-users-list',
  templateUrl: './manage-users-list.component.html',
  styleUrls: ['./manage-users-list.component.css']
})
export class ManageUsersListComponent implements OnInit {
  
  public page: number = 1;

  constructor(public  userClient: UserClient, private router: Router, private toastService: ToastrService) {
    this.userClient.filter(this.page, 15, '', '', '');
  }

  ngOnInit() {
  }

  onPageChanged(event): void {
    this.page = event;
    this.userClient.filter(this.page, 15, '', '', '');
  }

  onDeleteConfirm(event): void {
    if (window.confirm("Are you sure you want to delete?")) {
      this.userClient.delete(event.id).subscribe(reponse => {
        if (reponse.isSuccess) {
          this.toastService.success('User successfully deleted');
        } else {
          for (let i = 0; i < reponse.errors.length; i++) {
            this.toastService.error(reponse.errors[i]);
          }
        }

        //ugly refresh of page.
        this.userClient.filter(this.page, 15, '', '', '');
      });
    }
  }

  onEditConfirm(event): void {
    this.router.navigate(['/admin/user-edit']);
    this.userClient.setEditUser(event);
  }
}
