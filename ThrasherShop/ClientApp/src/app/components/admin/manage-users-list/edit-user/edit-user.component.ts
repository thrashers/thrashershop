import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { UserClient } from '../../../../api/user/user.service';
import { UserViewModel } from "../../../../models/userviewmodel";
import { ToastrService } from 'ngx-toastr';
import { NgForm } from "@angular/forms";

@Component({
  selector: 'app-edit-user',
  templateUrl: './edit-user.component.html',
  styleUrls: ['./edit-user.component.css']
})
export class EditUserComponent implements OnInit {

  constructor(private userService: UserClient, private router: Router, private toastService: ToastrService) { }

  editUser: UserViewModel;

  ngOnInit() {
    this.getActiveRecord();
  }

  getActiveRecord() {
    let edit = this.userService.getEditUser();
    console.log(edit);

    if (edit != undefined) {
      this.editUser = edit;
    } else {
      this.router.navigate(['/admin/users']);
    }
  }

  onSubmit(form: NgForm) {
//    let formData = form.value;
    this.userService.save(this.editUser).subscribe(response => {
      if (response.isSuccess) {
        this.toastService.success('Changes successfully saved.', 'Success');
        this.router.navigate(['/admin/users']);
      } else {
        this.toastService.error(response.errors[0], 'Failed');
      }
    });
  }
}
