import { ICollectionEntity, ICollection } from './../../api/common/interfaces';
import { IProduct, IProductCollection, IProductMapped } from './../../api/product/interfaces';
import { Component, OnInit } from '@angular/core';
import { ProductClient } from '../../api/product/product.service';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css'],
  providers: [
    ProductClient
  ]
})
export class HomeComponent implements OnInit {
  productsObservable: Subscription;
  bestSellingProducts: IProduct[];

  constructor(
    private productClient: ProductClient
  ) { }

  ngOnInit() {
    this.productsObservable = this.productClient.getBestSelling(1, 12).subscribe(
      (products: IProductCollection) => { this.bestSellingProducts = this.productClient.mapProdoctCollection(products.resultSet); },
      error => {}
    );
  }
}
