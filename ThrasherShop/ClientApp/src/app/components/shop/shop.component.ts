import { ICollectionEntity, ICollection } from './../../api/common/interfaces';
import { IProduct, IProductCollection, IProductMapped } from './../../api/product/interfaces';
import { Component, OnInit } from '@angular/core';
import { ProductClient } from '../../api/product/product.service';
import { Subscription } from 'rxjs';
import { ICategory, ICategoryResult, ICategoriesViewModel, ICategoryCollection } from '../../api/category/interfaces';
import { CategoryClient } from '../../api/category/category.service';
import { ShoppingCartService } from '../../services/shopping-cart/shopping-cart.service';
import { ApiService } from '../../api.service';
import { ActivatedRoute } from '@angular/router';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-shop',
  templateUrl: './shop.component.html',
  styleUrls: ['./shop.component.css'],
  providers: [
    ProductClient,
    CategoryClient,
    ShoppingCartService
  ]
})
export class ShopComponent implements OnInit {
  productsObservable: Subscription;
  currentProducts: IProduct[];
  categoriesObservable: Subscription;
  categories: ICategory[];
  currentCategoryId: number;
  productsPerPage = 12;
  currentPage = 1;
  maxPage = 1;
  currentPageArray: number[];

  constructor(
    private productClient: ProductClient,
    private shoppingCartService: ShoppingCartService,
    private api: ApiService,
    private route: ActivatedRoute,
    private toastService: ToastrService 
  ) { }


  /* method used for getting the right amount of stars for the product rating*/
  getRating = function (rating: number) {
    if (rating > 6)
      rating = 5;

    if (rating < 1)
      return "no rating available";
    else
      return '<i class="fa fa-star"></i>'.repeat(rating);
  }

  /* method used to get the right product image */
  getProductImage = function (isbn: string) {
    return '../assets/images/' + isbn + '.jpg';
  }

  /* method used for adding the product to the shopping cart */
  addProductToCart(product: IProduct, quantity: number): void {
    this.shoppingCartService.addProductToCart(product, quantity);
    this.toastService.success('Successfully added the product to your shopping cart', "Success");
  }

  /* method used to get a new list of products from the specified category  */
  loadProductsFromCategory(categoryId: number, pageNumber: number = 1): void {
    this.currentPage = pageNumber;
    this.currentCategoryId = categoryId;

    //get new products list from selected category
    this.productsObservable =
      this.productClient.getByCategory(this.currentPage, this.productsPerPage, categoryId).subscribe((products: IProductCollection) => {
      this.currentProducts = this.productClient.mapProdoctCollection(products.resultSet);
      //this.maxPage = products.amountOfPages;
      //this.loadPagesArray(this.currentPage, this.maxPage);
      });
  }

  /* method used to change the page number */
  changePageNumber(pageNumber: number): void {
    this.loadProductsFromCategory(this.currentCategoryId, pageNumber);
  }

  /* method used for loading the categories */
  loadCategories = function () {
    this.api.getCategories().subscribe(response =>
    {
      this.categories = response.resultSet.categories;
      this.loadProductsFromCategory(this.categories[0].id, 1);
    });
  }

  /* method used to build up the pagination\page navigation based on the current page and the maximum amount of pages in the curent category */
  //loadPagesArray(newCurrentPage: number, maxPage: number): void {
  //  var numberarray: number[];

  //  for (var page = 1; page < maxPage; page++){
  //    if (page == 1 || page == maxPage || (page >= newCurrentPage - 2 && page <= newCurrentPage + 2)) {
  //      numberarray.push(page);
  //    }
  //  }

  //  this.currentPageArray = numberarray;
  //}
  
  /* executed when initializing shop component... */
  ngOnInit()  {

    this.loadCategories();

    this.currentPageArray = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10];

    /* get categoryId as parameter */
    var paramCatId = this.route.snapshot.paramMap.get('categoryId');

    if (paramCatId) {
      this.currentCategoryId = Number(paramCatId);
      this.loadProductsFromCategory(this.currentCategoryId, 1);
    }
          
  }
}
