import { ShoppingCartService } from './../../services/shopping-cart/shopping-cart.service';
import { ICartItem } from './../../services/shopping-cart/interfaces';
import { Subscription } from 'rxjs';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-shopping-cart',
  templateUrl: './shopping-cart.component.html',
  styleUrls: ['./shopping-cart.component.css'],
})
export class ShoppingCartComponent implements OnInit {
  productsObservable: Subscription;
  shoppingCart: ICartItem[] = [];
  totalPrice = 0;

  constructor(
    private route: ActivatedRoute,
    private shoppingCartService: ShoppingCartService
  ) { }

  ngOnInit(): void {
    this.shoppingCart = this.shoppingCartService.getCartFromSession();
    this.calculateTotalPrice();
  }

  calculateTotalPrice(): void {
    this.totalPrice = this.shoppingCartService.getCartTotalPrice();
  }

  onUpdateQuantityItem(item: ICartItem, quantity: number): void  {
    quantity < 0 ? quantity = 0 : quantity = quantity;
    this.shoppingCartService.addProductToCart(item, quantity);
    this.calculateTotalPrice();
  }

  onRemoveFromCart(item: ICartItem): void  {
    this.shoppingCartService.removeItemFromCart(item);
    this.shoppingCart = this.shoppingCartService.getCartFromSession();
  }
}
