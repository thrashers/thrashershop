import {Component, OnInit} from '@angular/core';
import { ApiService } from '../../api.service';
import { ActivatedRoute } from '@angular/router';
import { forEach } from '../../../../node_modules/@angular/router/src/utils/collection';


@Component({
  selector: 'app-category',
  templateUrl: './category.component.html',
  styleUrls: ['./category.component.css']
})
export class CategoryComponent implements OnInit {

    constructor(private api: ApiService, private route: ActivatedRoute) { }
    
    categoryId;
    selectedCategory;
    categories;

    ngOnInit() { 
        this.categoryId = this.route.snapshot.paramMap.get('categoryId');
 
        if(this.categoryId > 0)
            this.api.getCategory(this.categoryId).subscribe(res => {this.selectedCategory = res});
        else
            this.api.getCategories().subscribe(res => {this.categories = res});
    }

    selectCategory = function (category) {
        this.categories.resultSet.categories.forEach((e) => e.active = false);
        category.active = !category.active;
        this.categoryId = category.id;
        this.api.getCategory(this.categoryId).subscribe(res => {this.selectedCategory = res});
    }

    editCategory = function () {
        this.api.saveCategory(this.selectedCategory.resultSet).subscribe(res => {this.ngOnInit()});
    }

    deleteCategory = function () {
        //alert(this.categoryId);
        this.api.deleteCategory(this.categoryId).subscribe(res => {
            if (res.isSuccess)
            {
                this.selectedCategory = null;
                this.ngOnInit();
            }
            else
                alert(res.errors[0].substring(res.errors[0].indexOf("Message"), res.errors[0].indexOf("Stack")));

        });
    }

}
