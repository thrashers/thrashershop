import { Component, Output, EventEmitter } from '@angular/core';
import { AuthenticationService } from '../../services/authentication/authentication.service';
import { User } from '../../models/user';

@Component({
  selector: 'app-nav-sidebar',
  templateUrl: './nav-sidebar.component.html',
  styleUrls: ['./nav-sidebar.component.css']
})
export class NavSidebarComponent {
  @Output() onSearchBar: EventEmitter<boolean> = new EventEmitter<boolean>();

  constructor(public authService: AuthenticationService) { }

  onSearchClick(): void {
      this.onSearchBar.emit(true);
  }
}
