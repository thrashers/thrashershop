import { Subscription } from 'rxjs';
import { IProductCollection, IProduct } from './../../api/product/interfaces';
import { ProductClient } from './../../api/product/product.service';
import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-search-bar',
  templateUrl: './search-bar.component.html',
  styleUrls: ['./search-bar.component.css'],
  providers: [
    ProductClient
  ]
})
export class SearchBarComponent implements OnInit {
  @Input() isSearchBarVisable: boolean;
  searchInput: string;
  productsObservable: Subscription;
  products: IProduct[];
  searchResult = false;

  requestModel = {
    PageNumber: 0,
    PageSize: 50,
    CategoryId: 0,
    Description: '',
    Name: '',
    PriceWithoutVat: null,
    PriceWithVat: null,
    VatId: 0,
    ISBN: ''
  };

  constructor(
    private productClient: ProductClient,
  ) {

  }

  ngOnInit() {
  }

  onCloseSearch(): void {
    this.isSearchBarVisable = false;
    this.searchResult = false;
  }

  onSearch(searchInput: string): void {
    this.requestModel.Name = searchInput;
    this.productsObservable = this.productClient.filter(this.requestModel).subscribe((products: IProductCollection) => {
      this.products = products.resultSet;
      this.searchResult = true;
    });
  }

}
