import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Router } from '@angular/router';
import { AuthenticationService } from "../../services/authentication/authentication.service";

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  constructor(private authService: AuthenticationService, private router: Router) {
    if (this.authService.isAuthenticated()) {
      this.router.navigate(['/']);
    }
  }

  ngOnInit() {
  }

  onSubmit(form: NgForm) {
    let formData = form.value;
    this.authService.login(formData.username, formData.password)
      .subscribe(response => {
        if (response.isSuccess) {
          this.authService.setToken(response.resultSet);
          this.router.navigate(['/']); 
        }
      });;
  }

  onForgotPassword() {
    this.router.navigate(['/forgotpassword']);
  }
}
