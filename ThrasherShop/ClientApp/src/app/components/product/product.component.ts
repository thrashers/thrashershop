import { ShoppingCartService } from './../../services/shopping-cart/shopping-cart.service';
import { IProductCollection, IProductMapped, IProductEntity } from './../../api/product/interfaces';
import { Subscription } from 'rxjs';
import { ProductClient } from './../../api/product/product.service';
import { Component, OnInit } from '@angular/core';
import { ApiService } from '../../api.service';
import { ActivatedRoute } from '@angular/router';
import { IProduct } from '../../api/product/interfaces';
import { WishlistClient } from './../../api/wishlist/wishlist.service';
import { AuthenticationService } from "../../services/authentication/authentication.service";
import { IWishlist, IWishlistCollection } from './../../api/wishlist/interfaces';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'product',
  templateUrl: './product.component.html',
  styleUrls: ['./product.component.css'],
  providers: [
    ProductClient,
    WishlistClient
  ]
})
export class ProductComponent implements OnInit {

    constructor(
        private api: ApiService,
        private route: ActivatedRoute,
        private productClient: ProductClient,
        private shoppingCartService: ShoppingCartService,
        private wishlistClient: WishlistClient,
        private authService: AuthenticationService,
        private toastService: ToastrService 
    ) { }

    productId: string;
    product: IProduct;
    productsObservable: Subscription;

    wishlistObservable: Subscription;
    wishlist: IWishlist[];

    getRating = function (rating: number) {
        if (rating > 6) 
            rating = 5;

        if (rating < 1)
            return "no rating available";
        else
            return '<i class="fa fa-star"></i>'.repeat(rating);
  }


    ngOnInit() {
        this.productId = this.route.snapshot.paramMap.get('productId');

        if (this.productId) {
            this.productsObservable = this.productClient.get(this.productId).subscribe((response: IProductEntity) => {
                this.product = response.resultSet;
                console.log(this.product);
            });
        } else {
            // TODO: Create a valid call to backend to return a default
            this.productsObservable = this.productClient.get('1').subscribe((response: IProductEntity) => {
                this.product = response.resultSet;
            });
        }
    }

    addProductToCart(product: IProduct, quantity: number): void {
        this.shoppingCartService.addProductToCart(product, Number(quantity));
        this.toastService.success('Successfully added the product to your shopping cart', "Success");
    }

    addProductToWishlist(productId: number): void {
      var userId = this.authService.getUserId();
      this.wishlistObservable = this.wishlistClient.addProduct(Number(userId), Number(productId)).subscribe(
        (wishlists: IWishlistCollection) => { this.wishlist = this.wishlistClient.mapWishlistCollection(wishlists.resultSet); },
          error => {}
      );
      this.toastService.success('Successfully added the product to your wishlist', "Success");
    }

}
