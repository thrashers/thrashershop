import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { AuthenticationService } from "../../services/authentication/authentication.service";
import { WishlistClient } from './../../api/wishlist/wishlist.service';
import { Subscription } from 'rxjs';
import { IWishlist, IWishlistCollection } from './../../api/wishlist/interfaces';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-wishlist',
  templateUrl: './wishlist.component.html',
  styleUrls: ['./wishlist.component.css'],
    providers: [
    WishlistClient
  ]
})
export class WishlistComponent implements OnInit {
  wishlistObservable: Subscription;
  wishlist: IWishlist[];

  constructor(
    private route: ActivatedRoute,
    private authService: AuthenticationService,
    private wishlistClient: WishlistClient,
    private toastService: ToastrService) {}

  ngOnInit() {
    //No paging necessary
    var pageNr = 1;
    var pageSize = 100;
    var userId = this.authService.getUserId();

    this.wishlistObservable = this.wishlistClient.getByUserId(userId, pageNr, pageSize).subscribe(
        (wishlists: IWishlistCollection) => { this.wishlist = this.wishlistClient.mapWishlistCollection(wishlists.resultSet); },
        error => {}
    );
  }

  // concoct an image url based on isbn number of the product
  // with a fallback if the isbn number is not available
  getImageUrl = function (isbn: string) {
    if (isbn.length === 0)
      isbn = "unknown-book";
    return "./assets/images/" + isbn + ".jpg";
  }

  deleteFromWishlist = function(productId: number): void {
      var userId = this.authService.getUserId();
      this.wishlistObservable = this.wishlistClient.deleteProduct(Number(userId), Number(productId)).subscribe(
        (wishlists: IWishlistCollection) => { this.wishlist = this.wishlistClient.mapWishlistCollection(wishlists.resultSet); },
        error => {}
      );
      this.toastService.success('Successfully removed the product from your wishlist', "Success");
  }

}
