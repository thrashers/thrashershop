import { AuthenticationService } from './../../services/authentication/authentication.service';
import { OrderClient } from './../../api/order/order.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-orders',
  templateUrl: './orders.component.html',
  styleUrls: ['./orders.component.css'],
  providers: [
    OrderClient
  ]
})
export class OrdersComponent implements OnInit {
  orders;

  requestModel = {
    PageNumber: 0,
    PageSize: 100,
    Firstname: '',
    LastName: '',
  };

  constructor(
    private orderClient: OrderClient,
  ) { }

  ngOnInit() {
    this.orderClient.filter(this.requestModel).subscribe((orders) => {
      this.orders = this.orderClient.mapOrdersCollection(orders.resultSet);
     },
      error => {}
    );
  }
}
