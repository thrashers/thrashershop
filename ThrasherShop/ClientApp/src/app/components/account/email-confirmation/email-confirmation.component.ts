import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { AuthenticationService } from "../../../services/authentication/authentication.service";

@Component({
  selector: 'app-email-confirmation',
  templateUrl: './email-confirmation.component.html',
  styleUrls: ['./email-confirmation.component.css']
})

export class EmailConfirmationComponent implements OnInit {
  public errors = [];

  constructor(private authService: AuthenticationService, private router: Router, private activatedRoute: ActivatedRoute) { }

  ngOnInit() {
    let token = this.activatedRoute.snapshot.params.token;
    this.authService.confirmEmail(token)
      .subscribe(response => {
        if (response.isSuccess) {
          this.authService.setToken(response.resultSet);
          this.router.navigate(['/']);
        } else {
          this.errors = response.errors;
        }
      });
  }
}
