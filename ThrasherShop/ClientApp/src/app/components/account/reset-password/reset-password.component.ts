import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { AuthenticationService } from "../../../services/authentication/authentication.service";

import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-reset-password',
  templateUrl: './reset-password.component.html',
  styleUrls: ['./reset-password.component.css']
})
export class ResetPasswordComponent implements OnInit {
  private token: string;

  constructor(private authService: AuthenticationService, private toastService: ToastrService, private router: Router, private activatedRoute: ActivatedRoute) { }

  ngOnInit() {
    this.token = this.activatedRoute.snapshot.params["token"];
  }

  onSubmit(form: NgForm) {
    let formData = form.value;

    if (formData.password !== formData.confirmPassword) {
      this.toastService.error("Passwords don't match", "Success");
      return;
    }

    this.authService.resetPassword(this.token, formData.password)
      .subscribe(response => {
        if (response.isSuccess) {
          this.toastService.success('Password reset successful', "Success");
          this.router.navigate(['/login']);
        } else {
          for (var error in response.errors) {
            this.toastService.error(error, 'Failed'); 
          }
        }
      });;
  }
  //resetPassword
}
