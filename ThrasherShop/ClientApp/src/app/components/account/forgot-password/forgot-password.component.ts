import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { AuthenticationService } from "../../../services/authentication/authentication.service";

import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-forgot-password',
  templateUrl: './forgot-password.component.html',
  styleUrls: ['./forgot-password.component.css']
})
export class ForgotPasswordComponent implements OnInit {

  constructor(private authService: AuthenticationService, private toastService: ToastrService) { }

  ngOnInit() {
  }

  onSubmit(form: NgForm) {
    let formData = form.value;
    this.authService.sendPasswordLink(formData.email).subscribe(response => {
      if (response.isSuccess) {
        this.toastService.success('Password recovery link has been send', "Success!");
      }
    });
  }
}
