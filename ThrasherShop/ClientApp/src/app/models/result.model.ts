export interface IResultModel<T> {
  resultSet: T;
  errors: string[];
  isSuccess: boolean;
  amountOfPages: number;
}
