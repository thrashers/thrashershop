import { IResource } from '../../../src/app/api/common/interfaces';

export class User implements IResource {
  id: number;
  email: string;
  name: string;
  role: string;
}
