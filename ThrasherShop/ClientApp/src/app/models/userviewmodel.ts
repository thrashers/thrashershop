import { IResource } from '../api/common/interfaces';

export class UserViewModel //implements  IResource
{
  id: number;
  email: string;
  firstName: string;
  lastName: string;
}
