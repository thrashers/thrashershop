export interface IListResultModel<T> {
  resultSet: T;
  amountOfPages: number;
  errors: string[];
  isSuccess: boolean;
}
