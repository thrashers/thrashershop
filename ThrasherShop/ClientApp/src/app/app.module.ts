import { OrdersComponent } from './components/orders/orders.component';
import { ApiModule } from './api/api.module';
import { ShopComponent } from './components/shop/shop.component';
import { SearchBarComponent } from './components/search-bar/search-bar.component';
import { LoginComponent } from './components/login/login.component';
import { RegisterComponent } from './components/register/register.component';
import { HomeComponent } from './components/home/home.component';
import { AppRoutingModule } from './app-routing.module';
import { NavSidebarComponent } from './components/nav-sidebar/nav-sidebar.component';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { RouterModule } from '@angular/router';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgxPaginationModule } from 'ngx-pagination';
import { ChartsModule } from 'ng2-charts';

import { ToastrModule } from 'ngx-toastr';

import { AppComponent } from './app.component';
import { ProductComponent } from './components/product/product.component';
import { FooterComponent } from './components/footer/footer.component';
import { ApiService } from './api.service';
import { WishlistComponent } from './components/wishlist/wishlist.component';
import { AuthenticationService } from './services/authentication/authentication.service';
import { LogoutComponent } from './components/logout/logout.component';
import { AuthInterceptor } from './interceptors/auth.interceptor';
import { AdminGuard } from './guards/admin.guard';
import { UserGuard } from './guards/user.guard';
import { HttpModule } from '@angular/http';
import { NotfoundComponent } from './components/notfound/notfound.component';
import { EmailConfirmationComponent } from './components/account/email-confirmation/email-confirmation.component';
import { ObservableSpinnerComponent } from './components/observable-spinner/observable-spinner.component';
import { CheckoutComponent } from './components/checkout/checkout.component';
import { Globals } from './globals';
import { ProfileComponent } from './components/account/profile/profile.component';
import { ManageUsersListComponent } from './components/admin/manage-users-list/manage-users-list.component';
import { DashboardComponent } from './components/admin/dashboard-statistics/dashboard.component';
import { UserClient } from './api/user/user.service';
import { NgxWebstorageModule } from 'ngx-webstorage';
import { ShoppingCartService } from './services/shopping-cart/shopping-cart.service';
import { ShoppingCartComponent } from './components/cart/shopping-cart.component';
import { HeaderComponent } from './components/header/header.component';
import { ForgotPasswordComponent } from './components/account/forgot-password/forgot-password.component';
import { ConfirmEmailComponent } from './components/account/confirm-email/confirm-email.component';
import { ResetPasswordComponent } from './components/account/reset-password/reset-password.component';
import { ManageProductsComponent } from './components/admin/manage-products/manage-products.component';
import { ShopinfoComponent } from './components/shopinfo/shopinfo.component';
import { CategoryComponent } from './components/category/category.component';
import { ManageVatComponent } from './components/admin/manage-vat/manage-vat.component';
import { ManageCategoriesComponent } from './components/admin/manage-categories/manage-categories.component';
import { EditUserComponent } from './components/admin/manage-users-list/edit-user/edit-user.component';

@NgModule({
  declarations: [
    AppComponent,
    NavSidebarComponent,
    HomeComponent,
    ProductComponent,
    RegisterComponent,
    LoginComponent,
    SearchBarComponent,
    ShopComponent,
    WishlistComponent,
    FooterComponent,
    LogoutComponent,
    NotfoundComponent,
    EmailConfirmationComponent,
    ObservableSpinnerComponent,
    CheckoutComponent,
    CheckoutComponent,
    ProfileComponent,
    ManageUsersListComponent,
    ShoppingCartComponent,
    ForgotPasswordComponent,
    ConfirmEmailComponent,
    ResetPasswordComponent,
    HeaderComponent,
    OrdersComponent,
    ManageProductsComponent,
    ShopinfoComponent,
    ManageVatComponent,
    ManageCategoriesComponent,
    EditUserComponent,
    DashboardComponent,
    CategoryComponent
  ],
  imports: [
    BrowserModule.withServerTransition({ appId: 'ng-cli-universal' }),
    NgxWebstorageModule.forRoot(),
    HttpClientModule,
    AppRoutingModule,
    ApiModule,
    HttpModule,
    FormsModule,
    BrowserAnimationsModule,
    ToastrModule.forRoot(),
    ChartsModule,
    NgxPaginationModule
  ],
  providers: [
    ApiService,
    AuthenticationService,
    AdminGuard,
    UserGuard,
    Globals,
    UserClient,
    { provide: HTTP_INTERCEPTORS, useClass: AuthInterceptor, multi: true },
    ShoppingCartService,
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
