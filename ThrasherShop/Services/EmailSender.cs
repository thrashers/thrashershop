﻿using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity.UI.Services;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using SendGrid;
using SendGrid.Helpers.Mail;
using ThrasherShop.Extensions;

namespace ThrasherShop.Services
{
    public class EmailSender : IEmailSender
    {
        private readonly ILogger<IEmailSender> _logger;
        private readonly IConfiguration _configuration;

        public EmailSender(ILogger<IEmailSender> logger, IConfiguration configuration)
        {
            _logger = logger;
            _configuration = configuration;
        }

        public Task SendEmailAsync(string email, string subject, string message)
        {
            return Execute(_configuration.GetSection("SendGrid").GetValue<string>("ApiKey"), subject, message, email);
        }

        private Task Execute(string apiKey, string subject, string message, string email)
        {
            _logger.LogInformation($"Sending mail using SendGrid to {email}");

            try
            {
                var client = new SendGridClient(apiKey);

                var msg = new SendGridMessage
                {
                    From = new EmailAddress("noreply@thrasher-bookshop.com", "Thrasher bookshop"),
                    Subject = subject,
                    PlainTextContent = message,
                    HtmlContent = message
                };
                msg.AddTo(new EmailAddress(email));

                // Disable click tracking.
                // See https://sendgrid.com/docs/User_Guide/Settings/tracking.html
                msg.TrackingSettings = new TrackingSettings
                {
                    ClickTracking = new ClickTracking { Enable = false }
                };

                return client.SendEmailAsync(msg);
            }
            catch (Exception e)
            {
                _logger.LogError($"Error while sending an email to {email} using SendGrid. {e.FormatExceptionMessage()}");
                throw;
            }
        }
    }
}