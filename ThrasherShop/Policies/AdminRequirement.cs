﻿using Microsoft.AspNetCore.Authorization;

namespace ThrasherShop.Policies
{
    public class AdminRequirement : IAuthorizationRequirement
    {
        public const string RoleName = "Admin";
    }
}