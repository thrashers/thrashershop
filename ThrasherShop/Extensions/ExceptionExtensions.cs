﻿using System;

namespace ThrasherShop.Extensions
{
    public static class ExceptionExtensions
    {
        public static Exception ExtractInnerException(this Exception ex)
        {
            if (ex != null)
            {
                if (ex.InnerException != null)
                {
                    return ExtractInnerException(ex.InnerException);
                }
                return ex;
            }
            return new Exception("Empty exception");
        }

        public static string FormatExceptionMessage(this Exception ex)
        {
            if (ex == null)
            {
                return "Exception is null";
            }
            return $"Message: {ex.Message}. StackTrace: {ex.StackTrace}. InnerException: {ex.ExtractInnerException().Message}";
        }
    }
}