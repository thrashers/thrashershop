﻿using System;
using System.Text.RegularExpressions;
using Microsoft.IdentityModel.Tokens;

namespace ThrasherShop.Extensions
{
    public static class StringExtensions
    {
        public static bool EqualsInvariant(this string s, string valueToCompare)
        {
            return s.Equals(valueToCompare, StringComparison.InvariantCultureIgnoreCase);
        }

        public static bool ContainsInvariant(this string s, string valueToCompare)
        {
            if (s == null)
            {
                return false;
            }
            return s.Contains(valueToCompare, StringComparison.InvariantCultureIgnoreCase);
        }

        public static bool IsValidEmail(this string s)
        {
            if (string.IsNullOrEmpty(s?.Trim()))
            {
                return false;
            }

            return new Regex(
                    @"^(?("")("".+?(?<!\\)""@)|(([0-9a-z]((\.(?!\.))|[-!#\$%&'\*\+/=\?\^`{}|~\w])*)(?<=[0-9a-z])@))(?([)([(\d{1,3}.){3}\d{1,3}])|(([0-9a-z][-0-9a-z]*[0-9a-z]*.)+[a-z0-9][-a-z0-9]{0,22}[a-z0-9]))$")
                .IsMatch(s);
        }

        public static int GetUserIdFromToken(this string token)
        {
            var decoded = Base64UrlEncoder.Decode(token);
            var splitted = decoded.Split("/", StringSplitOptions.RemoveEmptyEntries);

            return int.Parse(splitted[0]);
        }
    }
}