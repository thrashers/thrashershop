﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity.UI.Services;

namespace ThrasherShop.Extensions
{
    public static class EmailSenderExtensions
    {
        public static Task SendEmailConfirmationAsync(this IEmailSender emailSender, string email, string url, string confirmationCode)
        {
            return emailSender.SendEmailAsync(email, "Confirm your email",
                $"Please confirm your e-mail by clicking on the following link: <a href=\"{url}/{confirmationCode}\">Click here</a>");
        }

        public static Task SendPasswordRecoveryLinkAsync(this IEmailSender emailSender, string email, string url, string passwordToken)
        {
            return emailSender.SendEmailAsync(email, "Your password recovery link",
                $"Reset your password by clicking on the following link: <a href=\"{url}/{passwordToken}\">Click here</a>");
        }
    }
}