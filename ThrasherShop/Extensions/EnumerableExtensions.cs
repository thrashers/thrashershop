﻿using System;
using System.Collections.Generic;

namespace ThrasherShop.Extensions
{
    public static class EnumerableExtensions
    {
        public static IEnumerable<TOutput> ConvertAll<T, TOutput>(this IEnumerable<T> enumerable, Func<T, TOutput> func)
        {
            foreach (var item in enumerable)
            {
                yield return func(item);
            }
        }
    }
}