﻿using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Logging;
using Microsoft.IdentityModel.Tokens;
using ThrasherShop.Models.DbModels;
using ThrasherShop.Repositories;

namespace ThrasherShop.Extensions
{
    public static class UserTokenRepositoryExtensions
    {
        public static string GenerateThrasherEmailConfirmationToken(this IUserTokenRepository repository, int userId)
        {
            var userToken = new UserToken
            {
                UserId = userId,
                DateTime = DateTime.Now,
                TokenType = TokenType.Email,
                Token = Base64UrlEncoder.Encode($"{userId}/{DateTimeOffset.UtcNow:yyyyMMddHHmmssffff}")
            };
            repository.Save(userToken);
            return userToken.Token;
        }

        public static async Task<string> GenerateThrasherPasswordRestoreToken(this IUserTokenRepository repository, int userId)
        {
            var latestToken = await repository.GetLastNotUsedTokenByUser(userId, TokenType.Password).ConfigureAwait(false);
            var userToken = latestToken ?? new UserToken
            {
                UserId = userId,
                DateTime = DateTime.Now,
                TokenType = TokenType.Password,
                Token = Base64UrlEncoder.Encode($"{userId}/{DateTimeOffset.UtcNow:yyyyMMddHHmmssffff}")
            };

            repository.Save(userToken);
            return userToken.Token;
        }

        public static async Task<bool> ValidatePasswordRestoreToken(this IUserTokenRepository repository, UserManager<User> userManager, ILogger logger,
            string token)
        {
            var userId = token.GetUserIdFromToken();

            var userToken = await repository.GetUserToken(userId, token, TokenType.Password).ConfigureAwait(false);
            if (userToken == null)
            {
                logger.LogInformation($"Password restore token {token} not found.");
                return false;
            }

            var user = await userManager.FindByIdAsync(userId.ToString()).ConfigureAwait(false);
            if (user == null)
            {
                logger.LogInformation($"User ID {userId}. Not found for token {token}");
                return false;
            }

            userToken.IsUsed = true;
            repository.Save(userToken);
            return true;
        }

        public static async Task<User> ConfirmEmail(this IUserTokenRepository repository, UserManager<User> userManager, string token)
        {
            var userId = token.GetUserIdFromToken();

            var userToken = await repository.GetUserToken(userId, token, TokenType.Email).ConfigureAwait(false);
            if (userToken == null)
            {
                throw new Exception($"Email confirmation token {token} not found.");
            }

            if (userToken.IsUsed)
            {
                throw new Exception($"Token {token} already used.");
            }

            var user = await userManager.FindByIdAsync(userId.ToString()).ConfigureAwait(false);
            if (user == null)
            {
                throw new Exception($"User ID {userId}. Not found for token {token}");
            }

            if (user.EmailConfirmed)
            {
                throw new Exception($"User email {user.Email} already confirmed.");
            }

            userToken.IsUsed = true;
            repository.Save(userToken);

            user.EmailConfirmed = true;
            await userManager.UpdateAsync(user).ConfigureAwait(false);
            return user;
        }
    }
}