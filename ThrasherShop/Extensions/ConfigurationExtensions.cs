﻿using Microsoft.Extensions.Configuration;

namespace ThrasherShop.Extensions
{
    public static class ConfigurationExtensions
    {
        public static string GetValueFromJwtSection(this IConfiguration config, string key)
        {
            return config.GetSection("JwtToken").GetValue<string>(key);
        }
    }
}