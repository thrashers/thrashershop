using System;
using System.Globalization;
using System.IO;
using System.Reflection;
using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Serilog;
using Serilog.Events;
using ThrasherShop.Database;
using ThrasherShop.Extensions;

namespace ThrasherShop
{
    public class Program
    {
        public static void Main(string[] args)
        {
            Log.Logger = new LoggerConfiguration()
                .WriteTo.Async(action => action.File($@"{Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location)}\logs\{DateTime.UtcNow.ToString("yyyy-MM-dd", CultureInfo.InvariantCulture)}_log_Debug.txt",
                    LogEventLevel.Debug,
                    fileSizeLimitBytes: null))
                .WriteTo.Async(action => action.File($@"{Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location)}\logs\{DateTime.UtcNow.ToString("yyyy-MM-dd", CultureInfo.InvariantCulture)}_log_Info.txt",
                    LogEventLevel.Information,
                    fileSizeLimitBytes: null))
                .WriteTo.Async(action => action.File($@"{Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location)}\logs\{DateTime.UtcNow.ToString("yyyy-MM-dd", CultureInfo.InvariantCulture)}_log_Errors.txt",
                    LogEventLevel.Error,
                    fileSizeLimitBytes: null))
                .CreateLogger();
            try
            {
                var host = CreateWebHostBuilder(args).Build();
                DatabaseInitializer.InitializeAndSeedDatabase(host.Services);
                host.Run();
            }
            catch (Exception e)
            {
                Log.Error(e, e.FormatExceptionMessage());
                throw;
            }
            finally
            {
                Log.CloseAndFlush();
            }
        }

        public static IWebHostBuilder CreateWebHostBuilder(string[] args) =>
            WebHost.CreateDefaultBuilder(args)
                .UseStartup<Startup>()
                .ConfigureAppConfiguration((hostContext, config) =>
                {
                    config.Sources.Clear();
                    config.AddJsonFile("appsettings.json");
                    config.AddJsonFile($"appsettings.{hostContext.HostingEnvironment.EnvironmentName}.json", optional: false, reloadOnChange: true);
                })
                .UseSerilog();
    }
}
