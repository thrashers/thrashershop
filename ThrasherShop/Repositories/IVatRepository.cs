﻿using System.Threading.Tasks;
using ThrasherShop.Models.DbModels;
using ThrasherShop.Models.RequestModels;
using ThrasherShop.Models.ResultModels;
using System.Collections.Generic;

namespace ThrasherShop.Repositories
{
    public interface IVatRepository : IRepository<Vat>
    {
        Task<DbResultModel<Vat>> Filter(VatRequestModel requestModel);

        Task<IEnumerable<Vat>> GetAll();

        Task<IEnumerable<Vat>> GetVatsByDescription(string description);
    }
}