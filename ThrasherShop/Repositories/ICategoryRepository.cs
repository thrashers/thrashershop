﻿using System.Threading.Tasks;
using ThrasherShop.Models.DbModels;
using ThrasherShop.Models.RequestModels;
using ThrasherShop.Models.ResultModels;
using System.Collections.Generic;

namespace ThrasherShop.Repositories
{
    public interface ICategoryRepository : IRepository<Category>
    {
        Task<DbResultModel<Category>> Filter(CategoryRequestModel requestModel);
        Task<IEnumerable<Category>> GetCategories();
        Task<IEnumerable<Category>> GetCategoriesByDescription(string description);
    }
}