﻿using ThrasherShop.Models.DbModels;

namespace ThrasherShop.Repositories
{
    internal interface IRoleRepository : IRepository<Role>
    {
    }
}