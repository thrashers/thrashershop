﻿using System.Threading.Tasks;
using ThrasherShop.Models.DbModels;
using ThrasherShop.Models.RequestModels;
using ThrasherShop.Models.ResultModels;

namespace ThrasherShop.Repositories
{
    public interface IUserRepository : IRepository<User>
    {
        Task<DbResultModel<User>> Filter(UserRequestModel requestModel);
    }
}