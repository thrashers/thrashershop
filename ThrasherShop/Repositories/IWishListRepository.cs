﻿using System.Threading.Tasks;
using ThrasherShop.Models.DbModels;
using ThrasherShop.Models.ResultModels;

namespace ThrasherShop.Repositories
{
    public interface IWishListRepository : IRepository<WishList>
    {
        Task<DbResultModel<WishList>> GetByUser(int userId, int pageNumber, int pageSize);
        void Remove(int userId, int productId);
    }
}