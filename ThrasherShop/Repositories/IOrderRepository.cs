﻿using System.Threading.Tasks;
using ThrasherShop.Models.DbModels;
using ThrasherShop.Models.RequestModels;
using ThrasherShop.Models.ResultModels;

namespace ThrasherShop.Repositories
{
    public interface IOrderRepository : IRepository<Order>
    {
        Task<DbResultModel<Order>> GetByUser(int pageNumber, int pageSize, int userId);
        Task<DbResultModel<Order>> Filter(OrderRequestModel requestModel);
    }
}