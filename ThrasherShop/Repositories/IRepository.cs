﻿using System.Threading.Tasks;
using ThrasherShop.Models.DbModels;

namespace ThrasherShop.Repositories
{
    public interface IRepository<T> where T : class, IEntity
    {
        Task<T> Get(int id);
        void Save(T entity);
        Task Remove(int id);
    }
}