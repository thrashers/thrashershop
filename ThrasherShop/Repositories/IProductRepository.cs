﻿using System.Threading.Tasks;
using ThrasherShop.Models.DbModels;
using ThrasherShop.Models.RequestModels;
using ThrasherShop.Models.ResultModels;

namespace ThrasherShop.Repositories
{
    public interface IProductRepository : IRepository<Product>
    {
        Task<DbResultModel<Product>> Filter(ProductRequestModel requestModel);
        Task<DbResultModel<Product>> GetBestSelling(int pageNumber, int pageSize);
        Task<DbResultModel<Product>> GetByCategory(int pageNumber, int pageSize, int categoryId);
    }
}