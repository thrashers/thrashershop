﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using ThrasherShop.Database;
using ThrasherShop.Models.DbModels;

namespace ThrasherShop.Repositories.Implementations
{
    internal class Repository<TEntity> : IRepository<TEntity> where TEntity : class, IEntity
    {
        protected readonly ThrasherDbContext DbContext;
        protected readonly DbSet<TEntity> DbSet;

        protected Repository(ThrasherDbContext dbContext)
        {
            DbContext = dbContext;
            DbSet = dbContext.Set<TEntity>();
        }

        /// <summary>
        /// Set pagination on the list. Needs to be moved to some kind of repository
        /// </summary>
        /// <typeparam name="T">The type the list holds. Just needed it because there are some methods that require a generic list</typeparam>
        /// <param name="list">The list that needs to be cut</param>
        /// <param name="pageNumber">The page number</param>
        /// <param name="pageSize">The maximum amount of records that should be returned</param>
        /// <returns>A list with a maximum amount of <see cref="pageSize"/></returns>
        protected async Task<IEnumerable<T>> SkipAndTakeList<T>(IQueryable<T> list, int pageNumber, int pageSize)
        {
            if (pageNumber <= 0)
            {
                pageNumber = 1;
            }
            if (pageSize <= 0)
            {
                pageSize = 1;
            }
            return await list.Skip((pageNumber - 1) * pageSize).Take(pageSize).ToListAsync().ConfigureAwait(false);
        }

        /// <summary>
        /// Determined the amount of pages the list contains based on the <see cref="pageSize"/> indicating the maximum amount of records.
        /// </summary>
        /// <param name="totalCount">The total count of the list</param>
        /// <param name="pageSize">The maximum amount of records that should be returned</param>
        /// <returns>The amount of pages based on the <see cref="totalCount"/> and the <see cref="pageSize"/> indicating the maximum count of the list</returns>
        protected int DetermineAmountOfPages(int totalCount, int pageSize)
        {
            if (totalCount <= 0)
            {
                totalCount = 1;
            }
            if (pageSize <= 0)
            {
                pageSize = 1;
            }
            return Convert.ToInt32(Math.Ceiling(totalCount / (decimal)pageSize));
        }

        public virtual async Task<TEntity> Get(int id)
        {
            return await DbSet.FindAsync(id);
        }

        protected IQueryable<TEntity> Filter(Expression<Func<TEntity, bool>> expression)
        {
            return DbSet.Where(expression);
        }

        public void Save(TEntity entity)
        {
            if (entity.Id > 0)
            {
                DbSet.Update(entity);
            }
            else
            {
                DbSet.Add(entity);
            }
            DbContext.SaveChanges();
        }

        public async Task Remove(int id)
        {
            var entity = await Get(id).ConfigureAwait(false);
            DbSet.Remove(entity);
            DbContext.SaveChanges();
        }
    }
}