﻿using Microsoft.Extensions.Logging;
using ThrasherShop.Database;
using ThrasherShop.Models.DbModels;

namespace ThrasherShop.Repositories.Implementations
{
    internal class RoleRepository : Repository<Role>, IRoleRepository
    {
        public RoleRepository(ThrasherDbContext dbContext, ILogger<RoleRepository> logger) : base(dbContext)
        {
        }
    }
}