﻿using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using ThrasherShop.Database;
using ThrasherShop.Extensions;
using ThrasherShop.Models.DbModels;
using ThrasherShop.Models.RequestModels;
using ThrasherShop.Models.ResultModels;
using System.Collections.Generic;
using System.Linq;

namespace ThrasherShop.Repositories.Implementations
{
    internal class VatRepository : Repository<Vat>, IVatRepository
    {
        public VatRepository(ThrasherDbContext dbContext, ILogger<VatRepository> logger) : base(dbContext)
        {
        }

        public async Task<DbResultModel<Vat>> Filter(VatRequestModel requestModel)
        {
            var vats= Filter(e => e.Description.ContainsInvariant(requestModel.Description) &&
                               (!requestModel.Tax.HasValue || e.Tax == requestModel.Tax));

            return new DbResultModel<Vat>(
                DetermineAmountOfPages(await vats.CountAsync().ConfigureAwait(false), requestModel.PageSize),
                await SkipAndTakeList(vats, requestModel.PageNumber, requestModel.PageSize).ConfigureAwait(false));
        }

        public async Task<IEnumerable<Vat>> GetAll()
        {
            return await DbSet.OrderByDescending(c => c.Tax).ToListAsync().ConfigureAwait(false);
        }

        public async Task<IEnumerable<Vat>> GetVatsByDescription(string description)
        {
            return await DbSet
                .Where(v => v.Description == description)
                .OrderBy(v => v.Description)
                .ToListAsync()
                .ConfigureAwait(false);
        }
    }
}