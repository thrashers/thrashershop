﻿using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using ThrasherShop.Database;
using ThrasherShop.Models.DbModels;
using ThrasherShop.Models.ResultModels;


namespace ThrasherShop.Repositories.Implementations
{
    internal class WishListRepository : Repository<WishList>, IWishListRepository
    {
        public WishListRepository(ThrasherDbContext dbContext, ILogger<WishListRepository> logger) : base(dbContext)
        {
        }

        public async Task<DbResultModel<WishList>> GetByUser(int userId, int pageNumber, int pageSize)
        {
            var query = DbSet.Where(e => e.UserId == userId)
                .Include(w => w.Product).ThenInclude(p => p.ProductAuthors).ThenInclude(a => a.Author);

            return new DbResultModel<WishList>(
                DetermineAmountOfPages(query.Count(), pageSize),
                await SkipAndTakeList(query, pageNumber, pageSize).ConfigureAwait(false)
                );
        }

        public void Remove(int userId, int productId)
        {
            var wishlist = DbSet.FirstOrDefault(e => e.UserId == userId && e.ProductId == productId);
            if (wishlist == null)
            {
                return;
            }
            DbSet.Remove(wishlist);
            DbContext.SaveChanges();
        }
    }
}