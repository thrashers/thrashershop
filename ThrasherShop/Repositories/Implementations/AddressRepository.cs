﻿using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using ThrasherShop.Database;
using ThrasherShop.Extensions;
using ThrasherShop.Models.DbModels;
using ThrasherShop.Models.RequestModels;
using ThrasherShop.Models.ResultModels;

namespace ThrasherShop.Repositories.Implementations
{
    internal class AddressRepository : Repository<Address>, IAddressRepository
    {
        public AddressRepository(ThrasherDbContext dbContext, ILogger<AddressRepository> logger) : base(dbContext)
        {
        }

        public async Task<DbResultModel<Address>> Filter(AddressRequestModel requestModel)
        {
            var addresses = Filter(e => e.AddressLine1.ContainsInvariant(requestModel.AddressLine1) ||
                                        e.AddressLine2.ContainsInvariant(requestModel.AddressLine2) ||
                                        e.AddressLine3.ContainsInvariant(requestModel.AddressLine3) ||
                                        e.AddressLine4.ContainsInvariant(requestModel.AddressLine4));
            return new DbResultModel<Address>(
                DetermineAmountOfPages(await addresses.CountAsync().ConfigureAwait(false), requestModel.PageSize),
                await SkipAndTakeList(addresses, requestModel.PageNumber, requestModel.PageSize).ConfigureAwait(false)
            );
        }
    }
}