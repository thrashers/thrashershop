﻿using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using ThrasherShop.Database;
using ThrasherShop.Extensions;
using ThrasherShop.Models.DbModels;
using ThrasherShop.Models.RequestModels;
using ThrasherShop.Models.ResultModels;

namespace ThrasherShop.Repositories.Implementations
{
    internal class UserRepository : Repository<User>, IUserRepository
    {
        public UserRepository(ThrasherDbContext dbContext, ILogger<UserRepository> logger) : base(dbContext)
        {
        }

        public async Task<DbResultModel<User>> Filter(UserRequestModel requestModel)
        {
            var users = Filter(e => e.Email.ContainsInvariant(requestModel.Email) &&
                                    e.FirstName.ContainsInvariant(requestModel.FirstName) &&
                                    e.LastName.ContainsInvariant(requestModel.LastName));

            int totalAmount = await users.CountAsync().ConfigureAwait(false);
            return new DbResultModel<User>(
                DetermineAmountOfPages(totalAmount, requestModel.PageSize),
                await SkipAndTakeList(users, requestModel.PageNumber, requestModel.PageSize)
            )
            {
                TotalAmount = totalAmount
            };
        }
    }
}