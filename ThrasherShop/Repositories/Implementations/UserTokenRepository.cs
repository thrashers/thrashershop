﻿using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using ThrasherShop.Database;
using ThrasherShop.Models.DbModels;

namespace ThrasherShop.Repositories.Implementations
{
    internal class UserTokenRepository : Repository<UserToken>, IUserTokenRepository
    {
        public UserTokenRepository(ThrasherDbContext dbContext) : base(dbContext)
        {
        }

        public async Task<UserToken> GetLastNotUsedTokenByUser(int userId, TokenType tokenType)
        {
            return await DbSet
                .FirstOrDefaultAsync(e => e.UserId == userId && e.TokenType == tokenType && !e.IsUsed)
                .ConfigureAwait(false);
        }

        public async Task<UserToken> GetUserToken(int userId, string token, TokenType tokenType)
        {
            return await DbSet
                .FirstOrDefaultAsync(e => e.UserId == userId && e.Token == token && e.TokenType == tokenType && !e.IsUsed)
                .ConfigureAwait(false);
        }
    }
}