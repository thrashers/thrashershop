﻿using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using ThrasherShop.Database;
using ThrasherShop.Extensions;
using ThrasherShop.Models.DbModels;
using ThrasherShop.Models.RequestModels;
using ThrasherShop.Models.ResultModels;

namespace ThrasherShop.Repositories.Implementations
{
    internal class TagRepository : Repository<Tag>, ITagRepository
    {
        public TagRepository(ThrasherDbContext dbContext, ILogger<TagRepository> logger) : base(dbContext)
        {
        }

        public async Task<DbResultModel<Tag>> Filter(TagRequestModel requestModel)
        {
            var tags = Filter(e => e.TagName.ContainsInvariant(requestModel.TagName));

            return new DbResultModel<Tag>(
                DetermineAmountOfPages(await tags.CountAsync().ConfigureAwait(false), requestModel.PageSize),
                await SkipAndTakeList(tags, requestModel.PageNumber, requestModel.PageSize).ConfigureAwait(false)
            );
        }
    }
}