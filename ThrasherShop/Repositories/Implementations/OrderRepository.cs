﻿using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using ThrasherShop.Database;
using ThrasherShop.Extensions;
using ThrasherShop.Models.DbModels;
using ThrasherShop.Models.RequestModels;
using ThrasherShop.Models.ResultModels;

namespace ThrasherShop.Repositories.Implementations
{
    internal class OrderRepository : Repository<Order>, IOrderRepository
    {
        public OrderRepository(ThrasherDbContext dbContext, ILogger<OrderRepository> logger) : base(dbContext)
        {
        }

        public async Task<DbResultModel<Order>> GetByUser(int pageNumber, int pageSize, int userId)
        {
            var orders = DbSet.Where(e => e.OrderedById == userId);
            return new DbResultModel<Order>(
                DetermineAmountOfPages(await orders.CountAsync().ConfigureAwait(false), pageSize),
                await SkipAndTakeList(orders, pageNumber, pageSize).ConfigureAwait(false)
            );
        }

        public async Task<DbResultModel<Order>> Filter(OrderRequestModel requestModel)
        {
            var orders = Filter(e => e.FirstName.ContainsInvariant(requestModel.FirstName) &&
                                     e.LastName.ContainsInvariant(requestModel.LastName) &&
                                     e.Email.ContainsInvariant(requestModel.Email) &&
                                     e.AddressLine1.ContainsInvariant(requestModel.AddressLine1) &&
                                     e.HouseNumber.ContainsInvariant(requestModel.HouseNumber) &&
                                     e.ZipCode.ContainsInvariant(requestModel.ZipCode) &&
                                     e.City.ContainsInvariant(requestModel.City) &&
                                     e.Country.ContainsInvariant(requestModel.Country) &&
                                     (!requestModel.OrderDateFrom.HasValue ||
                                      e.OrderedOn.Date >= requestModel.OrderDateFrom.Value.Date) &&
                                     (!requestModel.OrderDateTo.HasValue ||
                                      e.OrderedOn.Date <= requestModel.OrderDateTo.Value.Date)
            );

            return new DbResultModel<Order>(
                DetermineAmountOfPages(await orders.CountAsync().ConfigureAwait(false), requestModel.PageSize),
                await SkipAndTakeList(orders, requestModel.PageNumber, requestModel.PageSize).ConfigureAwait(false)
            );
        }
    }
}