﻿using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using ThrasherShop.Database;
using ThrasherShop.Extensions;
using ThrasherShop.Models.DbModels;
using ThrasherShop.Models.RequestModels;
using ThrasherShop.Models.ResultModels;

namespace ThrasherShop.Repositories.Implementations
{
    internal class ProductRepository : Repository<Product>, IProductRepository
    {
        public ProductRepository(ThrasherDbContext dbContext, ILogger<ProductRepository> logger) : base(dbContext)
        {
        }

        public override async Task<Product> Get(int id)
        {
            return await DbSet.Where(c=> c.Id == id)
                .Include(p => p.Category)
                .Include(p => p.Vat)
                .Include(p => p.UserRatings)
                .Include(p => p.ProductAuthors).ThenInclude(a => a.Author)
                .FirstOrDefaultAsync()
                .ConfigureAwait(false);
        }
        
        public async Task<DbResultModel<Product>> Filter(ProductRequestModel requestModel)
        {
            var products = Filter(e => (requestModel.CategoryId == 0 || e.CategoryId == requestModel.CategoryId) &&
                                       e.Name.ContainsInvariant(requestModel.Name) &&
                                       e.Description.ContainsInvariant(requestModel.Description) &&
                                       (requestModel.VatId == 0 || e.VatId == requestModel.VatId) &&
                                       (!requestModel.PriceWithoutVat.HasValue || e.PriceWithoutVat == requestModel.PriceWithoutVat) &&
                                       (!requestModel.PriceWithVat.HasValue || e.PriceWithVat == requestModel.PriceWithVat));

            return new DbResultModel<Product>(
                DetermineAmountOfPages(await products.CountAsync().ConfigureAwait(false), requestModel.PageSize),
                await SkipAndTakeList(products, requestModel.PageNumber, requestModel.PageSize).ConfigureAwait(false)
            );
        }

        public async Task<DbResultModel<Product>> GetBestSelling(int pageNumber, int pageSize)
        {
            var products = DbContext.OrderLines.GroupBy(orderLine => orderLine.Product)
                .OrderByDescending(orderProduct => orderProduct.Count())
                .Select(orderProduct => orderProduct.Key).Include(p => p.Vat);

            return new DbResultModel<Product>(
                DetermineAmountOfPages(await products.CountAsync().ConfigureAwait(false), pageSize),
                await SkipAndTakeList(products, pageNumber, pageSize).ConfigureAwait(false)
            );
        }

        public async Task<DbResultModel<Product>> GetByCategory(int pageNumber, int pageSize, int categoryId)
        {
            var products = DbSet.Where(e => e.CategoryId == categoryId)
                .Include(p => p.Category)
                .Include(p => p.ProductAuthors).ThenInclude(a => a.Author)
                .Include(p => p.Vat)
                .OrderBy(e => e.Name);

            return new DbResultModel<Product>(
                DetermineAmountOfPages(products.Count(), pageSize),
                await SkipAndTakeList(products, pageNumber, pageSize).ConfigureAwait(false)
                );
        }

        public Product SaveProduct(Product productToSave)
        {


            return productToSave;
        }
    }
}