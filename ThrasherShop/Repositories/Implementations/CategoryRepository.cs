﻿using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using ThrasherShop.Database;
using ThrasherShop.Extensions;
using ThrasherShop.Models.DbModels;
using ThrasherShop.Models.RequestModels;
using ThrasherShop.Models.ResultModels;
using System.Collections.Generic;
using System.Linq;

namespace ThrasherShop.Repositories.Implementations
{
    internal class CategoryRepository : Repository<Category>, ICategoryRepository
    {
        public CategoryRepository(ThrasherDbContext dbContext, ILogger<CategoryRepository> logger) : base(dbContext)
        {
        }

        public override async Task<Category> Get(int id)
        {
            return await DbSet.Where(c => c.Id == id)
                .Include(c => c.Products)
                .FirstOrDefaultAsync()
                .ConfigureAwait(false);
        }

        public async Task<DbResultModel<Category>> Filter(CategoryRequestModel requestModel)
        {
            var categories = Filter(e => e.Description.ContainsInvariant(requestModel.Description) &&
                                         (!requestModel.ParentCategory.HasValue || e.ParentId == requestModel.ParentCategory)
            );

            return new DbResultModel<Category>(
                DetermineAmountOfPages(categories.Count(), requestModel.PageSize),
                await SkipAndTakeList(categories, requestModel.PageNumber, requestModel.PageSize).ConfigureAwait(false)
            );
        }

        public async Task<IEnumerable<Category>> GetCategories()
        {
            return await DbSet.OrderBy(c => c.Description).ToListAsync().ConfigureAwait(false);
        }

        public async Task<IEnumerable<Category>> GetCategoriesByDescription(string description)
        {
            return await DbSet
                .Where(c => c.Description == description)
                .OrderBy(c => c.Description)
                .ToListAsync()
                .ConfigureAwait(false);
        }
    }
}