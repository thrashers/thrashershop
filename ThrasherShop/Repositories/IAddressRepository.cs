﻿using System.Threading.Tasks;
using ThrasherShop.Models.DbModels;
using ThrasherShop.Models.RequestModels;
using ThrasherShop.Models.ResultModels;

namespace ThrasherShop.Repositories
{
    public interface IAddressRepository : IRepository<Address>
    {
        Task<DbResultModel<Address>> Filter(AddressRequestModel requestModel);
    }
}