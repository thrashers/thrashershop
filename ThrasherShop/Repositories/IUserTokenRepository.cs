﻿using System.Threading.Tasks;
using ThrasherShop.Models.DbModels;

namespace ThrasherShop.Repositories
{
    public interface IUserTokenRepository : IRepository<UserToken>
    {
        Task<UserToken> GetLastNotUsedTokenByUser(int userId, TokenType tokenType);
        Task<UserToken> GetUserToken(int userId, string token, TokenType tokenType);
    }
}