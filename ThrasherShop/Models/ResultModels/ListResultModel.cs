﻿using System.Collections.Generic;
using Microsoft.EntityFrameworkCore.Internal;

namespace ThrasherShop.Models.ResultModels
{
    public class ListResultModel<T> : IResultModel
    {
        public int TotalAmount { get; set; }
        public int AmountOfPages { get; set; }
        public bool IsSuccess => !Errors.Any();
        public ICollection<string> Errors { get; } = new List<string>();
        public IEnumerable<T> ResultSet { get; set; } = new List<T>();
    }
}