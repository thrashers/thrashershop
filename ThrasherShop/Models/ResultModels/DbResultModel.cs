﻿using System.Collections.Generic;
using Microsoft.EntityFrameworkCore.Internal;

namespace ThrasherShop.Models.ResultModels
{
    public class DbResultModel<T> : IResultModel
    {
        public DbResultModel(int amountOfPages, IEnumerable<T> resultSet)
        {
            AmountOfPages = amountOfPages;
            ResultSet = resultSet;
        }

        public int TotalAmount { get; set; }
        public bool IsSuccess => !Errors.Any();
        public int AmountOfPages { get; }
        public IEnumerable<T> ResultSet { get; }
        public ICollection<string> Errors { get; } = new List<string>();
    }
}