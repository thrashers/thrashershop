﻿using System.Collections.Generic;
using Microsoft.EntityFrameworkCore.Internal;

namespace ThrasherShop.Models.ResultModels
{
    public class ResultModel<T> : IResultModel
    {
        public bool IsSuccess => !Errors.Any();
        public ICollection<string> Errors { get; } = new List<string>();
        public T ResultSet { get; set; }
    }
}