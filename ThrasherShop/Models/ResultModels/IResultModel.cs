﻿using System.Collections.Generic;

namespace ThrasherShop.Models.ResultModels
{
    public interface IResultModel
    {
        bool IsSuccess { get; }
        ICollection<string> Errors { get; }
    }
}