﻿namespace ThrasherShop.Models
{
    public class ClaimTypeConstants
    {
        public const string Email = "email";
        public const string Name = "name";
        public const string Identifier = "id";
        public const string Role = "role";
    }
}