﻿using ThrasherShop.Models.DbModels;

namespace ThrasherShop.Models.ViewModels
{
    public class WishListViewModel
    {
        public WishListViewModel()
        {
        }

        public WishListViewModel(WishList wishList) : this()
        {
            Id = wishList.Id;
            ConcurrencyStamp = wishList.ConcurrencyStamp;
            UserId = wishList.UserId;
            ProductId = wishList.ProductId;

            if (wishList.User != null)
                User = new UserViewModel(wishList.User);

            if (wishList.Product != null)
                Product = new ProductViewModel(wishList.Product);
        }

        public int Id { get; set; }
        public string ConcurrencyStamp { get; set; }
        public int UserId { get; set; }
        public UserViewModel User { get; set; }
        public int ProductId { get; set; }
        public ProductViewModel Product { get; set; }
    }
}