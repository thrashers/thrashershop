﻿using ThrasherShop.Models.DbModels;

namespace ThrasherShop.Models.ViewModels
{
    public class UserRatingViewModel
    {
        public UserRatingViewModel(UserRating rating)
        {
            Id = rating.Id;
            ConcurrencyStamp = rating.ConcurrencyStamp;
            ProductId = rating.ProductId;
            Product = rating.Product;
            UserId = rating.UserId;
            User = rating.User;
            Rating = rating.Rating;
        }

        public int Id { get; set; }
        public string ConcurrencyStamp { get; set; }
        public int ProductId { get; set; }
        public Product Product { get; set; }
        public int UserId { get; set; }
        public User User { get; set; }
        public int Rating { get; set; }
    }
}