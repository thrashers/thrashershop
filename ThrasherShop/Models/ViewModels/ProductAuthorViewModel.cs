﻿using ThrasherShop.Models.DbModels;

namespace ThrasherShop.Models.ViewModels
{
    public class ProductAuthorViewModel 
    {
        public ProductAuthorViewModel(ProductAuthor productAuthor) 
        {
            ProductId = productAuthor.ProductId;
            AuthorId = productAuthor.AuthorId;
            ConcurrencyStamp = productAuthor.ConcurrencyStamp;
        }

        public int ProductId { get; set; }
        public int AuthorId { get; set; }
        public string ConcurrencyStamp { get; set; }
    }
}
