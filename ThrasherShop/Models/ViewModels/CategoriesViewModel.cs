﻿using System.Collections.Generic;
using ThrasherShop.Models.DbModels;

namespace ThrasherShop.Models.ViewModels
{
    public class CategoriesViewModel
    {
        public CategoriesViewModel(IEnumerable<Category> categories) 
        {
            Categories = new List<CategoryViewModel>();

            foreach(Category category in categories)
            {
                Categories.Add(new CategoryViewModel(category));
            }
        }

        public List<CategoryViewModel> Categories { get; set; }
    }
}
