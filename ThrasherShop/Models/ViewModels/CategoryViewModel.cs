﻿using ThrasherShop.Models.DbModels;

namespace ThrasherShop.Models.ViewModels
{
    public class CategoryViewModel : IEntity
    {
        public CategoryViewModel()
        {
        }

        public CategoryViewModel(Category category) : this()
        {
            Id = category.Id;
            Description = category.Description;
            ConcurrencyStamp = category.ConcurrencyStamp;
            ParentId = category.ParentId;
        }

        public int Id { get; set; }
        public string ConcurrencyStamp { get; set; }
        public string Description { get; set; }
        public int? ParentId { get; set; }
        public CategoryViewModel Parent { get; set; }

        public Category ToEntity()
        {
            return new Category
            {
                Id = Id,
                ConcurrencyStamp = ConcurrencyStamp,
                Description = Description,
                ParentId = ParentId
            };
        }
    }
}