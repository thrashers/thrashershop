﻿using ThrasherShop.Models.DbModels;

namespace ThrasherShop.Models.ViewModels
{
    public class AddressViewModel
    {
        public AddressViewModel()
        {
        }

        public AddressViewModel(Address address) : this()
        {
            Id = address.Id;
            ConcurrencyStamp = address.ConcurrencyStamp;
            AddressLine1 = address.AddressLine1;
            AddressLine2 = address.AddressLine2;
            AddressLine3 = address.AddressLine3;
            AddressLine4 = address.AddressLine4;
            HouseNumber = address.HouseNumber;
            ZipCode = address.City;
            City = address.City;
            Country = address.Country;
            UserId = address.UserId;
            User = address.User;
        }

        public int Id { get; set; }
        public string ConcurrencyStamp { get; set; }
        public string AddressLine1 { get; set; }
        public string AddressLine2 { get; set; }
        public string AddressLine3 { get; set; }
        public string AddressLine4 { get; set; }
        public string HouseNumber { get; set; }
        public string ZipCode { get; set; }
        public string City { get; set; }
        public string Country { get; set; }
        public int UserId { get; set; }
        public User User { get; set; }

        public Address ToEntity()
        {
            return new Address
            {
                Id = Id,
                ConcurrencyStamp = ConcurrencyStamp,
                AddressLine1 = AddressLine1,
                AddressLine2 = AddressLine2,
                AddressLine3 = AddressLine3,
                AddressLine4 = AddressLine4,
                HouseNumber = HouseNumber,
                ZipCode = ZipCode,
                City = City,
                Country = Country,
                UserId = UserId
            };
        }
    }
}