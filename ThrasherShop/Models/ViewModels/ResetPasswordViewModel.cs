﻿using System.ComponentModel.DataAnnotations;

namespace ThrasherShop.Models.ViewModels
{
    public class ResetPasswordViewModel
    {
        [Required]
        public string Token { get; set; }

        [Required]
        public string Password { get; set; }
    }
}