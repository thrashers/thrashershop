﻿using ThrasherShop.Models.DbModels;
using System.Linq;
using System;

namespace ThrasherShop.Models.ViewModels
{
    public class ProductViewModel
    {
        public ProductViewModel()
        {
        }

        public ProductViewModel(Product product) : this()
        {
            Id = product.Id;
            ConcurrencyStamp = product.ConcurrencyStamp;
            Name = product.Name;
            Description = product.Description;
            PriceWithoutVat = product.PriceWithoutVat;
            PriceWithVat = product.PriceWithVat;
            InStock = product.InStock;
            CategoryId = product.CategoryId;
            ISBN = product.ISBN;
            ImageUrl = product.ImageUrl;
            VatId = product.VatId;

            if (product.Category != null)
            {
                Category = new CategoryViewModel(product.Category);
            }

            if (product.Vat != null)
            {
                Vat = new VatViewModel(product.Vat);
            }

            if (product.UserRatings != null && product.UserRatings.Any())
            {
                Rating = Convert.ToInt32(Math.Round(product.UserRatings.Select(r => r.Rating).Average()));
            }

            if (product.ProductAuthors != null && product.ProductAuthors.Any())
            {
                Author = string.Join(", ",product.ProductAuthors.Select(a => a.Author.Name).OrderBy(a => a).ToArray());
            }
            else
            {
                Author = "(unknown)";
            }
        }

        public int Id { get; set; }
        public string ConcurrencyStamp { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string Author { get; set; }

        // ReSharper disable once InconsistentNaming
        public string ISBN { get; set; }
        public double PriceWithoutVat { get; set; }
        public double PriceWithVat { get; set; }
        public int InStock { get; set; }
        public string ImageUrl { get; set; }
        public int Rating { get; set; }
        public int CategoryId { get; set; }
        public CategoryViewModel Category { get; set; }
        public int VatId { get; set; }
        public VatViewModel Vat { get; set; }

        public Product ToEntity()
        {
            return new Product
            {
                Id = Id,
                ConcurrencyStamp = ConcurrencyStamp,
                Name = Name,
                Description = Description,
                ISBN = ISBN,
                PriceWithoutVat = PriceWithoutVat,
                InStock = InStock,
                CategoryId = CategoryId,
                VatId = VatId
            };
        }
    }
}