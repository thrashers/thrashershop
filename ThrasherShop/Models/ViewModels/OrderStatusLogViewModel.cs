﻿using System;
using ThrasherShop.Models.DbModels;

namespace ThrasherShop.Models.ViewModels
{
    public class OrderStatusLogViewModel 
    {
        public OrderStatusLogViewModel()
        {
        }

        public OrderStatusLogViewModel(OrderStatusLog orderStatusLog)
        {
            Id =orderStatusLog.Id;
            ConcurrencyStamp = orderStatusLog.ConcurrencyStamp;
            OrderStatus = orderStatusLog.OrderStatus;
            DateTime = orderStatusLog.DateTime;
            OrderId = orderStatusLog.OrderId;
            Order = orderStatusLog.Order;
        }

        public int Id { get; }
        public string ConcurrencyStamp { get; }
        public OrderStatus OrderStatus { get; }
        public DateTime DateTime { get; }
        public int OrderId { get; }
        public Order Order { get; }
    }
}