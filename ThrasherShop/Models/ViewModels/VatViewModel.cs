using ThrasherShop.Models.DbModels;

namespace ThrasherShop.Models.ViewModels
{
    public class VatViewModel
    {
        public VatViewModel(Vat vat)
        {
            if (vat != null)
            {
                Id = vat.Id;
                ConcurrencyStamp = vat.ConcurrencyStamp;
                Description = vat.Description;
                Tax = vat.Tax;
            }
        }

        public int Id { get; set; }
        public string ConcurrencyStamp { get; set; }
        public string Description { get; set; }
        public double Tax { get; set; }

        public Vat ToEntity()
        {
            return new Vat
            {
                Id = Id,
                ConcurrencyStamp = ConcurrencyStamp,
                Description = Description,
                Tax = Tax
            };
        }
    }
}