﻿using ThrasherShop.Models.DbModels;

namespace ThrasherShop.Models.ViewModels
{
    public class OrderLineViewModel
    {
        public OrderLineViewModel()
        {
        }

        public OrderLineViewModel(OrderLine orderLine) : this()
        {
            Id = orderLine.Id;
            ConcurrencyStamp = orderLine.ConcurrencyStamp;
            OrderId = orderLine.OrderId;
            Order = orderLine.Order;
            ProductId = orderLine.ProductId;
            Product = orderLine.Product;
            PriceWithoutVat = orderLine.PriceWithoutVat;
            Vat = orderLine.Vat;
            PriceWithVat = orderLine.PriceWithVat;
        }

        public int Id { get; set; }
        public string ConcurrencyStamp { get; set; }
        public int OrderId { get; set; }
        public Order Order { get; set; }
        public int ProductId { get; set; }
        public Product Product { get; set; }
        public double PriceWithoutVat { get; set; }
        public double Vat { get; set; }
        public double PriceWithVat { get; }
    }
}