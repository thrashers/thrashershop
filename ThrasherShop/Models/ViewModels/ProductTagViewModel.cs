﻿using ThrasherShop.Models.DbModels;

namespace ThrasherShop.Models.ViewModels
{
    public class ProductTagViewModel : IEntity
    {
        public ProductTagViewModel()
        {
        }

        public ProductTagViewModel(ProductTag productTag)
        {
            Id = productTag.Id;
            ConcurrencyStamp = productTag.ConcurrencyStamp;
            TagId = productTag.TagId;
            Tag = new TagViewModel(productTag.Tag);
            ProductId = productTag.ProductId;
            Product = new ProductViewModel(productTag.Product);
        }

        public int Id { get; set; }
        public string ConcurrencyStamp { get; set; }
        public int TagId { get; set; }
        public TagViewModel Tag { get; set; }
        public int ProductId { get; set; }
        public ProductViewModel Product { get; set; }
    }
}