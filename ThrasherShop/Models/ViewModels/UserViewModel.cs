﻿using System.Collections.Generic;
using ThrasherShop.Models.DbModels;

namespace ThrasherShop.Models.ViewModels
{
    public class UserViewModel
    {
        public UserViewModel()
        {
        }

        public UserViewModel(User user) : this()
        {
            Id = user.Id;
            ConcurrencyStamp = user.ConcurrencyStamp;
            SecurityStamp = user.SecurityStamp;
            FirstName = user.FirstName;
            LastName = user.LastName;
            Email = user.Email;

            if (user.Addresses != null)
            {
                foreach (var address in user.Addresses)
                {
                    Addresses.Add(new AddressViewModel(address));
                    
                }
            }
            if (user.Orders != null)
            {
                foreach (var order in user.Orders)
                {
                    Orders.Add(new OrderViewModel(order));
                }
            }
        }

        public int Id { get; set; }
        public string ConcurrencyStamp { get; set; }
        public string SecurityStamp { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string FullName => $"{FirstName} {LastName}".Trim();
        public string Email { get; set; }
        public ICollection<AddressViewModel> Addresses { get; } = new List<AddressViewModel>();
        public ICollection<OrderViewModel> Orders { get; } = new List<OrderViewModel>();

        public User ToEntity()
        {
            return new User
            {
                Id = Id,
                ConcurrencyStamp = ConcurrencyStamp,
                SecurityStamp = SecurityStamp,
                FirstName = FirstName,
                LastName = LastName,
                Email = Email
            };
        }
    }
}