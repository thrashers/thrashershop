﻿using System;
using System.Collections.Generic;
using ThrasherShop.Models.DbModels;

namespace ThrasherShop.Models.ViewModels
{
    public class OrderViewModel : IEntity
    {
        public OrderViewModel()
        {
        }

        public OrderViewModel(Order order)
        {
            Id = order.Id;
            ConcurrencyStamp = order.ConcurrencyStamp;
            OrderedOn = order.OrderedOn;
            Email = order.Email;
            FirstName = order.FirstName;
            LastName = order.LastName;
            AddressLine1 = order.AddressLine1;
            AddressLine2 = order.AddressLine2;
            AddressLine3 = order.AddressLine3;
            AddressLine4 = order.AddressLine4;
            HouseNumber = order.HouseNumber;
            ZipCode = order.ZipCode;
            City = order.City;
            Country = order.Country;
            Status = order.Status;
            OrderedById = order.OrderedById;
            OrderedBy = order.OrderedBy;

            if (order.Products != null)
            {
                foreach (var orderLine in order.Products)
                {
                    Products.Add(new OrderLineViewModel(orderLine));
                }
            }
        }

        public int Id { get; set; }
        public string ConcurrencyStamp { get; set; }
        public DateTime OrderedOn { get; set; }
        public string Email { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string FullName => $"{FirstName} {LastName}".Trim();
        public string AddressLine1 { get; set; }
        public string AddressLine2 { get; set; }
        public string AddressLine3 { get; set; }
        public string AddressLine4 { get; set; }
        public string HouseNumber { get; set; }
        public string ZipCode { get; set; }
        public string City { get; set; }
        public string Country { get; set; }
        public OrderStatus Status { get; set; }
        public int? OrderedById { get; set; }
        public User OrderedBy { get; set; }
        public ICollection<OrderLineViewModel> Products { get; } = new List<OrderLineViewModel>();

        public Order ToEntity()
        {
            return new Order
            {
                Id = Id,
                ConcurrencyStamp = ConcurrencyStamp,
                OrderedOn = OrderedOn,
                Email = Email,
                FirstName = FirstName,
                LastName = LastName,
                AddressLine1 = AddressLine1,
                AddressLine2 = AddressLine2,
                AddressLine3 = AddressLine3,
                AddressLine4 = AddressLine4,
                HouseNumber = HouseNumber,
                ZipCode = ZipCode,
                City = City,
                Country = Country,
                Status = Status,
                OrderedById = OrderedById
            };
        }
    }
}