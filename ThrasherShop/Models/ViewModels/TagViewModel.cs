﻿using ThrasherShop.Models.DbModels;

namespace ThrasherShop.Models.ViewModels
{
    public class TagViewModel
    {
        public TagViewModel()
        {
        }

        public TagViewModel(Tag tag)
        {
            Id = tag.Id;
            ConcurrencyStamp = tag.ConcurrencyStamp;
            TagName = tag.TagName;
        }

        public int Id { get; set; }
        public string ConcurrencyStamp { get; set; }
        public string TagName { get; set; }

        public Tag ToEntity()
        {
            return new Tag
            {
                Id = Id,
                ConcurrencyStamp = ConcurrencyStamp,
                TagName = TagName
            };
        }
    }
}