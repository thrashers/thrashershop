namespace ThrasherShop.Models.RequestModels
{
    public class TagRequestModel : IRequestModel
    {
        public int PageNumber { get; set; }
        public int PageSize { get; set; }
        public string TagName { get; set; }
    }
}