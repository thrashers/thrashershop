namespace ThrasherShop.Models.RequestModels
{
    public class CategoryRequestModel : IRequestModel
    {
        public string Description { get; set; }
        public int? ParentCategory { get; set; }
        public int PageNumber { get; set; }
        public int PageSize { get; set; }
    }
}