namespace ThrasherShop.Models.RequestModels
{
    public class AddressRequestModel : IRequestModel
    {
        public int PageNumber { get; set; }
        public int PageSize { get; set; }
        public string AddressLine1 { get; set; }
        public string AddressLine2 { get; set; }
        public string AddressLine3 { get; set; }
        public string AddressLine4 { get; set; }
        public string HouseNumber { get; set; }
        public string ZipCode { get; set; }
        public string City { get; set; }
        public string Country { get; set; }
    }
}