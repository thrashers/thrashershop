namespace ThrasherShop.Models.RequestModels
{
    public interface IRequestModel
    {
        int PageNumber { get; set; }
        int PageSize { get; set; }
    }
}