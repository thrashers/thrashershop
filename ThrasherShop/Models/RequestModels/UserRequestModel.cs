namespace ThrasherShop.Models.RequestModels
{
    public class UserRequestModel : IRequestModel
    {
        public int PageNumber { get; set; }
        public int PageSize { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
    }
}