namespace ThrasherShop.Models.RequestModels
{
    public class VatRequestModel : IRequestModel
    {
        public int PageNumber { get; set; }
        public int PageSize { get; set; }
        public string Description { get; set; }
        public double? Tax { get; set; }
    }
}