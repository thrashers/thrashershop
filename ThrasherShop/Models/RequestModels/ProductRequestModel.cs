﻿namespace ThrasherShop.Models.RequestModels
{
    public class ProductRequestModel : IRequestModel
    {
        public int PageNumber { get; set; }
        public int PageSize { get; set; }
        public int CategoryId { get; set; }
        public string Description { get; set; }
        public string Name { get; set; }
        public double? PriceWithoutVat { get; set; }
        public double? PriceWithVat { get; set; }
        public int VatId { get; set; }
        // ReSharper disable once InconsistentNaming
        public string ISBN { get; set; }
    }
}