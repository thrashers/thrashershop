﻿namespace ThrasherShop.Models.DbModels
{
    public interface IEntity
    {
        int Id { get; set; }
        string ConcurrencyStamp { get; set; }
    }
}