﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ThrasherShop.Models.DbModels
{
    public class WishList : IEntity
    {
        [NotMapped]
        public int Id { get; set; }

        [Required]
        public string ConcurrencyStamp { get; set; } = Guid.NewGuid().ToString();
        public int UserId { get; set; }
        public User User { get; set; }
        public int ProductId { get; set; }
        public Product Product { get; set; }
    }
}