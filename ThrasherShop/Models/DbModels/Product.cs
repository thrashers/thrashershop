﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace ThrasherShop.Models.DbModels
{
    public class Product : IEntity
    {
        public int Id { get; set; }

        [Required]
        public string ConcurrencyStamp { get; set; } = Guid.NewGuid().ToString();

        [Required]
        [StringLength(256)]
        public string Name { get; set; }

        [Required]
        public string Description { get; set; }

        [Required]
        public double PriceWithoutVat { get; set; }

        public double PriceWithVat => (PriceWithoutVat / 100) * (Vat?.Tax ?? 0) + PriceWithoutVat;

        [Required]
        public int InStock { get; set; }

        // ReSharper disable once InconsistentNaming
        [StringLength(256)]
        public string ISBN { get; set; }

        public string ImageUrl { get; set; }

        public int VatId { get; set; }
        public Vat Vat { get; set; }
        public int CategoryId { get; set; }
        public Category Category { get; set; }
        public ICollection<ProductAuthor> ProductAuthors { get; set; }
        public ICollection<UserRating> UserRatings { get; set; }
    }
}