﻿using System;
using System.ComponentModel.DataAnnotations;

namespace ThrasherShop.Models.DbModels
{
    public class UserToken : IEntity
    {
        public int Id { get; set; }

        [Required]
        public string ConcurrencyStamp { get; set; } = Guid.NewGuid().ToString();

        [StringLength(256)]
        public string Token { get; set; }
        public TokenType TokenType { get; set; }
        public bool IsUsed { get; set; }
        public DateTime DateTime { get; set; }

        public int UserId { get; set; }
        public User User { get; set; }
    }

    public enum TokenType
    {
        Email,
        Password
    }
}