﻿using System;
using System.ComponentModel.DataAnnotations;

namespace ThrasherShop.Models.DbModels
{
    public class Vat : IEntity
    {
        public int Id { get; set; }
        public string ConcurrencyStamp { get; set; } = Guid.NewGuid().ToString();

        [Required]
        [StringLength(256)]
        public string Description { get; set; }
        public double Tax { get; set; }
    }
}