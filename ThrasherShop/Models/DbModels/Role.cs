﻿using Microsoft.AspNetCore.Identity;

namespace ThrasherShop.Models.DbModels
{
    public class Role : IdentityRole<int>, IEntity
    {
    }
}