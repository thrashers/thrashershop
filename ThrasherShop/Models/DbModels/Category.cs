﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace ThrasherShop.Models.DbModels
{
    public class Category : IEntity
    {
        public int Id { get; set; }

        [Required]
        public string ConcurrencyStamp { get; set; } = Guid.NewGuid().ToString();

        [Required]
        [StringLength(256)]
        public string Description { get; set; }
        public int? ParentId { get; set; }
        public Category Parent { get; set; }
        public ICollection<Product> Products { get; set; }
    }
}