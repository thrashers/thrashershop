﻿using System;
using System.ComponentModel.DataAnnotations;

namespace ThrasherShop.Models.DbModels
{
    public class Tag : IEntity
    {
        public int Id { get; set; }

        [Required]
        public string ConcurrencyStamp { get; set; } = Guid.NewGuid().ToString();

        [Required]
        [StringLength(256)]
        public string TagName { get; set; }
    }
}