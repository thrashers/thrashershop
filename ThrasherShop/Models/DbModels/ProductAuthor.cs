﻿using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace ThrasherShop.Models.DbModels
{
    public class ProductAuthor : IEntity
    {
        [NotMapped]
        public int Id { get; set; }
        public string ConcurrencyStamp { get; set; } = Guid.NewGuid().ToString();
        public int ProductId { get; set; }
        public Product Product { get; set; }
        public int AuthorId { get; set; }
        public Author Author { get; set; }
    }
}