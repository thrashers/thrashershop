﻿using System;
using System.ComponentModel.DataAnnotations;

namespace ThrasherShop.Models.DbModels
{
    public class Address : IEntity
    {
        public int Id { get; set; }

        [ConcurrencyCheck]
        public string ConcurrencyStamp { get; set; } = Guid.NewGuid().ToString();

        [Required]
        [StringLength(256)]
        public string AddressLine1 { get; set; }

        [StringLength(256)]
        public string AddressLine2 { get; set; }

        [StringLength(256)]
        public string AddressLine3 { get; set; }

        [StringLength(256)]
        public string AddressLine4 { get; set; }

        [Required]
        [StringLength(6)]
        public string HouseNumber { get; set; }

        [Required]
        [StringLength(10)]
        public string ZipCode { get; set; }

        [Required]
        [StringLength(256)]
        public string City { get; set; }

        [Required]
        [StringLength(256)]
        public string Country { get; set; }

        public int UserId { get; set; }
        public User User { get; set; }
    }
}