﻿using System;
using System.ComponentModel.DataAnnotations;

namespace ThrasherShop.Models.DbModels
{
    public class OrderLine : IEntity
    {
        public int Id { get; set; }

        [Required]
        public string ConcurrencyStamp { get; set; } = Guid.NewGuid().ToString();

        public int OrderId { get; set; }
        public Order Order { get; set; }
        public int ProductId { get; set; }
        public Product Product { get; set; }

        [Required]
        public double PriceWithoutVat { get; set; }

        [Required]
        public double Vat { get; set; }

        public double PriceWithVat => (PriceWithoutVat / 100) * Vat + PriceWithoutVat;
    }
}