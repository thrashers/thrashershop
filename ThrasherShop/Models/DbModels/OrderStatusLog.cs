﻿using System;
using System.ComponentModel.DataAnnotations;

namespace ThrasherShop.Models.DbModels
{
    public class OrderStatusLog : IEntity
    {
        public int Id { get; set; }

        [Required]
        public string ConcurrencyStamp { get; set; } = Guid.NewGuid().ToString();

        [Required]
        public OrderStatus OrderStatus { get; set; }

        [Required]
        public DateTime DateTime { get; set; }

        public int OrderId { get; set; }
        public Order Order { get; set; }
    }
}