﻿namespace ThrasherShop.Models.DbModels
{
    public enum OrderStatus
    {
        Ordered,
        Paid,
        Completed,
        Returned
    }
}