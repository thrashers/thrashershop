﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace ThrasherShop.Models.DbModels
{
    public class Order : IEntity
    {
        public int Id { get; set; }

        [Required]
        public string ConcurrencyStamp { get; set; } = Guid.NewGuid().ToString();

        [Required]
        public DateTime OrderedOn { get; set; }

        [Required]
        [StringLength(256)]
        public string Email { get; set; }

        [Required]
        [StringLength(256)]
        public string FirstName { get; set; }

        [Required]
        [StringLength(256)]
        public string LastName { get; set; }
        public string FullName => $"{FirstName} {LastName}".Trim();

        [StringLength(256)]
        public string AddressLine1 { get; set; }

        [StringLength(256)]
        public string AddressLine2 { get; set; }

        [StringLength(256)]
        public string AddressLine3 { get; set; }

        [StringLength(256)]
        public string AddressLine4 { get; set; }

        [StringLength(6)]
        public string HouseNumber { get; set; }

        [StringLength(10)]
        public string ZipCode { get; set; }

        [StringLength(256)]
        public string City { get; set; }

        [StringLength(256)]
        public string Country { get; set; }

        [Required]
        public OrderStatus Status { get; set; }
        public int? OrderedById { get; set; }
        public User OrderedBy { get; set; }
        public IEnumerable<OrderLine> Products { get; set; }
    }
}