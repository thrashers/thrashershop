using System;
using System.Collections.Generic;
using Microsoft.Extensions.Logging.Abstractions;
using ThrasherShop.Controllers;
using Xunit;

namespace ThrasherShopTests.ControllerTests
{
    public class BaseControllerUnitTests : IDisposable
    {
        private readonly BaseController _controller;

        public BaseControllerUnitTests()
        {
            _controller = new BaseController(NullLogger.Instance);
        }

        [Fact]
        public void LogInformation_ShouldReturnMessage()
        {
            var message = _controller.LogInfo("UnitTest", "Unit testing log information");
            Assert.Equal("Method: UnitTest. Message: Unit testing log information", message);
        }

        [Fact]
        public void LogError_ShouldReturnFormattedExceptionMessage()
        {
            var message = _controller.LogError("UnitTest", new Exception("Unit testing scenario 1"));
            Assert.Equal("Error in method: UnitTest. Message: Unit testing scenario 1. StackTrace: . InnerException: Unit testing scenario 1", message.Replace("\r\n", ""));
        }

        [Fact]
        public void LogAggregateErrorInLogError_ShouldReturnMultipleFormattedLinesWithExceptionMessages()
        {
            var exceptions = new List<Exception>
            {
                new Exception("Exception one"),
                new Exception("Exception two")
            };
            var message = _controller.LogError("UnitTest", new AggregateException(exceptions));

            Assert.Contains("Error in method: UnitTest. Message: Exception one", message);
            Assert.Contains("Error in method: UnitTest. Message: Exception two", message);
        }

        [Fact]
        public void LogAggregateErrorInLogAggregationError_ShouldReturnMultipleFormattedLinesWithExceptionMessages()
        {
            var exceptions = new List<Exception>
            {
                new Exception("Exception one"),
                new Exception("Exception two")
            };
            var message = _controller.LogError("UnitTest", new AggregateException(exceptions));

            Assert.Contains("Error in method: UnitTest. Message: Exception one", message);
            Assert.Contains("Error in method: UnitTest. Message: Exception two", message);
        }

        public void Dispose()
        {
            _controller?.Dispose();
        }
    }
}