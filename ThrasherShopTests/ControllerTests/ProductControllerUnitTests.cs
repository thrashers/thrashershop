using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging.Abstractions;
using Moq;
using ThrasherShop.Controllers;
using ThrasherShop.Models.DbModels;
using ThrasherShop.Models.RequestModels;
using ThrasherShop.Models.ResultModels;
using ThrasherShop.Models.ViewModels;
using ThrasherShop.Repositories;
using Xunit;

namespace ThrasherShopTests.ControllerTests
{
    public class ProductControllerUnitTests : IDisposable
    {
        private readonly ProductController _controller;
        private readonly Mock<IProductRepository> _mockRepository;

        public ProductControllerUnitTests()
        {
            _mockRepository = new Mock<IProductRepository>();
            _controller = new ProductController(NullLogger<ProductController>.Instance, _mockRepository.Object);
        }

        [Fact]
        public async Task GetSingleBook_ShouldReturnOk()
        {
            _mockRepository.Setup(e => e.Get(1)).Returns(() => Task.Factory.StartNew(() => new Product { Id = 1 }));
            var productResultModel = await _controller.Get(1);
            Assert.Equal(1, productResultModel.ResultSet.Id);
        }

        [Fact]
        public async Task FilterOnBook_ShouldReturnSuccess()
        {
            var requestModel = new ProductRequestModel();
            _mockRepository.Setup(e => e.Filter(requestModel)).Returns(() => Task.Factory.StartNew(() => new DbResultModel<Product>(1, new List<Product>())));
            var listResultModel = await _controller.Filter(requestModel);
            Assert.True(listResultModel.IsSuccess);
        }

        [Fact]
        public void SaveNewProduct_ShouldReturnOk()
        {
            _mockRepository.Setup(e => e.Save(new Product()));
            var resultModel = _controller.Post(new ProductViewModel { Id = 11 });
            Assert.True(resultModel.IsSuccess);
        }

        [Fact]
        public async Task DeleteProduct_ShouldBeSuccess()
        {
            var resultModel = await _controller.Delete(1);
            Assert.True(resultModel.IsSuccess);
        }

        public void Dispose()
        {
            _controller?.Dispose();
        }
    }
}