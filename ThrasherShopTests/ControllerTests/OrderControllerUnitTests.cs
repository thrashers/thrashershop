using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging.Abstractions;
using Moq;
using ThrasherShop.Controllers;
using ThrasherShop.Models.DbModels;
using ThrasherShop.Models.RequestModels;
using ThrasherShop.Models.ResultModels;
using ThrasherShop.Models.ViewModels;
using ThrasherShop.Repositories;
using Xunit;

namespace ThrasherShopTests.ControllerTests
{
    public class OrderControllerUnitTests : IDisposable
    {
        private readonly OrderController _controller;
        private readonly Mock<IOrderRepository> _mockRepository;

        public OrderControllerUnitTests()
        {
            _mockRepository = new Mock<IOrderRepository>();
            _controller = new OrderController(NullLogger<OrderController>.Instance, _mockRepository.Object);
        }

        [Fact]
        public async Task GetSingleOrder_ShouldReturnOk()
        {
            _mockRepository.Setup(e => e.Get(1)).Returns(() => Task.Factory.StartNew(() => new Order { Id = 1 }));
            var orderResultModel = await _controller.Get(1);
            Assert.Equal(1, orderResultModel.ResultSet.Id);
        }

        [Fact]
        public async Task FilterOrder_ShouldReturnSuccess()
        {
            var requestModel = new OrderRequestModel();
            _mockRepository.Setup(e => e.Filter(requestModel)).Returns(() => Task.Factory.StartNew(() => new DbResultModel<Order>(1, new List<Order>())));
            var listResultModel = await _controller.Filter(requestModel);
            Assert.True(listResultModel.IsSuccess);
        }

        [Fact]
        public void SaveOrder_ShouldReturnOk()
        {
            _mockRepository.Setup(e => e.Save(new Order()));
            var resultModel = _controller.Post(new OrderViewModel());
            Assert.True(resultModel.IsSuccess);
        }

        [Fact]
        public async Task DeleteOrder_ShouldReturnOk()
        {
            var resultModel = await _controller.Delete(1);
            Assert.True(resultModel.IsSuccess);
        }

        public void Dispose()
        {
            _controller?.Dispose();
        }
    }
}