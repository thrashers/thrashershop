using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging.Abstractions;
using Moq;
using ThrasherShop.Controllers;
using ThrasherShop.Models.DbModels;
using ThrasherShop.Models.RequestModels;
using ThrasherShop.Models.ResultModels;
using ThrasherShop.Models.ViewModels;
using ThrasherShop.Repositories;
using Xunit;

namespace ThrasherShopTests.ControllerTests
{
    public class AddressControllerUnitTests : IDisposable
    {
        private readonly AddressController _controller;
        private readonly Mock<IAddressRepository> _mockRepository;

        public AddressControllerUnitTests()
        {
            _mockRepository = new Mock<IAddressRepository>();
            _controller = new AddressController(NullLogger<AddressController>.Instance, _mockRepository.Object);
        }

        [Fact]
        public async Task GetSingleAddress_ShouldReturnOk()
        {
            _mockRepository.Setup(m => m.Get(1)).Returns(() => Task.Factory.StartNew(() => new Address { Id = 1 }));
            var addressResultModel = await _controller.Get(1);
            Assert.Equal(1, addressResultModel.ResultSet.Id);
        }

        [Fact]
        public async Task FilterAddress_ShouldReturnOk()
        {
            var requestModel = new AddressRequestModel();
            _mockRepository.Setup(e => e.Filter(requestModel)).Returns(() => Task.Factory.StartNew(() => new DbResultModel<Address>(1, new List<Address>())));
            var addressResult = await _controller.Filter(requestModel);
            Assert.True(addressResult.IsSuccess);
        }

        [Fact]
        public void SaveOrder_ShouldReturnOk()
        {
            _mockRepository.Setup(e => e.Save(new Address()));
            var resultModel = _controller.Post(new AddressViewModel());
            Assert.True(resultModel.IsSuccess);
        }

        [Fact]
        public async Task DeleteOrder_ShouldReturnOk()
        {
            var resultModel = await _controller.Delete(1);
            Assert.True(resultModel.IsSuccess);
        }

        public void Dispose()
        {
            _controller?.Dispose();
        }
    }
}