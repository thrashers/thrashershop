using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging.Abstractions;
using Moq;
using ThrasherShop.Controllers;
using ThrasherShop.Models.DbModels;
using ThrasherShop.Models.ResultModels;
using ThrasherShop.Repositories;
using Xunit;

namespace ThrasherShopTests.ControllerTests
{
    public class WishListControllerTests : IDisposable
    {
        private readonly WishListController _controller;
        private readonly Mock<IWishListRepository> _mockRepository;

        public WishListControllerTests()
        {
            _mockRepository = new Mock<IWishListRepository>();
            _controller = new WishListController(NullLogger<WishListController>.Instance, _mockRepository.Object);
        }

        [Fact]
        public async Task WishListController_GetByUser_ShouldReturnUserWishList()
        {
            _mockRepository.Setup(e => e.GetByUser(1, 1, 15)).Returns(GetByUserFunction);
            var wishListResultModel = await _controller.GetByUser(1, 1, 15);
            Assert.True(wishListResultModel.ResultSet.Any());
        }

        [Fact]
        public async Task WishListController_GetByUser_ShouldReturnInvalidUser()
        {
            _mockRepository.Setup(e => e.GetByUser(0, 1, 15));
            var wishListResultModel = await _controller.GetByUser(0, 1, 15);
            Assert.Equal("User id is invalid", wishListResultModel.Errors.First());
        }

        [Fact]
        public void WishListController_Post_ShouldFail()
        {
            var resultModel = _controller.Save(0, 0);
            Assert.True(!resultModel.IsSuccess);
        }

        [Fact]
        public void WishListController_Post_ShouldSucceed()
        {
            var wishList = new WishList();
            _mockRepository.Setup(e => e.Save(wishList));
            var resultModel = _controller.Save(1, 1);
            Assert.True(resultModel.IsSuccess);
        }

        public void Dispose()
        {
        }

        private Task<DbResultModel<WishList>> GetByUserFunction()
        {
            return Task.Factory.StartNew(() => new DbResultModel<WishList>(1, new List<WishList>
            {
                new WishList
                {
                    ConcurrencyStamp = Guid.NewGuid().ToString(),
                    ProductId = 1,
                    UserId = 1
                }
            }));
        }
    }
}