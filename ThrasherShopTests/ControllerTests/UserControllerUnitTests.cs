using System;
using System.Collections.Generic;
using Microsoft.Extensions.Logging.Abstractions;
using System.Threading.Tasks;
using Moq;
using ThrasherShop.Controllers;
using ThrasherShop.Models.DbModels;
using ThrasherShop.Models.RequestModels;
using ThrasherShop.Models.ResultModels;
using ThrasherShop.Models.ViewModels;
using ThrasherShop.Repositories;
using Xunit;

namespace ThrasherShopTests.ControllerTests
{
    public class UserControllerUnitTests : IDisposable
    {
        private readonly UserController _controller;
        private readonly Mock<IUserRepository> _mockRepository;
        private readonly Mock<IOrderRepository> _mockOrders;

        public UserControllerUnitTests()
        {
            _mockRepository = new Mock<IUserRepository>();
            _mockOrders = new Mock<IOrderRepository>();
            _controller = new UserController(NullLogger<UserController>.Instance, _mockRepository.Object, _mockOrders.Object);
        }

        //[Fact]
        //public async Task GetSingleUser_ShouldReturnOk()
        //{
        //    _mockRepository.Setup(e => e.Get(1)).Returns(() => Task.Factory.StartNew(() => new User { Id = 1 }));
        //    var userResultModel = await _controller.Get(1);
        //    Assert.Equal(1, userResultModel.ResultSet.Id);
        //}

        [Fact]
        public async Task FilterUser_ShouldReturnSuccess()
        {
            var requestModel = new UserRequestModel();
            _mockRepository.Setup(e => e.Filter(requestModel)).Returns(() => Task.Factory.StartNew(() => new DbResultModel<User>(1, new List<User>())));
            var listResultModel = await _controller.Filter(requestModel);
            Assert.True(listResultModel.IsSuccess);
        }

        [Fact]
        public void PostUser_ShouldReturnOk()
        {
            _mockRepository.Setup(e => e.Save(new User()));
            var resultModel = _controller.Post(new UserViewModel { Id = 11 });
            Assert.True(resultModel.IsSuccess);
        }

        [Fact]
        public async Task DeleteUser_ShouldReturnSuccess()
        {
            _mockOrders.Setup(e => e.GetByUser(1, 1, 1));
            var resultModel = await _controller.Delete(1);
            Assert.True(resultModel.IsSuccess);
        }

        public void Dispose()
        {
            _controller?.Dispose();
        }
    }
}