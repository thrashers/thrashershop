using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging.Abstractions;
using Moq;
using ThrasherShop.Controllers;
using ThrasherShop.Models.DbModels;
using ThrasherShop.Models.RequestModels;
using ThrasherShop.Models.ResultModels;
using ThrasherShop.Models.ViewModels;
using ThrasherShop.Repositories;
using Xunit;

namespace ThrasherShopTests.ControllerTests
{
    public class TagControllerUnitTests : IDisposable
    {
        private readonly TagController _controller;
        private readonly Mock<ITagRepository> _mockRepository;

        public TagControllerUnitTests()
        {
            _mockRepository = new Mock<ITagRepository>();
            _controller = new TagController(NullLogger<TagController>.Instance, _mockRepository.Object);
        }

        [Fact]
        public async Task GetSingleTag_ShouldReturnOk()
        {
            _mockRepository.Setup(e => e.Get(1)).Returns(() => Task.Factory.StartNew(() => new Tag { Id = 1 }));
            var tagResultModel = await _controller.Get(1);
            Assert.Equal(1, tagResultModel.ResultSet.Id);
        }

        [Fact]
        public async Task FilterTag_ShouldReturnSuccess()
        {
            var requestModel = new TagRequestModel();
            _mockRepository.Setup(e => e.Filter(requestModel)).Returns(() => Task.Factory.StartNew(() => new DbResultModel<Tag>(1, new List<Tag>())));
            var listResultModel = await _controller.Filter(requestModel);
            Assert.True(listResultModel.IsSuccess);
        }

        [Fact]
        public void PostUser_ShouldReturnOk()
        {
            var resultModel = _controller.Post(new TagViewModel { Id = 11 });
            Assert.True(resultModel.IsSuccess);
        }

        [Fact]
        public async Task DeleteTag_ShouldReturnSuccess()
        {
            var resultModel = await _controller.Delete(1);
            Assert.True(resultModel.IsSuccess);
        }

        public void Dispose()
        {
            _controller?.Dispose();
        }
    }
}