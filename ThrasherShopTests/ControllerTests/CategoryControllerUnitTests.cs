using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging.Abstractions;
using Moq;
using ThrasherShop.Controllers;
using ThrasherShop.Models.DbModels;
using ThrasherShop.Models.RequestModels;
using ThrasherShop.Models.ResultModels;
using ThrasherShop.Models.ViewModels;
using ThrasherShop.Repositories;
using Xunit;

namespace ThrasherShopTests.ControllerTests
{
    public class CategoryControllerUnitTests : IDisposable
    {
        private readonly CategoryController _controller;
        private readonly Mock<ICategoryRepository> _mockRepository;

        public CategoryControllerUnitTests()
        {
            _mockRepository = new Mock<ICategoryRepository>();
            _controller = new CategoryController(NullLogger<CategoryController>.Instance, _mockRepository.Object);
        }

        [Fact]
        public async Task GetSingleCategory_ShouldReturnOk()
        {
            _mockRepository.Setup(e => e.Get(1)).Returns(() => Task.Factory.StartNew(() => new Category { Id = 1 }));
            var categoryResultModel = await _controller.Get(1);
            Assert.Equal(1, categoryResultModel.ResultSet.Id);
        }

        [Fact]
        public async Task FilterCategoryFuzzyOnName_ShouldReturn2Categories()
        {
            var requestModel = new CategoryRequestModel();
            _mockRepository.Setup(e => e.Filter(requestModel)).Returns(() => Task.Factory.StartNew(() => new DbResultModel<Category>(1, new List<Category>())));
            var listResultModel = await _controller.Filter(requestModel);
            Assert.True(listResultModel.IsSuccess);
        }

        [Fact]
        public async Task PostCategory_ShouldReturnOk()
        {
            var resultModel = await _controller.Post(new CategoryViewModel { Id = 11 });
            Assert.True(resultModel.IsSuccess);
        }

        [Fact]
        public async Task DeleteCategory_ShouldReturnSuccess()
        {
            _mockRepository.Setup(e => e.Get(1)).Returns(DeleteCategory);
            var resultModel = await _controller.Delete(1);
            Assert.True(resultModel.IsSuccess);
        }

        [Fact]
        public async Task DeleteCategory_ShouldFail()
        {
            _mockRepository.Setup(e => e.Get(1)).Returns(DeleteCategoryWithProducts);
            var resultModel = await _controller.Delete(1);
            Assert.True(!resultModel.IsSuccess);
        }

        private Task<Category> DeleteCategoryWithProducts()
        {
            return Task.Factory.StartNew(() => new Category {Products = new List<Product> {new Product()}});
        }

        private Task<Category> DeleteCategory()
        {
            return Task.Factory.StartNew(() => new Category {Products = new List<Product>()});
        }

        public void Dispose()
        {
            _controller?.Dispose();
        }
    }
}