﻿using ThrasherShop.Extensions;
using Xunit;
// ReSharper disable StringLiteralTypo

namespace ThrasherShopTests.ExtensionsTests
{
    public class StringExtensionsUnitTests
    {
        [Theory]
        [InlineData("Some", null)]
        [InlineData("vaRIable", null)]
        [InlineData("fuZZy", null)]
        [InlineData("INVARIANT", null)]
        [InlineData("stRING", null)]
        [InlineData("Test", null)]
#pragma warning disable xUnit1026 // Theory methods should use all of their parameters
        public void FuzzyTests_ShouldSucceed(string parameter, string skip)
#pragma warning restore xUnit1026 // Theory methods should use all of their parameters
        {
            var text = "Some string variable to test the fuzzy(contains) invariant culture ignore case string test";
            Assert.True(text.ContainsInvariant(parameter));
        }
    }
}