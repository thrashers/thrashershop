﻿using System;
using ThrasherShop.Extensions;
using Xunit;

namespace ThrasherShopTests.ExtensionsTests
{
    public class ExceptionExtensionUnitTests
    {
        [Fact]
        public void ExtractInnerException_ReturnsEmptyException()
        {
            Exception ex = null;
            var extractInnerException = ex.ExtractInnerException();
            Assert.Equal("Empty exception".ToLowerInvariant(), extractInnerException.Message.ToLowerInvariant());
        }

        [Fact]
        public void ExtractInnerException_ReturnsFirstException()
        {
            Exception ex = new Exception("Find this one");
            var extractedException = ex.ExtractInnerException();
            Assert.Equal("Find this one", extractedException.Message);
        }

        [Fact]
        public void ExtractInnerException_ReturnsOneInnerException()
        {
            var innerException = new Exception("Find this one");
            var wrapperException = new Exception("Not this one", innerException);
            var extractedException = wrapperException.ExtractInnerException();
            Assert.Equal("Find this one", extractedException.Message);
        }

        [Fact]
        public void FormatException_ReturnsEmptyException()
        {
            Exception exception = null;
            var message = exception.FormatExceptionMessage();
            Assert.Equal("Exception is null", message);
        }

        [Fact]
        public void FormatException_ReturnsFormattedException()
        {
            Exception exception = new Exception("Testing");
            var message = exception.FormatExceptionMessage();
            Assert.Equal("Message: Testing. StackTrace: . InnerException: Testing", message);
        }
    }
}