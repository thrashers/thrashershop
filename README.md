Project ThrasherShop.

Vereisten om te kunnen draaien:
- Visual Studio 2017 (meest recente updates);
- .Net Core SDK (v2.1.402 op het moment van schrijven);
- Node js (v10.10.0-x64 op het moment van schrijven).

Voor front-end development is slechts NodeJs en een code editor vereist.